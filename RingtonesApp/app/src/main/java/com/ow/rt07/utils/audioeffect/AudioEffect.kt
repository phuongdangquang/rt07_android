package com.ow.rt07.utils.audioeffect
import android.content.Context
import android.util.Log
import com.ow.rt07.utils.FileUtil
import linc.com.library.AudioTool
import linc.com.library.callback.OnFileComplete
import linc.com.library.types.Echo
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File


private const val LOG_TAG = "Audio Effect"

interface AudioEffectCallback {
    fun success(path: String)
}

interface AudioSaveEffectCallback {
    fun success()
}

class AudioEffect  {

    private var context:Context? = null

    private var cachePath:String = ""
    private var effectPath:String = ""
    private var listener:AudioEffectCallback? = null
    private var listenerSave:AudioSaveEffectCallback? = null

    private var eSpeed:Float? = null
    private var eStartTime:Int = 0
    private var eTrimTime:Int = 0
    private var eEndTime:Int = 0

    constructor(ctx: Context) {
        this.context = ctx
        this.cachePath = context?.getExternalFilesDir(null)?.absolutePath + "/Music/cache.mp3"
        this.effectPath = context?.getExternalFilesDir(null)?.absolutePath + "/Music/effect.mp3"
    }

    fun cut(
        path:String,
        startTime:Int,
        endTime:Int,
        trimTime:Int,
        isFadeIn:Boolean,
        isFadeOut:Boolean,
        isEffect:Boolean,
        speed: Float?,
        lis: AudioEffectCallback) {

        this.eStartTime = startTime
        this.eEndTime = endTime
        this.eTrimTime = trimTime
        this.eSpeed = speed
        this.listener = lis

        Log.d(LOG_TAG,"Cut Start Time : $startTime")
        Log.d(LOG_TAG,"Cut End Time : $endTime")

        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(path))
                    .cutAudio(startTime, endTime - startTime, OnFileComplete {
                        Log.d(LOG_TAG,"Cut Audio Success !")
                    })
                    .saveCurrentTo(cachePath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }

            uiThread {
                Log.d(LOG_TAG,"Duration After Cut : ${context?.let { it1 ->
                    FileUtil.audioFileDuration(
                        it1, cachePath)
                }}")

                if(isFadeIn && !isFadeOut && !isEffect && eSpeed == null){
                    fadeInOnly()
                }else if (isFadeIn && !isFadeOut && isEffect){
                    fadeInWithEffect()
                }else if (isFadeIn && !isFadeOut && !isEffect && eSpeed != null){
                    fadeInWithSpeed()
                }else if (isFadeIn && !isFadeOut && isEffect && eSpeed != null){
                    fadeInWithSpeedAndEffect()
                } else if (!isFadeIn && isFadeOut && !isEffect && eSpeed == null){
                    fadeOutOnly()
                } else if (!isFadeIn && isFadeOut && isEffect){
                    fadeOutWithEffect()
                } else if (!isFadeIn && isFadeOut && !isEffect && eSpeed != null){
                    fadeOutWithSpeed()
                }else if (!isFadeIn && isFadeOut && isEffect && eSpeed != null){
                    fadeOutWithSpeedAndEffect()
                } else if (isFadeIn && isFadeOut && !isEffect && eSpeed == null){
                    fadeInAndOutOnly()
                } else if (isFadeIn && isFadeOut && isEffect){
                    fadeInAndOutWithEffect()
                }else if (isFadeIn && isFadeOut && !isEffect && eSpeed != null){
                    fadeInAndOutWithSpeed()
                } else if (!isFadeIn && !isFadeOut && !isEffect && eSpeed != null){
                    speedOnly()
                }else if (!isFadeIn && !isFadeOut && isEffect && eSpeed == null){
                    effectOnly()
                }else if (!isFadeIn && !isFadeOut && isEffect && eSpeed != null){
                    effectWithSpeed()
                }else if (isFadeIn && isFadeOut && isEffect && eSpeed != null){
                    allEffect()
                }else{
                    listener?.success(effectPath)
                }
            }
        }
    }

    private fun fadeInOnly(){
        Log.d(LOG_TAG, cachePath)
        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
                    .fadeIn(OnFileComplete {
                        Log.d(LOG_TAG,"Fade In Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }

    private fun fadeInWithSpeed(){
        Log.d(LOG_TAG, cachePath)
        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
                    .fadeIn(OnFileComplete {
                        Log.d(LOG_TAG,"Fade In Audio Success !")
                    })
                    .changeAudioSpeed(eSpeed!!, OnFileComplete {
                        Log.d(LOG_TAG,"Speed Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }

    private fun fadeInWithEffect(){
        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
                    .fadeIn(OnFileComplete {
                        Log.d(LOG_TAG,"Fade In Audio Success !")
                    })
//                    .removeVocal(OnFileComplete {
//                        Log.d(LOG_TAG,"Remove Vocal  Audio Success !")
//                    })
                    .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                        Log.d(LOG_TAG,"Echo Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }

    private fun fadeInWithSpeedAndEffect(){
        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
                    .fadeIn(OnFileComplete {
                        Log.d(LOG_TAG,"Fade In Audio Success !")
                    })

                    .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                        Log.d(LOG_TAG,"Echo Audio Success !")
                    })
                    .changeAudioSpeed(eSpeed!!, OnFileComplete {
                        Log.d(LOG_TAG,"Speed Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }

    private fun fadeOutOnly(){
        Log.d(LOG_TAG, cachePath)
        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
                    .fadeOut(eEndTime - eStartTime, OnFileComplete {
                        Log.d(LOG_TAG,"Fade Out Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }

    private fun fadeOutWithSpeed(){
        Log.d(LOG_TAG, cachePath)
        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
                    .fadeOut(eEndTime - eStartTime, OnFileComplete {
                        Log.d(LOG_TAG,"Fade Out Audio Success !")
                    })
                    .changeAudioSpeed(eSpeed!!, OnFileComplete {
                        Log.d(LOG_TAG,"Speed Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }

    private fun fadeOutWithSpeedAndEffect(){
        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
                    .fadeOut(eEndTime - eStartTime, OnFileComplete {
                        Log.d(LOG_TAG,"Fade Out Audio Success !")
                    })
                    .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                        Log.d(LOG_TAG,"Echo Audio Success !")
                    })
                    .changeAudioSpeed(eSpeed!!, OnFileComplete {
                        Log.d(LOG_TAG,"Speed Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }

    private fun fadeOutWithEffect(){
        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
                    .fadeOut(eEndTime - eStartTime, OnFileComplete {
                        Log.d(LOG_TAG,"Fade Out Audio Success !")
                    })
//                    .removeVocal(OnFileComplete {
//                        Log.d(LOG_TAG,"Remove Vocal  Audio Success !")
//                    })
                    .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                        Log.d(LOG_TAG,"Echo Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }

    private fun fadeInAndOutOnly(){

        Log.d(LOG_TAG, cachePath)

        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
                    .fadeInAndOut(eEndTime - eStartTime, OnFileComplete {
                        Log.d(LOG_TAG,"Fade In/Out Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }

    private fun fadeInAndOutWithSpeed(){

        Log.d(LOG_TAG, cachePath)

        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
                    .fadeInAndOut(eEndTime - eStartTime, OnFileComplete {
                        Log.d(LOG_TAG,"Fade In/Out Audio Success !")
                    })
                    .changeAudioSpeed(eSpeed!!, OnFileComplete {
                        Log.d(LOG_TAG,"Speed Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }


    private fun fadeInAndOutWithEffect(){
        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
                    .fadeInAndOut(eEndTime - eStartTime, OnFileComplete {
                        Log.d(LOG_TAG,"Fade In/Out Audio Success !")
                    })
//                    .removeVocal(OnFileComplete {
//                        Log.d(LOG_TAG,"Remove Vocal  Audio Success !")
//                    })
                    .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                        Log.d(LOG_TAG,"Echo Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }

    private fun speedOnly(){
        Log.d(LOG_TAG,"Speed : $eSpeed")

        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
                    .changeAudioSpeed(eSpeed!!, OnFileComplete {
                        Log.d(LOG_TAG,"Speed Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }

    private fun effectOnly(){
        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
//                    .removeVocal(OnFileComplete {
//                        Log.d(LOG_TAG,"Remove Vocal  Audio Success !")
//                    })
                    .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                        Log.d(LOG_TAG,"Echo Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }

    private fun effectWithSpeed(){
        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
//                    .removeVocal(OnFileComplete {
//                        Log.d(LOG_TAG,"Remove Vocal  Audio Success !")
//                    })
                    .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                        Log.d(LOG_TAG,"Echo Audio Success !")
                    })
                    .changeAudioSpeed(eSpeed!!, OnFileComplete {
                        Log.d(LOG_TAG,"Speed Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }

    private fun allEffect(){

        doAsync {
            try {
                AudioTool.getInstance(context)
                    .withAudio(File(cachePath))
//                    .removeVocal(OnFileComplete {
//                        Log.d(LOG_TAG,"Remove Vocal  Audio Success !")
//                    })
                    .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                        Log.d(LOG_TAG,"Echo Audio Success !")
                    })
                    .changeAudioSpeed(eSpeed!!, OnFileComplete {
                        Log.d(LOG_TAG,"Speed Audio Success !")
                    })
                    .fadeInAndOut(eEndTime - eStartTime, OnFileComplete {
                        Log.d(LOG_TAG,"Fade In/Out Audio Success !")
                    })
                    .saveCurrentTo(effectPath)
                    .release()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            uiThread {
                listener?.success(effectPath)
            }
        }
    }

    /**
     * Save
     * **/

    fun saveAudioEffectToMyTone(path:String,
                                startTime:Int,
                                endTime:Int,
                                trimTime:Int,
                                isFadeIn:Boolean,
                                isFadeOut:Boolean,
                                isEffect:Boolean,
                                speed: Float?,
                                nameInput:String,
                                lis: AudioSaveEffectCallback){

        this.eStartTime = startTime
        this.eEndTime = endTime
        this.eTrimTime = trimTime
        this.eSpeed = speed
        this.listenerSave = lis

        Log.d(LOG_TAG,"Cut Start Time : $startTime")
        Log.d(LOG_TAG,"Cut End Time : $endTime")
        Log.d(LOG_TAG,"My Tone Path : ${context?.let {
            FileUtil.getPath(it, nameInput)
        }}")

        var nameFinal = ""
        context?.let {
            val filePath = FileUtil.getPath(it, nameInput)
            val file = File(filePath)
            if(file.exists()){
                Log.d(LOG_TAG,"Exist !")
                val newFile = FileUtil.getUniqueFileName(file)
                if (newFile != null) {
                    nameFinal = newFile?.name.substring(0, newFile?.name.indexOf("."))
                }
            }else{
                nameFinal = nameInput
            }

            Log.d(LOG_TAG,"New Name : $nameFinal")

            doAsync {
                try {
                    if(isFadeIn && !isFadeOut && !isEffect && eSpeed == null){

                        /**
                         * Fade In Only
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .fadeIn(OnFileComplete {
                                Log.d(LOG_TAG,"Fade In Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()

                    }else if (isFadeIn && !isFadeOut && isEffect){

                        /**
                         * Fade In With Effect
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .fadeIn(OnFileComplete {
                                Log.d(LOG_TAG,"Fade In Audio Success !")
                            })
                            .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                                Log.d(LOG_TAG,"Echo Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()

                    }else if (isFadeIn && !isFadeOut && !isEffect && eSpeed != null){

                        /**
                         * Fade In With Speed
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .fadeIn(OnFileComplete {
                                Log.d(LOG_TAG,"Fade In Audio Success !")
                            })
                            .changeAudioSpeed(eSpeed!!, OnFileComplete {
                                Log.d(LOG_TAG,"Speed Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()

                    }else if (isFadeIn && !isFadeOut && isEffect && eSpeed != null){

                        /**
                         * Fade In With Speed And Effect
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .fadeIn(OnFileComplete {
                                Log.d(LOG_TAG,"Fade In Audio Success !")
                            })
                            .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                                Log.d(LOG_TAG,"Echo Audio Success !")
                            })
                            .changeAudioSpeed(eSpeed!!, OnFileComplete {
                                Log.d(LOG_TAG,"Speed Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()

                    } else if (!isFadeIn && isFadeOut && !isEffect && eSpeed == null){

                        /**
                         * Fade Out Only
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .fadeOut(eEndTime - eStartTime, OnFileComplete {
                                Log.d(LOG_TAG,"Fade Out Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()

                    } else if (!isFadeIn && isFadeOut && isEffect){

                        /**
                         * Fade Out With Effect
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                                Log.d(LOG_TAG,"Echo Audio Success !")
                            })
                            .fadeOut(eEndTime - eStartTime, OnFileComplete {
                                Log.d(LOG_TAG,"Fade Out Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()

                    } else if (!isFadeIn && isFadeOut && !isEffect && eSpeed != null){

                        /**
                         * Fade Out With Speed
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .fadeOut(eEndTime - eStartTime, OnFileComplete {
                                Log.d(LOG_TAG,"Fade Out Audio Success !")
                            })
                            .changeAudioSpeed(eSpeed!!, OnFileComplete {
                                Log.d(LOG_TAG,"Speed Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()

                    }else if (!isFadeIn && isFadeOut && isEffect && eSpeed != null){

                        /**
                         * Fade Out With Speed And Effect
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .fadeOut(eEndTime - eStartTime, OnFileComplete {
                                Log.d(LOG_TAG,"Fade Out Audio Success !")
                            })
                            .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                                Log.d(LOG_TAG,"Echo Audio Success !")
                            })
                            .changeAudioSpeed(eSpeed!!, OnFileComplete {
                                Log.d(LOG_TAG,"Speed Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()

                    } else if (isFadeIn && isFadeOut && !isEffect && eSpeed == null){

                        /**
                         * Fade In And Out Only
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .fadeInAndOut(eEndTime - eStartTime, OnFileComplete {
                                Log.d(LOG_TAG,"Fade In/Out Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()

                    } else if (isFadeIn && isFadeOut && isEffect){

                        /**
                         * Fade In And Out With Effect
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .fadeInAndOut(eEndTime - eStartTime, OnFileComplete {
                                Log.d(LOG_TAG,"Fade In/Out Audio Success !")
                            })
                            .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                                Log.d(LOG_TAG,"Echo Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()

                    }else if (isFadeIn && isFadeOut && !isEffect && eSpeed != null){

                        /**
                         * Fade In And Out With Speed
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .fadeInAndOut(eEndTime - eStartTime, OnFileComplete {
                                Log.d(LOG_TAG,"Fade In/Out Audio Success !")
                            })
                            .changeAudioSpeed(eSpeed!!, OnFileComplete {
                                Log.d(LOG_TAG,"Speed Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()

                    } else if (!isFadeIn && !isFadeOut && !isEffect && eSpeed != null){

                        /**
                         * Speed Only
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .changeAudioSpeed(eSpeed!!, OnFileComplete {
                                Log.d(LOG_TAG,"Speed Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()

                    }else if (!isFadeIn && !isFadeOut && isEffect && eSpeed == null){

                        /**
                         * Effect Only
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                                Log.d(LOG_TAG,"Echo Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()

                    }else if (!isFadeIn && !isFadeOut && isEffect && eSpeed != null){

                        /**
                         * Effect With Speed
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                                Log.d(LOG_TAG,"Echo Audio Success !")
                            })
                            .changeAudioSpeed(eSpeed!!, OnFileComplete {
                                Log.d(LOG_TAG,"Speed Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()

                    }else if (isFadeIn && isFadeOut && isEffect && eSpeed != null){

                        /**
                         * All Effect
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime, OnFileComplete {
                                Log.d(LOG_TAG,"Cut Audio Success !")
                            })
                            .applyEchoEffect(Echo.ECHO_FEW_MOUNTAINS, OnFileComplete {
                                Log.d(LOG_TAG,"Echo Audio Success !")
                            })
                            .fadeInAndOut(eEndTime - eStartTime, OnFileComplete {
                                Log.d(LOG_TAG,"Fade In/Out Audio Success !")
                            })
                            .changeAudioSpeed(eSpeed!!, OnFileComplete {
                                Log.d(LOG_TAG,"Speed Audio Success !")
                            })
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()
                    }else{

                        /**
                         * Save File With No Effect
                         * **/

                        AudioTool.getInstance(context)
                            .withAudio(File(path))
                            .cutAudio(startTime, endTime - startTime) {
                                Log.d(LOG_TAG, "Cut Audio Success !")
                            }
                            .saveCurrentTo(FileUtil.getPath(it, nameFinal))
                            .release()
                    }

                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
                uiThread {
                    listenerSave?.success()
                }
            }
        }
    }
}
