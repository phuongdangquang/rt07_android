package com.ow.rt07.ui.main.maker.setRingtone

import android.content.Context
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.R
import com.ow.rt07.databinding.SetRingtoneItemBinding
import kotlinx.android.parcel.Parcelize

enum class SetRingtoneState {
    SET_RINGTONE,
    SET_NOTIFICATION,
    SET_ALARM_SOUND,
    SET_CONTACT_RINGTONE,
    NOTHING,
}

@Parcelize
data class SetItem(
    val state: SetRingtoneState,
    val title: String,
    val icon:Int,
): Parcelable

interface SetRingtoneAdapterInterface {
    fun onItemClicked(index: Int, state: SetRingtoneState)
}

private const val LOG_TAG  = "SetRingtoneAdapter"

class SetRingtoneAdapter(var context: Context, private val listener: SetRingtoneAdapterInterface) : RecyclerView.Adapter<SetRingtoneAdapter.SetRingtoneItemViewHolder>() {

    private val listItems:ArrayList<SetItem> = arrayListOf(
        SetItem(SetRingtoneState.SET_RINGTONE, "Set Ringtone", R.drawable.set_ringtone_as_ringtone),
        SetItem(SetRingtoneState.SET_NOTIFICATION, "Set Notification", R.drawable.set_ringtone_as_notification),
        SetItem(SetRingtoneState.SET_ALARM_SOUND, "Set Alarm sound", R.drawable.ic_set_ringtone_as_alarm),
        SetItem(SetRingtoneState.SET_CONTACT_RINGTONE, "Set Contact ringtone", R.drawable.ic_set_ringtone_as_contact)
    )

    class SetRingtoneItemViewHolder(val binding: SetRingtoneItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SetRingtoneItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = SetRingtoneItemBinding.inflate(layoutInflater, parent,false)
        return SetRingtoneItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SetRingtoneItemViewHolder, position: Int) {
        val item = listItems?.get(position)

        if (position == listItems.size - 1){
            holder.binding.seperatorBottomsheet.visibility = View.INVISIBLE
        }

        if (item != null) {
            holder.binding.txtName.text = listItems[position].title
            holder.binding.imgIcon.setImageResource(listItems[position].icon)
        }

        holder.binding.contentView.setOnClickListener {
            item?.let { it1 -> listener.onItemClicked(position, it1?.state) }
        }
    }

    override fun getItemCount(): Int {
        return listItems?.size ?: 0
    }
}