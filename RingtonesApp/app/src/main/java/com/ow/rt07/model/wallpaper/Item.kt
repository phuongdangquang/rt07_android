package com.ow.rt07.model.wallpaper

data class Item(
    val id: Int,
    val image: String,
    val image_4k: String,
    val image_thumb: String,
    val m4v: String,
    val m4v_4k: String,
    val name: String,
    val reward_ads: Int,
    val tag: String,
    val view: Int,
    val vip: Int
)