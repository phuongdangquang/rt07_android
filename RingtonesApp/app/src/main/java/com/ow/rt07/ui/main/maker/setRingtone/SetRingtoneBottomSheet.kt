package com.ow.rt07.ui.main.maker.setRingtone
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.andrefrsousa.superbottomsheet.SuperBottomSheetFragment
import com.ow.rt07.databinding.SetRingtoneBottomSheetBinding

private const val LOG_TAG = "Set Ringtone BottomSheet"

class SetRingtoneBottomSheet: SuperBottomSheetFragment(){

    private lateinit var binding: SetRingtoneBottomSheetBinding
    private lateinit var adapter: SetRingtoneAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = SetRingtoneBottomSheetBinding.inflate(inflater, container, false)
        binding.listItem.layoutManager =
            LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)


        setUpView()

        return binding.root
    }

    private fun setUpView(){
        adapter = context?.let {
            SetRingtoneAdapter(it, object : SetRingtoneAdapterInterface {
                override fun onItemClicked(index: Int, state: SetRingtoneState) {
                    when (state){
                        SetRingtoneState.SET_RINGTONE ->{
                            Log.d(LOG_TAG,"SET_RINGTONE")
                        }
                        SetRingtoneState.SET_NOTIFICATION ->{
                            Log.d(LOG_TAG,"SET_NOTIFICATION")
                        }
                        SetRingtoneState.SET_ALARM_SOUND ->{
                            Log.d(LOG_TAG,"SET_ALARM_SOUND")
                        }
                        SetRingtoneState.SET_CONTACT_RINGTONE ->{
                            Log.d(LOG_TAG,"SET_CONTACT_RINGTONE")
                        }
                    }
                }
            })
        }!!

        binding.listItem.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.listItem.adapter = adapter
    }

    override fun getCornerRadius(): Float {
        return 20f
    }

    override fun getExpandedHeight(): Int {
        return -2
    }

    override fun getPeekHeight(): Int {
        return 673
    }
}