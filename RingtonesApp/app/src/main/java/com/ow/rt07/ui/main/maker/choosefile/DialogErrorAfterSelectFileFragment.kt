package com.ow.rt07.ui.main.maker.choosefile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.ow.rt07.R
import com.ow.rt07.databinding.ErrorAfterSelecteFileFragmentBinding


class DialogErrorAfterSelectFileFragment(): DialogFragment() {

    lateinit var binding: ErrorAfterSelecteFileFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.FullScreenDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ErrorAfterSelecteFileFragmentBinding.inflate(inflater, container, false)

        binding.btnClose.setOnClickListener {
            dismiss()
        }
        binding.btnTryAgain.setOnClickListener {

            dismiss()
        }

        return binding.root
    }
}