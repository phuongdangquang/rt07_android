package com.ow.rt07.ui.main.ringtones

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.ow.rt07.databinding.AllCategoryRingtoneFragmentBinding
import com.ow.rt07.ui.main.wallpapers.AdapterRecyclerViewInterface
import com.ow.rt07.utils.base.BaseFragment

class AllCategoryRingtoneFragment : BaseFragment() {

    private lateinit var binding: AllCategoryRingtoneFragmentBinding

    //    private lateinit var viewModel: DetailCategoryViewModel
    val args: AllCategoryRingtoneFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = AllCategoryRingtoneFragmentBinding.inflate(inflater, container, false)
//        viewModel = ViewModelProvider(this).get(DetailCategoryViewModel::class.java)
        actionBack()
        return binding.root
    }

    override fun setUpView() {
        super.setUpView()
        binding.recyclerViewAllCategoryRingtones.layoutManager = GridLayoutManager(this.context, 2)
        binding.recyclerViewAllCategoryRingtones.adapter =
            AdapterItemCategoryRingtone(args.category, object : AdapterRecyclerViewInterface {
                override fun onItemClicked(index: Int) {
                    val action =
                        AllCategoryRingtoneFragmentDirections.actionNavigationAllCategoryRingtoneFragmentToNavigationDetailCategoryRingtone(
                            args.category[index].id,
                            args.category[index].name
                        )
                    findNavController().navigate(action)
                }
            })
    }

    fun actionBack() {
        binding.btnBackAllCateRingtone.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}