package com.ow.rt07.ui.main.maker.mytone

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ow.rt07.MainActivity
import com.ow.rt07.databinding.MyToneFragmentBinding
import com.ow.rt07.ui.main.maker.info.InfoFragment
import com.ow.rt07.utils.base.BaseFragment
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File

private const val LOG_TAG = "MyTone Fragment"

class MyToneFragment : BaseFragment() {

    private lateinit var binding:MyToneFragmentBinding
    private lateinit var viewModel: MyToneViewModel
    private lateinit var adapter:MyToneAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        Log.d(LOG_TAG,"On Create View !")

        (activity as MainActivity).hideBottomNavigation()
        viewModel = ViewModelProvider(this).get(MyToneViewModel::class.java)
        binding = MyToneFragmentBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun setUpView() {
        super.setUpView()

        adapter = context?.let {
            MyToneAdapter(it, object : MyToneAdapterInterface{
                override fun onItemClicked(index: Int, title:String) {

                    Log.d(LOG_TAG,"POSITION : $index")

                    playRingtones(index, title)
                }
            })
        }!!
        binding.listTonesView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.listTonesView.adapter = adapter

        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.btnInfo.setOnClickListener {
            Log.d(LOG_TAG,"Info")
            InfoFragment().show(requireActivity().supportFragmentManager, "InfoFragment")
        }
    }

    override fun bind() {
        super.bind()
        Handler(Looper.getMainLooper()).postDelayed({
            getTones()
        }, 500)
    }

    private fun getTones(){
        val files: Array<out File>? = File(context?.getExternalFilesDir(null)?.absolutePath + "/MyTones" ?: "").listFiles()
        if (files != null) {
            Log.d(LOG_TAG,"Count : ${files.size}")
        }
        adapter.setItems(files)
    }

    private fun playRingtones(index:Int, title:String){
//        val action = MyToneFragmentDirections.actionMyToneFragmentToNavigationDetailRingtone(index, title)
//        findNavController().navigate(action)
    }
}