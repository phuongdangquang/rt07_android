package com.ow.rt07.utils.audioplayer

import android.content.Context
import android.media.MediaPlayer
import android.media.MediaPlayer.OnPreparedListener
import android.net.Uri
import android.util.Log
import com.ow.rt07.utils.audioplayer.exception.AudioException
import com.ow.rt07.utils.audioplayer.exception.PermissionDeniedException
import com.ow.rt07.utils.audioplayer.exception.PlayerDataSourceException
import java.io.IOException
import java.util.*

const val VISUALIZATION_INTERVAL = 1000 / 25L

interface RTPlayerCallback {
    fun onStartPlay()
    fun onStopPlay()
    fun onPausePlay()
    fun onSeekPlay()
    fun onPlayProgress(mills: Long)
    fun onError(throwable: AudioException?)
}

class RTAudioPlayer : OnPreparedListener {

    var mediaPlayer: MediaPlayer? = null
    private var timerProgress: Timer? = null
    private var isPrepared = false
    private var context:Context? = null
    var isPause = false
    var isTouched = false
    private var pauseTime: Long = 0
    var pausePos: Long = 0
    private var dataSource: String? = null

    var listener:RTPlayerCallback? = null

    constructor(context: Context, lis: RTPlayerCallback) {

        listener = lis
        this.context = context
    }

    fun setData(path: String) {
        if (mediaPlayer != null && dataSource != null && dataSource == path) {
            //Do nothing
        } else {
            dataSource = path
            setPreparePlayer()
        }
    }

    private fun setPreparePlayer() {
        if (dataSource != null) {
            try {
                isPrepared = false

                mediaPlayer = MediaPlayer()
                try {
                    context?.let {
                        mediaPlayer?.setDataSource(dataSource)
                    }
                    mediaPlayer!!.prepare()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            } catch (e: IOException) {
                if (e.message?.contains("Permission denied") == true) {
                    listener?.onError(PermissionDeniedException())
                } else {
                    listener?.onError(PlayerDataSourceException())
                }
            } catch (e: IllegalArgumentException) {
                if (e.message?.contains("Permission denied") == true) {
                    listener?.onError(PermissionDeniedException())
                } else {
                    listener?.onError(PlayerDataSourceException())
                }
            } catch (e: IllegalStateException) {
                if (e.message?.contains("Permission denied") == true) {
                    listener?.onError(PermissionDeniedException())
                } else {
                    listener?.onError(PlayerDataSourceException())
                }
            } catch (e: SecurityException) {
                if (e.message?.contains("Permission denied") == true) {
                    listener?.onError(PermissionDeniedException())
                } else {
                    listener?.onError(PlayerDataSourceException())
                }
            }
        }
    }

    fun playOrPause() {
        try {
            if (mediaPlayer != null) {
                if (mediaPlayer?.isPlaying == true) {
                    pause()
                } else {
                    if (!isTouched){
                        isPause = false
                        if (!isPrepared) {

                            Log.e("TAG", "!isPrepared")

                            try {
                                mediaPlayer?.setOnPreparedListener(this)
                                mediaPlayer?.prepareAsync()
                            } catch (ex: IllegalStateException) {
                                setPreparePlayer()
                                mediaPlayer?.setOnPreparedListener(this)
                                try {
                                    mediaPlayer?.prepareAsync()
                                } catch (e: IllegalStateException) {
                                    setPreparePlayer()
                                }
                            }
                        } else {
                            mediaPlayer?.start()
                            mediaPlayer?.seekTo(pausePos.toInt())
                            listener?.onStartPlay()

                            mediaPlayer?.setOnCompletionListener {
                                stop()
                                listener?.onStopPlay()
                            }
                            setProgress()
                        }
                        pausePos = 0
                    }else{
                        Log.d("TAG","Seek in here !")
                        isTouched = false
                        listener?.onSeekPlay()
                    }
                }
            }else{
                Log.e("TAG", "Player is null")
            }
        } catch (e: IllegalStateException) {
            Log.e("TAG", "Player is not initialized!")
        }
    }

    override fun onPrepared(mp: MediaPlayer) {
        if (mediaPlayer !== mp) {
            mediaPlayer?.stop()
            mediaPlayer?.release()
            mediaPlayer = mp
        }
        isPrepared = true
        mediaPlayer?.start()
        mediaPlayer?.seekTo(pauseTime.toInt())
        listener?.onStartPlay()
        mediaPlayer?.setOnCompletionListener {
            stop()
            listener?.onStopPlay()
        }
        setProgress()
    }

    fun seek(mills: Long) {
        pauseTime = mills
        pausePos = mills

        try {
            if (mediaPlayer != null && mediaPlayer?.isPlaying == false) {

                Log.d("TAG","$pausePos")

                isPause = false
                mediaPlayer?.start()
                mediaPlayer?.seekTo(pausePos.toInt())
                listener?.onStartPlay()
                mediaPlayer?.setOnCompletionListener {
                    stop()
                    listener?.onStopPlay()
                }
                setProgress()
            }
        } catch (e: IllegalStateException) {
            Log.e("TAG", "Player is not initialized!")
        }
    }

    private fun setProgress(){
        timerProgress = Timer()
        timerProgress?.schedule(object : TimerTask() {
            override fun run() {
                try {
                    if (mediaPlayer != null && mediaPlayer?.isPlaying == true) {
                        val curPos = mediaPlayer?.currentPosition ?: 0
                        listener?.onPlayProgress(curPos.toLong())
                    }
                } catch (e: IllegalStateException) {
                    Log.e("TAG", "Player is not initialized!")
                }
            }
        }, 0, VISUALIZATION_INTERVAL)
    }


    fun pause() {
        if (timerProgress != null) {
            timerProgress?.cancel()
            timerProgress?.purge()
        }
        if (mediaPlayer != null) {
            if (mediaPlayer?.isPlaying == true) {
                mediaPlayer?.pause()
                listener?.onPausePlay()
                pauseTime = (mediaPlayer?.currentPosition ?: 0).toLong()
                isPause = true
                pausePos = pauseTime
            }
        }
    }

    fun stop() {
        if (timerProgress != null) {
            timerProgress?.cancel()
            timerProgress?.purge()
        }
        if (mediaPlayer != null) {
            mediaPlayer?.stop()
            mediaPlayer?.setOnCompletionListener(null)
            isPrepared = false
            listener?.onStopPlay()
            mediaPlayer?.currentPosition
            pauseTime = 0
        }
        isPause = false
        pausePos = 0
    }

    val isPlaying: Boolean
        get() {
            try {
                return mediaPlayer != null && mediaPlayer?.isPlaying == true
            } catch (e: IllegalStateException) {
                Log.e("TAG", "Player is not initialized!")
            }
            return false
        }

    fun release() {
        stop()
        if (mediaPlayer != null) {
            mediaPlayer?.release()
            mediaPlayer = null
        }
        isPrepared = false
        isPause = false
        dataSource = null
    }
}