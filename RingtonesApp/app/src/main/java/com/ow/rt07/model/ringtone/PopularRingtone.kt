package com.ow.rt07.model.ringtone

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PopularRingtone(
    val code: Int,
    val `data`: ArrayList<Ringtone>,
    val message: String,
    val next_page_url: String
): Parcelable