package com.ow.rt07.ui.main.ringtones

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.databinding.RowPopularRingtonesBinding
import com.ow.rt07.databinding.RowTopCategoryRingtonesBinding
import com.ow.rt07.model.ringtone.CategoryRingtone
import com.ow.rt07.model.ringtone.Ringtone
import com.ow.rt07.model.wallpaper.CateWallpaper
import com.ow.rt07.model.wallpaper.Wallpapers
import com.ow.rt07.network.RTApi
import com.ow.rt07.ui.main.wallpapers.AdapterRecyclerViewInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdapterRingtonesCategory(var category: ArrayList<CategoryRingtone> = arrayListOf(), var popular: ArrayList<Ringtone> = arrayListOf(), val listener: ItemRingtoneInterFace) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), ItemRingtoneInterFace {

    var adapterRingtone: AdapterItemPopularRingtones? = null
    override fun onClickPlayRingtone(position: Int) {
        listener.onClickPlayRingtone(position)
        Log.e("position", "postion"+ position)
    }

    class RowCategoryHolder(val binding: RowTopCategoryRingtonesBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)

    class RowPopularHolder(val binding: RowPopularRingtonesBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)



    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return 1
        } else if (position == 1) {
            return 2
        } else {
            return 2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 1) {
            val binding =
                RowTopCategoryRingtonesBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return RowCategoryHolder(binding, parent)
        } else if (viewType == 2) {
            val binding =
                RowPopularRingtonesBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return RowPopularHolder(binding, parent)
        } else {
            val binding =
                RowTopCategoryRingtonesBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return RowCategoryHolder(binding, parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
//category
            is RowCategoryHolder -> {
                holder.binding.recycleViewCategoryRingtones.layoutManager =
                    LinearLayoutManager(
                        holder.parent.context,
                        LinearLayoutManager.HORIZONTAL,
                        false
                    )
                holder.binding.recycleViewCategoryRingtones.adapter =
                    AdapterItemAllCategoryRingtones(
                        category,
                        object : AdapterRecyclerViewInterface {
                            override fun onItemClicked(position: Int) {

                                val action =
                                    RingtonesFragmentDirections.actionNavigationRingtoneToNavigationDetailCategoryRingtone(
                                        category[position].id,
                                        category[position].name
                                    )
                                Navigation.findNavController(holder.itemView).navigate(action)
                            }
                        })
            }
//popular
            is RowPopularHolder -> {
                adapterRingtone =  AdapterItemPopularRingtones(popular, object : AdapterRecyclerViewInterface {
                    override fun onItemClicked(index: Int) {
                        listener.onClickItemRingtone(index)
                    }
                },this)
                holder.binding.recyclerViewPopularRingtones.layoutManager =
                    LinearLayoutManager(
                        holder.parent.context,
                        LinearLayoutManager.VERTICAL,
                        false
                    )
                holder.binding.recyclerViewPopularRingtones.adapter = adapterRingtone
            }
        }
    }

    override fun getItemCount(): Int {
        return 2
    }
}