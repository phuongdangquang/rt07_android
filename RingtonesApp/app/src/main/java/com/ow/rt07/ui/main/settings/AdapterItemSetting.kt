package com.ow.rt07.ui.main.settings

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.R
import com.ow.rt07.databinding.RowItemSettingBinding


class AdapterItemSetting(val descriptions: ArrayList<String>, val image: ArrayList<Int>): RecyclerView.Adapter<AdapterItemSetting.ItemSettingHolder>() {
    class ItemSettingHolder(val binding: RowItemSettingBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemSettingHolder {
        val binding =
            RowItemSettingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemSettingHolder(binding, parent)
    }

    override fun onBindViewHolder(holder: ItemSettingHolder, position: Int) {
        holder.binding.imgSetting.setImageResource(image[position])
        holder.binding.txtDescriptionSetting.text = descriptions[position]
        if (position == descriptions.lastIndex){
            holder.binding.seperatorView.visibility = View.INVISIBLE
        }
    }

    override fun getItemCount(): Int {
        return return descriptions.size
    }
}