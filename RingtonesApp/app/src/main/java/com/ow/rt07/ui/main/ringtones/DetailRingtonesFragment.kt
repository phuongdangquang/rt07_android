package com.ow.rt07.ui.main.ringtones

import android.app.Activity
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.Ringtone
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.ow.rt07.MainActivity
import com.ow.rt07.databinding.DetailRingtonesFragmentBinding
import com.ow.rt07.utils.audioplayer.exception.AudioException
import com.ow.rt07.utils.base.BaseFragment
import com.ow.rt07.utils.minute
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.detail_ringtones_fragment.*
import kotlinx.android.synthetic.main.super_tune_maker_fragment.view.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.time.Duration.Companion.minutes

private const val LOG_TAG = "Detail RingtonesFragment"

class DetailRingtonesFragment : BaseFragment() {

    private val args: DetailRingtonesFragmentArgs by navArgs()
    private lateinit var binding: DetailRingtonesFragmentBinding
    var audioManager: PlayerManager? = null
    var currentPosition: Int = 0
    var ringtone: ArrayList<Ringtone> = arrayListOf()
    var duration: Int = 0
    var url: String = ""
    var title: String = ""
    var currrentTime: Long = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DetailRingtonesFragmentBinding.inflate(inflater, container, false)
        (activity as MainActivity).hideBottomNavigation()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.setFocusableInTouchMode(true);

        view.requestFocus()
        view.setOnKeyListener(object: View.OnKeyListener{
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if( keyCode == KeyEvent.KEYCODE_BACK )
                {
                    audioManager?.release()
                    findNavController().popBackStack()
                    return true;
                }
                return false;
            }
        })
    }

    override fun setUpView() {
        super.setUpView()
        url = args.itemRingtone[currentPosition].m4r
        currentPosition = args.currentPosition
        duration = args.itemRingtone[currentPosition].duration
        title = args.itemRingtone[currentPosition].title

        binding.lbTitle.text = title
        binding.txtDurationTime.text = duration.minute()
        binding.seekBar.max = duration
        binding.btnPlay.visibility = View.INVISIBLE
        binding.btnPause.visibility = View.VISIBLE
        action()
    }

    override fun bind() {
        audioSetData(args.itemRingtone[currentPosition].m4r)
    }

    fun action() {
        //play
        binding.btnPlay.setOnClickListener {
            if (currrentTime > 0) {
                audioManager?.playOrPause()
            } else {
                audioManager?.setData(url)
            }
        }
        //pause
        binding.btnPause.setOnClickListener {
            audioManager?.playOrPause()
        }
        //Next
        binding.btnNext.setOnClickListener {
            nextRingtone()
        }
        //Previous
        binding.btnPrevious.setOnClickListener {
            prevRingtone()
        }
        //Back
        binding.btnBackDetailRingtone.setOnClickListener {
            audioManager?.release()
            findNavController().popBackStack()
        }
    }


    fun nextRingtone() {
        if (currentPosition < (args.itemRingtone.size - 1)) {
            audioManager?.release()
            (context as Activity).runOnUiThread {
                currentPosition += 1
                binding.seekBar.max = args.itemRingtone[currentPosition].duration
                binding.lbTitle.text = args.itemRingtone[currentPosition].title
                binding.txtDurationTime.text = args.itemRingtone[currentPosition].duration.minute()
                audioManager?.setData(args.itemRingtone[currentPosition].m4r)
                audioManager?.playOrPause()
            }
        } else {
            currentPosition = 0
        }
    }

    fun prevRingtone() {
        if (currrentTime < 5000) {
            // giay nho hon 5 -> previous
            if (currentPosition > 0) {
                Log.e("allow prev", "allow")
                audioManager?.release()
                currentPosition -= 1
                binding.seekBar.max = args.itemRingtone[currentPosition].duration
                binding.lbTitle.text = args.itemRingtone[currentPosition].title
                binding.txtDurationTime.text =
                    args.itemRingtone[currentPosition].duration.minute()
                audioManager?.setData(args.itemRingtone[currentPosition].m4r)
                audioManager?.playOrPause()
            } else {
                Log.e("current ringtone", " = 0")
                binding.seekBar.max = args.itemRingtone[currentPosition].duration
                binding.lbTitle.text = args.itemRingtone[currentPosition].title
                binding.txtDurationTime.text =
                    args.itemRingtone[currentPosition].duration.minute()
                audioManager?.seek(0)
            }
        } else {
            //lon hon 5 seek ve 0
            Log.e("seek to", "0")
            binding.seekBar.max = args.itemRingtone[currentPosition].duration
            binding.lbTitle.text = args.itemRingtone[currentPosition].title
            binding.txtDurationTime.text = args.itemRingtone[currentPosition].duration.minute()
            audioManager?.seek(0)
        }
    }

    fun audioSetData(url: String) {
        audioManager = context?.let {
            PlayerManager(it, object : PlayerManagerCallback {
                override fun onPlay() {
                    Log.e("start", "start")
                    (context as Activity).runOnUiThread {
                        binding.btnPlay.visibility = View.INVISIBLE
                        binding.btnPause.visibility = View.VISIBLE
                    }
                }

                override fun onComplete() {
                    Log.e("onStopPlay", "")
                    (context as Activity).runOnUiThread {
                        currrentTime = 0
                        binding.btnPlay.visibility = View.VISIBLE
                        binding.btnPause.visibility = View.INVISIBLE
                        binding.seekBar.progress = 0
//                        binding.seekBar.setProgress(0, true)
                        binding.txtCurrentTime.text = "0:00"
                    }

                }

                override fun onPausePlay() {
                    Log.e("onPausePlay", "")
                    (context as Activity).runOnUiThread {
                        binding.btnPlay.visibility = View.VISIBLE
                        binding.btnPause.visibility = View.INVISIBLE
                    }
                }

                override fun onSeekPlay() {
                    (context as Activity).runOnUiThread {
                        binding.btnPlay.visibility = View.VISIBLE
                        binding.btnPause.visibility = View.INVISIBLE
                    }
                }

                override fun onPrepared() {
                    audioManager?.playOrPause()
                }

                override fun onPreparing() {
                }

                override fun onPlayProgress(mills: Long) {
                    currrentTime = mills
                    Log.e("onPlayProgress", "" + mills)
                    val nowTime = currrentTime / 1000
                    (context as Activity).runOnUiThread {
                        binding.seekBar.progress = nowTime.toInt()
//                        binding.seekBar.setProgress(nowTime.toInt(), true)
                        binding.txtCurrentTime.text = nowTime.toInt().minute()
                    }
                }

                override fun onError(throwable: AudioException?) {
                    Log.e("onplay", "")
                }

            })
        }
        audioManager?.setData(url)
    }
}


