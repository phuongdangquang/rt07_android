package com.ow.rt07.model.wallpaper

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Wallpapers(
    val categories: List<CategoryX> = listOf(),
    val load_more_category: String= ""
): Parcelable