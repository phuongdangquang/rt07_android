package com.ow.rt07.ui.main.maker.mix

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.ow.rt07.MainActivity
import com.ow.rt07.databinding.MixFragmentBinding
import com.ow.rt07.utils.base.BaseFragment

class MixFragment : BaseFragment() {

    private lateinit var binding:MixFragmentBinding
    private lateinit var viewModel: MixViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        Log.d("Mix Fragment","On Create View !")

        (activity as MainActivity).hideBottomNavigation()
        viewModel = ViewModelProvider(this).get(MixViewModel::class.java)
        binding = MixFragmentBinding.inflate(inflater, container,false)
        return binding.root
    }

    override fun setUpView() {
        super.setUpView()

        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.btnTunnerMaker.setOnClickListener {
            Log.d("Mix Fragment","Go To Tune Maker !")
        }

        binding.btnPickFile1.setOnClickListener {
            Log.d("Mix Fragment","Picker File 1 !")
        }

        binding.btnPickFile2.setOnClickListener {
            Log.d("Mix Fragment","Picker File 2 !")
        }
    }

    override fun bind() {
        super.bind()
    }
}