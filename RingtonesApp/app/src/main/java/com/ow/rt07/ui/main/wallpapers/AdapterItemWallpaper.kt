package com.ow.rt07.ui.main.wallpapers

import android.icu.number.NumberFormatter.with
import android.icu.number.NumberRangeFormatter.with
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.R
import com.ow.rt07.databinding.ItemWallpaperBinding
import com.ow.rt07.model.wallpaper.ItemX
import com.ow.rt07.model.wallpaper.Wallpapers
import com.squareup.picasso.Picasso

interface AdapterRecyclerViewInterface {
    fun onItemClicked(index: Int)
}


class AdapterItemWallpaper(
    var itemWallpaper: List<ItemX> = listOf() ,
    val listener: AdapterRecyclerViewInterface
) : RecyclerView.Adapter<AdapterItemWallpaper.ItemWallpaperHolder>() {

    class ItemWallpaperHolder(val binding: ItemWallpaperBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)


    fun setContent(value: List<ItemX>){
        itemWallpaper = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ItemWallpaperHolder {
        val binding =
            ItemWallpaperBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemWallpaperHolder(binding, parent)
    }

    override fun onBindViewHolder(holder: ItemWallpaperHolder, position: Int) {
        Picasso.get().load(itemWallpaper[position].image_thumb)
            .placeholder(R.drawable.img_item_category_default)
            .into(holder.binding.itemImgWallpaper)
        if (itemWallpaper[position].vip == 0){
            holder.binding.imgVip.visibility = View.INVISIBLE
        }else {
            holder.binding.imgVip.visibility = View.VISIBLE
        }
        holder.binding.root.setOnClickListener {
            listener.onItemClicked(position)
        }
    }

    override fun getItemCount(): Int {
        return itemWallpaper.size
    }


}