package com.ow.rt07.config

import android.content.Context
import android.os.Environment
import android.util.Log
import java.io.File

private const val LOG_TAG = "App Config"

object AppConfig{

    //App
    const val appName = ""
    const val packageName = ""

    //Iap

    //Term and Privacy
    const val link_privacy : String = ""
    const val link_terms : String = ""

    //Folder

    const val MY_TONE_FOLDER : String = "MyTones"

    //EndPoint

    fun endPointApi() : String{
        return "http://ringtone07-android.sboomtools.net:81"
    }
}
