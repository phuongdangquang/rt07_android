package com.ow.rt07.ui.main.settings

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.R
import com.ow.rt07.databinding.RowSettingBinding


class AdapterSetting: RecyclerView.Adapter<AdapterSetting.RowSettingHolder>() {
    class RowSettingHolder(val binding: RowSettingBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowSettingHolder {
        val binding =
            RowSettingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RowSettingHolder(binding, parent)
    }

    override fun onBindViewHolder(holder: RowSettingHolder, position: Int) {
        if (position == 0){
            holder.binding.txtTitleSetting.text = "Customer Service"
            val des: ArrayList<String> = arrayListOf("Send Feedback", "Rate and Review App", "Tell Friends")
            val images: ArrayList<Int> = arrayListOf(R.drawable.ic_send_feedback, R.drawable.ic_rate_app, R.drawable.ic_tell_friend)
            holder.binding.recyclerView.layoutManager = LinearLayoutManager(holder.parent.context, LinearLayoutManager.VERTICAL, false)
            holder.binding.recyclerView.adapter = AdapterItemSetting(des, images)
        }else if(position == 1){
            holder.binding.txtTitleSetting.text = "policy"
            val des: ArrayList<String> = arrayListOf("Terms of Use", "Privacy Policy")
            val images: ArrayList<Int> = arrayListOf(R.drawable.ic_term_of_use, R.drawable.ic_privacy_policy)
            holder.binding.recyclerView.layoutManager = LinearLayoutManager(holder.parent.context, LinearLayoutManager.VERTICAL, false)
            holder.binding.recyclerView.adapter = AdapterItemSetting(des, images)
        }else if(position == 2){
            holder.binding.txtTitleSetting.text = "Cache"
            val des: ArrayList<String> = arrayListOf("Clear Cache")
            val images: ArrayList<Int> = arrayListOf(R.drawable.ic_clear_cache)
            holder.binding.recyclerView.layoutManager = LinearLayoutManager(holder.parent.context, LinearLayoutManager.VERTICAL, false)
            holder.binding.recyclerView.adapter = AdapterItemSetting(des, images)
        }
    }

    override fun getItemCount(): Int {
       return 3
    }
}