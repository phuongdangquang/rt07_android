package com.ow.rt07.ui

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.PurchaseInfo
import com.anjlab.android.iab.v3.PurchaseState
import com.ow.rt07.MainActivity
import com.ow.rt07.R
import com.ow.rt07.databinding.ActivitySplashBinding
import com.ow.rt07.utils.UIApplicationUtils

class SplashActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySplashBinding
    private lateinit var billing: BillingProcessor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Log.d("Splash", "On Create !")
        launchVideo()
//        checkSubscription()
        router()
    }

    override fun onStop() {
        super.onStop()

        binding.videoView.stopPlayback()
    }

    override fun onResume() {
        super.onResume()
        UIApplicationUtils.transparentStatusBar(this, true)
        Log.d("Splash", "On Resume !")
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::billing.isInitialized){
            billing.release()
        }
    }

    private fun launchVideo(){
        val uriPath =
            "android.resource://" + this.packageName.toString() + "/" + R.raw.video_splash
        binding.videoView.setVideoURI(Uri.parse(uriPath))
        scaleVideo()
        binding.videoView.start()
        Log.d("Splash","This is Splash Video !")
    }

    private fun scaleVideo() {
        binding.videoView.setOnPreparedListener { mediaPlayer ->
            val videoRatio = mediaPlayer.videoWidth / mediaPlayer.videoHeight.toFloat()
            val screenRatio = binding.videoView.width / binding.videoView.height.toFloat()
            val scaleX = videoRatio / screenRatio
            if (scaleX >= 1f) {
                binding.videoView.scaleX = scaleX
            } else {
                binding.videoView.scaleY = 1f / scaleX
            }
            mediaPlayer.isLooping = true
        }
    }

    private fun checkSubscription() {
        val isAvailable = BillingProcessor.isIabServiceAvailable(this)
        if (!isAvailable) {
            // -> show alert
//            alertCHPlayError()
        }else {
            billing = BillingProcessor(this, null, object : BillingProcessor.IBillingHandler {
                override fun onProductPurchased(productId: String, details: PurchaseInfo?) {
                }

                override fun onPurchaseHistoryRestored() {
                }

                override fun onBillingError(errorCode: Int, error: Throwable?) {
                    Log.e(
                        "checkSubscription IAP onBillingError",
                        "error = ${error?.localizedMessage}"
                    )
                }

                override fun onBillingInitialized() {
                    if (billing.isConnected) {
                        // continue
                        Log.e("IAP checkSubscription", "onBillingInitialized")
                        getInfoIAP()
                    }
                }
            })
        }
    }

    private fun getInfoIAP(){
        billing.loadOwnedPurchasesFromGoogleAsync(object : BillingProcessor.IPurchasesResponseListener {

            override fun onPurchasesError() {
                Log.e("IAP Splash","onPurchasesError")
                router()
            }

            override fun onPurchasesSuccess() {
                Log.e("IAP Splash","onPurchasesSuccess")

//                val rs = billing.listOwnedSubscriptions()?.filter {
//                    billing.getSubscriptionPurchaseInfo(it)?.purchaseData?.purchaseState = PurchaseState.PurchasedSuccessfully
//                }

//                Log.e("IAP SPlash","list purchased = $rs")

//                if (rs != null) {
//                    AppPreferences.isPurchased = rs.isNotEmpty()
//                }
                router()
            }
        })
    }

    private fun goToMain(){
        Handler().postDelayed({
            Log.d("Splash","Go To Main !")

            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finishAffinity()
        }, 1000)
    }

    private fun goToIAP(){

    }

    private fun router(){
//        billing.release()
        Log.d("Splash","Router")
//        if(AppPreferences.isPurchased){
            goToMain()
//        }else{
//            goToIAP()
//        }
    }
}

