package com.ow.rt07.ui.main.wallpapers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ow.rt07.model.wallpaper.*
import com.ow.rt07.network.RTApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WallpaperViewModel : ViewModel() {

    private var wallpaperList: MutableLiveData<List<Data>?>? = null
    private var suggestKeyword: MutableLiveData<Data?>? = null
    private var searchWallpaper: MutableLiveData<DataX?>? = null


    fun getWallpaper(ratio: String): LiveData<List<Data>?> {
        if (wallpaperList == null) {
            wallpaperList = MutableLiveData()
            getDataWallpaper(ratio)
        }
        return wallpaperList as MutableLiveData<List<Data>?>

    }

    fun getSuggetKeyword(): MutableLiveData<Data?> {
        if (suggestKeyword == null) {
            suggestKeyword = MutableLiveData()
            getDataSuggetKeyword()
        }
        return suggestKeyword as MutableLiveData<Data?>
    }

    fun getWallpaperBySearch(keyword: String, ratio: String): MutableLiveData<DataX?> {
            searchWallpaper = MutableLiveData()
            getDataWallpaperBySearch(keyword, ratio)
        return searchWallpaper as MutableLiveData<DataX?>
    }

    fun getDataWallpaper(ratio: String) {
        var rtApi: RTApi = RTApi.invoke()
        rtApi.getMenuWallpaper(ratio).enqueue(object : Callback<MenuWallpaper> {
            override fun onResponse(call: Call<MenuWallpaper>, response: Response<MenuWallpaper>) {
                if (wallpaperList!!.value == null) {
                    wallpaperList!!.postValue(listOf(response.body()!!.data))
                }
            }

            override fun onFailure(call: Call<MenuWallpaper>, t: Throwable) {
                wallpaperList!!.postValue(null)
            }
        })
    }

    fun getDataSuggetKeyword() {
        var rtApi: RTApi = RTApi.invoke()
        rtApi.getKeywordSuggest("wallpaper").enqueue(object : Callback<MenuWallpaper> {
            override fun onResponse(call: Call<MenuWallpaper>, response: Response<MenuWallpaper>) {
                if (suggestKeyword!!.value == null) {
                    suggestKeyword!!.postValue(response.body()!!.data)
                }
            }

            override fun onFailure(call: Call<MenuWallpaper>, t: Throwable) {
                suggestKeyword!!.postValue(null)
            }
        })
    }

    fun getDataWallpaperBySearch(keyword: String, ratio: String) {
        var rtApi: RTApi = RTApi.invoke()
        rtApi.getSearchByKeyword(keyword, ratio).enqueue(object : Callback<SearchCateWallpaper> {
            override fun onResponse(
                call: Call<SearchCateWallpaper>,
                response: Response<SearchCateWallpaper>
            ) {
                searchWallpaper!!.postValue(response.body()!!.data)
            }

            override fun onFailure(call: Call<SearchCateWallpaper>, t: Throwable) {
                searchWallpaper!!.postValue(null)
            }
        })
    }
}


