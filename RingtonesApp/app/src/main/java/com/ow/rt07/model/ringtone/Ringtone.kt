package com.ow.rt07.model.ringtone

import android.os.Parcelable
import com.google.gson.annotations.Expose
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Ringtone(
    val id: Int,
    val title: String,
    val artist:String = "qweqweq",
    val duration: Int,
    val m4r: String,
    val download: Int,
    val reward_ads: Int ,
    var process: Int = 0,
    var statusPlay: String? = "default"
): Parcelable