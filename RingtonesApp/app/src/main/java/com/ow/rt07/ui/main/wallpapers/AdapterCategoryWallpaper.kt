package com.ow.rt07.ui.main.wallpapers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.core.view.isVisible
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.R
import com.ow.rt07.databinding.*
import com.ow.rt07.model.wallpaper.ItemX
import com.ow.rt07.ui.main.ringtones.AdapterRingtonesCategory
import com.squareup.picasso.Picasso

class AdapterCategoryWallpaper(
    var cateWallpaperList: ArrayList<ItemX> = arrayListOf(),
    val listener: AdapterRecyclerViewInterface,
) : RecyclerView.Adapter<AdapterCategoryWallpaper.CateWallpaperHolder>() {

    fun setContent(value: ArrayList<ItemX>) {
        cateWallpaperList.addAll(value)
        notifyDataSetChanged()
    }

    class CateWallpaperHolder(val binding: ItemCategoryWallpaperBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CateWallpaperHolder {
        val binding =
            ItemCategoryWallpaperBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return CateWallpaperHolder(binding, parent)
    }

    override fun onBindViewHolder(holder: CateWallpaperHolder, position: Int) {
        Picasso.get().load(cateWallpaperList[position].image_thumb)
            .placeholder(R.drawable.img_item_category_default)
            .into(holder.binding.imgCategoryWallpaper)
        if (cateWallpaperList[position].vip == 0) {
            holder.binding.imgVipCategoryWallpaper.visibility = View.INVISIBLE
        } else {
            holder.binding.imgVipCategoryWallpaper.visibility = View.VISIBLE
        }
        holder.binding.root.setOnClickListener {
//            val extras = FragmentNavigatorExtras(holder.binding.imgCategoryWallpaper to "image_big")
//            findNavController(holder.itemView).navigate(R.id.action_category_wallpaper_fragment_to_navigation_data_detail_wallpaper, null, null, extras)
            listener.onItemClicked(position)
        }
    }

    override fun getItemCount(): Int {
        return cateWallpaperList.size
    }

}