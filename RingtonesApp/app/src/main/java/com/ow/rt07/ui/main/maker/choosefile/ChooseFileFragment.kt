package com.ow.rt07.ui.main.maker.choosefile

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.ow.rt07.MainActivity
import com.ow.rt07.R
import com.ow.rt07.databinding.ChooseFileFragmentBinding
import com.ow.rt07.model.ringtone.Ringtone

import com.ow.rt07.utils.base.BaseFragment
import com.ow.rt07.utils.safeNavigate
import linc.com.library.AudioTool
import linc.com.library.callback.OnFileComplete
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File

private const val LOG_TAG = "ChooseFile Fragment"

class ChooseFileFragment : BaseFragment() {

    private val REQ_CODE_PICK_SOUND_FILE = 1
    private val REQ_CODE_STORAGE_PERMMISION = 2

    private lateinit var binding:ChooseFileFragmentBinding
    private lateinit var viewModel: ChooseFileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("Choose File Fragment","On Create View !")
        (activity as MainActivity).hideBottomNavigation()
        viewModel = ViewModelProvider(this).get(ChooseFileViewModel::class.java)
        binding = ChooseFileFragmentBinding.inflate(inflater, container,false)

        return binding.root
    }

    override fun setUpView() {
        super.setUpView()

//        mHandler = Handler()

        launchVideo()

        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.btnGoToFileManager.setOnClickListener {
            Log.d("Choose File Fragment","Go To File Manager")
            checkStoragePermission()
        }

        binding.btnGoToWifiTransfer.setOnClickListener {

            Log.d("Choose File Fragment","Go To File Transfer")
                val action = ChooseFileFragmentDirections.actionToWifiTransfer()
                findNavController().safeNavigate(action)
        }
    }

    override fun bind() {
        super.bind()
    }

    private fun launchVideo(){
        val uriPath =
            "android.resource://" + requireActivity().packageName.toString() + "/" + R.raw.video_choosefile
        binding.videoView.setVideoURI(Uri.parse(uriPath))
        scaleVideo()
        binding.videoView.start()
        Log.d("Splash","This is Splash Video !")
    }

    private fun scaleVideo() {
        binding.videoView.setOnPreparedListener { mediaPlayer ->
            val videoRatio = mediaPlayer.videoWidth / mediaPlayer.videoHeight.toFloat()
            val screenRatio = binding.videoView.width / binding.videoView.height.toFloat()
            val scaleX = videoRatio / screenRatio
            if (scaleX >= 1f) {
                binding.videoView.scaleX = scaleX
            } else {
                binding.videoView.scaleY = 1f / scaleX
            }
            mediaPlayer.isLooping = true
        }
    }

    private fun launchSelectAudio() {
        val i = Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI)
        resultLauncher.launch(i)
    }

    private fun checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val hasReadPermission = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
            val hasWritePermission = ContextCompat.checkSelfPermission(requireContext(),Manifest.permission.WRITE_EXTERNAL_STORAGE)

            val permissions = ArrayList<String>()

            if (hasReadPermission != PackageManager.PERMISSION_GRANTED)
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)

            if (hasWritePermission != PackageManager.PERMISSION_GRANTED)
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)

            if (permissions.isNotEmpty())
                requestPermissions(permissions.toTypedArray(), REQ_CODE_STORAGE_PERMMISION)


            else launchSelectAudio()

        } else launchSelectAudio()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQ_CODE_STORAGE_PERMMISION) {
            var denied = false
            for (i in permissions.indices)
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    denied = true
                    break
                }

            if (denied)
                Toast.makeText(requireContext(), getString(R.string.permission_error), Toast.LENGTH_SHORT).show()
            else
                launchSelectAudio()
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data

            data?.let { it ->
                val contentUri = it.data

                contentUri?.let { uriSelected ->

                    if (uriSelected.scheme.toString().compareTo("content") === 0) {
                        val cursor: Cursor? =
                            requireContext().contentResolver.query(uriSelected, null, null, null, null)
                        cursor?.let { cur ->
                            if (cur.moveToFirst()) {

                                val column_id: Int = cur.getColumnIndexOrThrow(MediaStore.Audio.Media._ID)
                                val column_title: Int = cur.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE)
                                val column_artist: Int = cur.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST)
                                val column_duration: Int = cur.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION)
                                val column_path: Int = cur.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA)

                                val ringtone = Ringtone(
                                    id = cur.getString(column_id).toInt(),
                                    title = cur.getString(column_title),
                                    artist = cur.getString(column_artist),
                                    duration = cur.getString(column_duration).toInt(),
                                    m4r = cur.getString(column_path),
                                    download = 0,
                                    reward_ads = 0
                                )

                                Log.d(LOG_TAG,"ID : ${ringtone.id}")
                                Log.d(LOG_TAG,"TITLE : ${ringtone.title}")
                                Log.d(LOG_TAG,"ARTIST : ${ringtone.artist}")
                                Log.d(LOG_TAG,"DURATION : ${ringtone.duration}")
                                Log.d(LOG_TAG,"PATH : ${ringtone.m4r}")

                                lifecycleScope.launchWhenResumed {

                                    if ((ringtone.duration.toFloat() / 1000F) >= 20F){
                                        Log.d(LOG_TAG,"URI : $uriSelected")
                                        val path:String = cur.getString(column_path)
                                        Log.d(LOG_TAG,"Path Choose : $path")
                                        Handler().postDelayed({
                                            val action = ChooseFileFragmentDirections.actionToSuperTuneMaker(ringtone, path)
                                            findNavController().safeNavigate(action)
                                        }, 200)
                                    }else {
                                        Log.d(LOG_TAG,"The length of ringtone should be greater than 20s")
                                        var dialog = DialogErrorAfterSelectFileFragment()
                                        dialog.show(requireActivity().supportFragmentManager, "errorAfterSelectFileFragment")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}