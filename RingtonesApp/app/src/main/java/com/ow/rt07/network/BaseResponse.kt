package com.ow.rt07.network


class BaseResponse<T>(val status: Boolean = false,
                      val message: String = "",
                      val data: T? = null) {

}