package com.ow.rt07.ui.main.ringtones

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.MainActivity
import com.ow.rt07.databinding.DetailCategoryRingtonesFragmentBinding
import com.ow.rt07.databinding.RingtonesFragmentBinding
import com.ow.rt07.model.ringtone.PopularRingtone
import com.ow.rt07.network.RTApi
import com.ow.rt07.ui.main.wallpapers.AdapterRecyclerViewInterface
import com.ow.rt07.ui.main.wallpapers.CategoryWallpaperFragmentArgs
import com.ow.rt07.ui.main.wallpapers.WallpaperFragmentDirections
import com.ow.rt07.utils.base.BaseFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailCategoryRingtonesFragment : BaseFragment(), ItemRingtoneInterFace {

    private lateinit var binding: DetailCategoryRingtonesFragmentBinding
    private lateinit var viewModel: DetailCategoryViewModel
    lateinit var adapter: AdapterItemPopularRingtones
    val args: DetailCategoryRingtonesFragmentArgs by navArgs()
    var load_more_url: String = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DetailCategoryRingtonesFragmentBinding.inflate(inflater, container, false)
        binding.loadMoreView.visibility = View.GONE
        (activity as MainActivity).hideBottomNavigation()
        viewModel = ViewModelProvider(this).get(DetailCategoryViewModel::class.java)
        binding.lbTitle.text = args.titleCategory
        getRingtone()
        action()
        return binding.root
    }

    override fun bind() {
        super.bind()
        loadMore()
        adapter =   AdapterItemPopularRingtones(arrayListOf(), object : AdapterRecyclerViewInterface {
            override fun onItemClicked(index: Int) {
                val action =
                    DetailCategoryRingtonesFragmentDirections.actionNavigationDetailCategoryRingtoneToNavigationDetailRingtone(
                        arrayOf(),
                        index
                    )
                findNavController().navigate(action)
            }
        }, this)
        binding.recyclerViewDetailCategoryRingtones.layoutManager =
            LinearLayoutManager(
                this.context,
                LinearLayoutManager.VERTICAL,
                false
            )
        binding.recyclerViewDetailCategoryRingtones.adapter = adapter

    }


    fun getRingtone() {
        viewModel.getRingtone(args.categoryId).observe(viewLifecycleOwner, Observer {
           it?.let {
//               adapter.setContent(it.data)
               load_more_url = it.next_page_url
               binding.loadingView4.visibility = View.GONE
               binding.loadMoreView.visibility = View.GONE
           }
        })
    }

    fun loadMore() {
        binding.recyclerViewDetailCategoryRingtones.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!binding.recyclerViewDetailCategoryRingtones.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    Log.e("load more", " load more")
                    if (load_more_url != "") {
                        binding.loadMoreView.visibility = View.VISIBLE
                        getRingtoneMore(load_more_url)
                        adapter.notifyDataSetChanged()
                    }
                }
            }
        })
    }

    fun getRingtoneMore(url: String){
        val rtApi: RTApi = RTApi.invoke()
        rtApi.loadMoreRingtone(url).enqueue(object : Callback<PopularRingtone> {
            override fun onResponse(call: Call<PopularRingtone>, response: Response<PopularRingtone>) {
                response.body()?.let {
//                    adapter.setContent(it.data)
                    load_more_url = it.next_page_url
                    binding.loadMoreView.visibility = View.GONE
                }
            }
            override fun onFailure(call: Call<PopularRingtone>, t: Throwable) {
                Log.e("error", ""+ t)
            }
        })
    }

    fun action() {
        btnBack()
    }

    fun btnBack() {
        binding.btnBackDetailRingtone.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}