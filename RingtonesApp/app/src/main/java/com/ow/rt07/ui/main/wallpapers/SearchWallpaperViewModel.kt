package com.ow.rt07.ui.main.wallpapers

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ow.rt07.model.wallpaper.*
import com.ow.rt07.network.RTApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchWallpaperViewModel: ViewModel() {
    var wallpapers: MutableLiveData<DataX?>? = null

    fun getWallpaper(keyword: String, ratio: String): MutableLiveData<DataX?> {
        if (wallpapers == null) {
            wallpapers = MutableLiveData()
            getSearchBySuggestKeyword(keyword, ratio)
        }
        return wallpapers as MutableLiveData<DataX?>

    }

    fun getSearchBySuggestKeyword(keyword: String, ratio: String){
        var rtApi: RTApi = RTApi.invoke()
        rtApi.getSearchByKeyword(keyword, ratio).enqueue(object : Callback<SearchCateWallpaper> {
            override fun onResponse(call: Call<SearchCateWallpaper>, response: Response<SearchCateWallpaper>) {
                if (wallpapers?.value == null) {
                    wallpapers?.postValue(response.body()!!.data)
                }
                Log.e("Data", response.body()!!.data.toString())
            }
            override fun onFailure(call: Call<SearchCateWallpaper>, t: Throwable) {
                wallpapers?.postValue(null)
            }
        })
    }

}