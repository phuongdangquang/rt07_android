package com.ow.rt07.network

import com.google.gson.JsonArray
import com.ow.rt07.config.AppConfig
import com.ow.rt07.model.Category
import com.ow.rt07.model.ringtone.DataRingtone
import com.ow.rt07.model.ringtone.PopularRingtone
import com.ow.rt07.model.wallpaper.*
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface RTApi {

    companion object {

        operator fun invoke(): RTApi {

            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()


            return Retrofit.Builder()
                .client(client)
                .baseUrl(AppConfig.endPointApi())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(RTApi::class.java)
        }
    }

    //category get all
    @GET("api/category/get_all")
    fun getAllCategory() : Call<DataRingtone>
    //get popular ringtone
    @GET("api/ringtone/popular?page=1")
    fun getPopular() : Call<PopularRingtone>

    //get ringtone in category
    @GET("api/ringtone/get_ringtone_in_category")
    fun getRingtoneInCategory(@Query("category_id") category_id: Int, @Query("page") page: Int) : Call<PopularRingtone>

    //search ringtone
    @GET("api/ringtone/search")
    fun searchRingtone(@Query("keyword") keyword: String) : Call<PopularRingtone>
    //other

    //menu wallpaper
    @GET("api/menu/wallpaper")
    fun getMenuWallpaper(@Query("ratio") ratio: String) : Call<MenuWallpaper>

    //get wallpaper by keyword
    @GET("api/search")
    fun getSearchByKeyword(@Query("keyword") keyword: String, @Query("ratio") ratio: String) : Call<SearchCateWallpaper>

    //keyword suggest
    @GET("api/keyword-suggest")
    fun getKeywordSuggest(@Query("type") type: String) : Call<MenuWallpaper>
    //post View
    @POST("api/wallpaper/view")
    fun postView(@Query("id") id: Int): Call<BaseData>

    //search wallpaper
    //live wallpaper view
    //live wallpaper download
    //wallpaper view

    //getCategoryWallpaper
    @GET
    fun getCateWallpaper(@Url url: String): Call<CateWallpaper>

    //loadmore wallpaper
    @GET
    fun loadMoreWallpaper(@Url url: String): Call<CateWallpaper>

    //loadmore ringtone
    @GET
    fun loadMoreRingtone(@Url url: String): Call<PopularRingtone>



    //wallpaper download
    @POST("api/wallpaper/download")
    fun postDownload(@Query("id") id: Int): Call<BaseData>
    //theme view
    //theme download



//    // merge data
//    @FormUrlEncoded
//    @POST("auth/merge")
//    suspend fun mergeData(@Field("new_user_id") new_user_id : Int? = null ,
//                          @Header("Authorization") token : String = "Bearer "+ AppPreferences.tokenAccessNow
//    ):Response<BaseResponse<Auth>>
//
//    //upload avatar
//    @Multipart
//    @POST("auth/uploadAvatar")
//    suspend fun upLoadAvatar(@Part avatar: MultipartBody.Part,
//                             @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<User>>
//
//
//    // get favorite
//    @GET("auth/favourites")
//    suspend fun getFavorite(@Header("Authorization") token : String = "Bearer "+ AppPreferences.token): Response<BaseResponse<Favorite>>
//
//    // update about you
//    @FormUrlEncoded
//    @POST("auth/updateInfo")
//    suspend fun updateInfo(
//        @Field("about_name") about_name : String? = null,
//        @Field("gender") gender : String? = null,
//        @Field("is_sync_health") is_sync_health : String? = null,
//        @Field("height") height : String? = null,
//        @Field("dob") dob : String? = null,
//        @Field("use_smart_reminder") use_smart_reminder : Int? = null,
//        @Field("workout_time") workout_time : String? = null,
//        @Field("attentions") attentions : String? = null,
//        @Field("workout_days") workout_days : String? = null,
//        @Field("activity_level") activity_level : String? = null,
//        @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<User>>
//
//    @FormUrlEncoded
//    @POST("auth/addDeviceToken")
//    suspend fun addDeviceToken(@Field("firebase_device_token") deviceToken : String,
//                                @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Any>>
//
//    // update workout setting
//
//    @FormUrlEncoded
//    @POST("auth/updateWorkoutSettings")
//    suspend fun updateWorkoutSet(
//        @Field("warm_up_enabled") warm_up_enabled : Int? = null,
//        @Field("cool_down_enabled") cool_down_enabled : Int? = null,
//        @Field("voice_assistant_enabled") voice_assistant_enabled : Int? = null,
//        @Field("voice") voice : String? = null,
//        @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<User>>
//
//    // update password in profile
//    @FormUrlEncoded
//    @POST("auth/updatePassword")
//    suspend fun updatePassword(@Field("password")password : String,
//                               @Field("confirm_password")confirm_password : String,
//                               @Field("current_pass")current_pass : String,
//                               @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Auth>>
//    // requets change password for email
//    @FormUrlEncoded
//    @POST("auth/requestChangePassword")
//    suspend fun requestChangePasswordForEmail(
//        @Field("email") email : String? = "",
//        @Field("phone") phone : String? = "",
//        @Field("request_type") request_type : String,
//    ):Response<BaseResponse<Any>>
//
//    // login
//    @FormUrlEncoded
//    @POST("auth/login")
//    suspend fun login(
//        @Field("email") email : String? = "",
//        @Field("phone") phone : String? = "",
//        @Field("password") password : String,
//        @Field("login_type") login_type : String,
//        @Field("previous_user_id") previous_user_id : Int? = null
//    ):Response<BaseResponse<Auth>>
//
//    // logout
//    @FormUrlEncoded
//    @POST("auth/logout")
//    suspend fun logout(@Field("email") email : String? = null,@Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Any>>
//
//    // verify email (request)
//    @FormUrlEncoded
//    @POST("auth/verifyEmail")
//    suspend fun verifyEmail(@Field("email") email : String):Response<BaseResponse<Any>>
//    @FormUrlEncoded
//    @POST("auth/verifyOtp")
//    suspend fun verifyOtpEmail(
//        @Field("email") email : String,
//        @Field("code") code : Int
//    ):Response<BaseResponse<Any>>
//
//    @FormUrlEncoded
//    @POST("auth/resetPassword")
//    suspend fun resetPassword(
//        @Field("code") code : String,
//        @Field("password") password : String,
//        @Field("confirm_password") confirm_password : String,
//    ):Response<BaseResponse<Any>>
//
//    @FormUrlEncoded
//    @POST("auth/register")
//    suspend fun registerEmail(
//        @Field("email") email : String = "" ,
//        @Field("password") password : String,
//        @Field("confirm_password") confirm_password : String,
//        @Field("information_id") information_id : Int? = null,
//        @Field("previous_user_id") previous_user_id : Int? = null
//    ):Response<BaseResponse<Auth>>
//
//    @FormUrlEncoded
//    @POST("auth/register")
//    suspend fun registerPhone(
//        @Field("phone") phone : String = "" ,
//        @Field("password") password : String,
//        @Field("confirm_password") confirm_password : String,
//        @Field("information_id") information_id : Int? = null,
//        @Field("previous_user_id") previous_user_id : Int? = null
//    ):Response<BaseResponse<Auth>>
//
//    //User
//    @FormUrlEncoded
//    @POST("preparePlan")
//    suspend fun preparePlan(@Field("gender") gender : String,
//                            @Field("height") height : String,
//                            @Field("weight") weight : String,
//                            @Field("goal") goal : String,
//                            @Field("age") age : String,
//                            @Field("about_name") about_name : String,
//                            @Field("is_sync_health") is_sync_health : String,
//                            @Field("sync_health_options") sync_health_options : String,
//                            @Field("workout_time") workout_time : String,
//                            @Field("attentions") attentions : String,
//                            @Field("workout_days") workout_days : String,
//                            @Field("use_smart_reminder") use_smart_reminder : Int,
//                            @Field("activity_level") activity_level : String,
//                            @Field("step_goal") step_goal : String,
//                            @Field("daily_water") daily_water : String,
//    ): Response<BaseResponse<UserInformation>>
//
//    @FormUrlEncoded
//    @POST("v2/preparePlan")
//    suspend fun updatePreparePlan(@Field("gender") gender : String,
//                            @Field("height") height : String,
//                            @Field("weight") weight : String,
//                            @Field("goal") goal : String,
//                            @Field("age") age : String,
//                            @Field("about_name") about_name : String,
//                            @Field("is_sync_health") is_sync_health : String,
//                            @Field("sync_health_options") sync_health_options : String,
//                            @Field("workout_time") workout_time : String,
//                            @Field("attentions") attentions : String,
//                            @Field("workout_days") workout_days : String,
//                            @Field("use_smart_reminder") use_smart_reminder : Int,
//                            @Field("activity_level") activity_level : String,
//                            @Field("step_goal") step_goal : String,
//                            @Field("daily_water") daily_water : String,
//                                  @Field("user_id") user_id : Int? = null
//
//    ): Response<BaseResponse<Auth>>
//
//    // access now
//    @FormUrlEncoded
//    @POST("auth/accessNow")
//    suspend fun accessNow(@Field("information_id") information_id : Int):Response<BaseResponse<Auth>>
//
//    @FormUrlEncoded
//    @POST("v2/auth/socialLogin")
//    suspend fun socialLogin(
//        @Field("phone") phone : String? = null ,
//        @Field("provider") provider : String ,
//        @Field("provider_id") provider_id : String ,
//        @Field("token") token : String? = null ,
//        @Field("email") email : String? = null ,
//        @Field("name") name : String? = null ,
//        @Field("avatar") avatar : String? = null ,
//        @Field("information_id") information_id : Int? = null ,
//        @Field("previous_user_id") previous_user_id : Int? = null
//        ):Response<BaseResponse<Auth>>
//
//    @GET("auth/me")
//    suspend fun getUserInfo(@Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<User>>
//    //HOME
//
//    @GET("home")
//    suspend fun home(@Header("Authorization") token : String = "Bearer "+ AppPreferences.token): Response<BaseResponse<Home>>
//
//    @GET("search")
//    suspend fun search(@Query("search") search: String,
//                       @Query("limit") limit: Int,
//                       @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<SearchResponse>>
//
//
//    @GET
//    suspend fun suggestions(@Url url: String) : Response<JsonArray>
//
//    @GET("plans")
//    suspend fun plans(@Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<ArrayList<Plan>>>
//
//    @GET("plan/{id}")
//    suspend fun planDetail(@Path("id") id : Int,
//                           @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<PlanDetail>>
//
//    @POST("plan/{id}/reset")
//    suspend fun resetPlan(@Path("id") id : Int,
//                          @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<PlanDetail>>
//
//    @POST("plan/{id}/start")
//    suspend fun startPlan(@Path("id") id : Int,
//                          @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<PlanDetail>>
//
//    @POST("plan/{id}/stop")
//    suspend fun stopPlan(@Path("id") id : Int,
//                         @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Any>>
//
//    @GET("customizedPlan")
//    suspend fun customizedPlan(@Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<ArrayList<CustomizedPlans>>>
//
//    @FormUrlEncoded
//    @POST("plan/{id}/schedule")
//    suspend fun reschedulePlan(@Path("id") id : Int,
//                               @Field("workout_id") wid : Int,
//                               @Field("days") days : String,
//                               @Field("new_date") new_date : String ,
//                               @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<PlanDetail>>
//
//    //Library
//    //workouts?search=&sort=workout_level&dir=asc&per_page=10&page=1&body_part=1&muscle_combination=4&workout_level=2
//
//    @GET("workouts")
//    suspend fun workouts(@Query("search") search : String? = null,
//                         @Query("sort") sort : String? = null,
//                         @Query("dir") dir : String? = null,
//                         @Query("per_page") per_page : Int? = null,
//                         @Query("page") page : Int? = null,
//                         @Query("body_part") body_part : Int? = null,
//                         @Query("muscle_combination") muscle_combination : Int? = null,
//                         @Query("workout_level") workout_level : Int? = null,
//                         @Query("duration") duration : Int? = null,
//                         @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<LibraryWorkout>>
//
//    @GET("workouts/filter_config")
//    suspend fun filterWorkout(@Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<FilterWorkout>>
//
//    @POST("workouts/{id}/favourite")
//    suspend fun favoriteWorkout(@Path("id") id : Int,
//                                @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Any>>
//    @POST("workouts/{id}/removeFavourite")
//    suspend fun removeWorkOutFromFav(@Path("id") id : Int,
//                                     @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Any>>
//    @POST("exercises/{id}/removeFavourite")
//    suspend fun removeExerciserFromFav(@Path("id") id : Int,
//                                       @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Any>>
//    @GET("workouts/{id}")
//    suspend fun workoutDetail(@Path("id") id : Int,
//                              @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Workout>>
//
//    @FormUrlEncoded
//    @POST("tracker/workout")
//    suspend fun trackerWorkout(@Field("date") date : String,
//                               @Field("total_energy") energy : Int,
//                               @Field("total_durations") duration : Int,
//                               @Field("details") details : String,
//                               @Field("workout_id") workout_id : Int,
//                               @Field("customized_plan_id") customized_plan_id : String? = null,
//                               @Field("customized_plan_days") customized_plan_days : String? = null,
//                               @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Any>>
//
//    //{{url}}/api/exercises?search=&sort=id&dir=asc&page=1&body_part=5&level=5
//
//    @GET("exercises")
//    suspend fun exercises(@Query("search") search : String? = null,
//                          @Query("sort") sort : String? = null,
//                          @Query("dir") dir : String? = null,
//                          @Query("page") page : Int? = null,
//                          @Query("per_page") per_page : Int? = null,
//                          @Query("body_part") body_part : Int? = null,
//                          @Query("level") level : Int? = null,
//                          @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<LibraryExercises>>
//
//    @GET("exercises/{id}")
//    suspend fun exerciseDetail(@Path("id") id : Int,
//                               @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Exercise>>
//
//    @POST("exercises/{id}/favourite")
//    suspend fun favoriteExercise(@Path("id") id : Int,
//                                 @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Any>>
//
//    @GET("exercises/filter_config")
//    suspend fun filterExercises(@Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<FilterWorkout>>
//
//    @Streaming
//    @GET
//    suspend fun downloadFile(@Url url: String) : Response<ResponseBody>
//
//    //MARK: - TRACKER
//
//    //{{url}}/api/tracker/logs?from=2021-06-17&to=2021-06-30
//    @GET("tracker/logs")
//    suspend fun log(@Query("from") from : String,
//                    @Query("to") to : String,
//                    @Query("weight_from") weight_from : String? = null,
//                    @Query("weight_to") weight_to : String? = null,
//                    @Query("today") today : String ,
//                    @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<LogReport>>
//
//    @GET("tracker/stepTrackers")
//    suspend fun getStepTracker(@Query("page") page : Int,
//                                @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<ArrayList<Tracker>>>
//
//    @GET("tracker/waterTrackers")
//    suspend fun getWaterTracker(@Query("page") page : Int,
//                               @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<ArrayList<Tracker>>>
//
//    @GET("tracker/weightTrackers")
//    suspend fun getWeightTracker(@Query("page") page : Int,
//                               @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<ArrayList<Tracker>>>
//    @GET("tracker/workoutTrackers")
//    suspend fun getWorkoutTracker(@Query("page") page : Int,
//                                  @Query("metric") metric : String,
//                                 @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<ArrayList<Tracker>>>
//
//    //history
//    @GET("tracker/workoutHistory")
//    suspend fun workoutHistory(@Query("from") from : String,
//                               @Query("to") to : String,
//                               @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<ArrayList<WorkoutHistory>>>
//
//    @GET("tracker/workoutHistoryTotal")
//    suspend fun workoutHistoryTotal(@Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<HistoryTotal>>
//
//    @GET("tracker/workoutHistoryDetail")
//    suspend fun workoutHistoryDetail(@Query("from") from : String,
//                                     @Query("to") to : String,
//                                     @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<HistoryDetail>>
//
//    // post Steps tracker
//    @FormUrlEncoded
//    @POST("tracker/steps")
//    suspend fun stepTracker(
//        @Field("steps") steps : Int? = null,
//        @Field("date") date : String? = null,
//        @Header("Authorization") token : String = "Bearer "+ AppPreferences.token
//    ):Response<BaseResponse<Any>>
//
//    // update info tracker
//    @FormUrlEncoded
//    @POST("tracker/updateInfo")
//    suspend fun updateInfoTracker(
//        @Field("step_goal") step_goal : Int? = null,
//        @Field("start_weight") start_weight : String? = null,
//        @Field("target_weight") target_weight : String? = null,
//        @Field("daily_water") daily_water : Int? = null,
//        @Field("activity_level") activity_level : Int? = null,
//        @Header("Authorization") token : String = "Bearer "+ AppPreferences.token
//    ):Response<BaseResponse<UserInformation>>
//
//    // post weight tracker
//    @FormUrlEncoded
//    @POST("tracker/weight")
//    suspend fun weightTracker(
//        @Field("weight") weight : String? = null,
//        @Field("date") date : String? = null,
//        @Header("Authorization") token : String = "Bearer "+ AppPreferences.token
//    ):Response<BaseResponse<Any>>
//
//    @FormUrlEncoded
//    @POST("tracker/water")
//    suspend fun waterTracker(
//        @Field("amount") amount : Int,
//        @Field("date") date : String,
//        @Header("Authorization") token : String = "Bearer "+ AppPreferences.token
//    ):Response<BaseResponse<Any>>
//
//    //MARK:- MEDALS
//    @POST("medal/active/{id}")
//    suspend fun activeMedal(@Path("id") id : Int,
//                            @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Any>>
//
//    //MARK: - LEADER
//
//    @GET("leaders")
//    suspend fun leaderboard(@Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Leaderboard>>
//
//    @GET("leaders/medals")
//    suspend fun medals(@Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<ArrayList<Medal>>>
//
//    @GET("rooms")
//    suspend fun rooms(@Query("search") search : String? = null,
//                      @Query("sort") sort : String? = null,
//                      @Query("dir") dir : String? = null,
//                      @Query("per_page") per_page:Int? = null,
//                      @Query("page") page : Int? = null,
//                      @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<RepositoryRoom>>
//
//    @Multipart
//    @POST("rooms")
//    suspend fun createNewRoom(@Part("name") name : RequestBody,
//                              @Part("type") type : RequestBody,
//                              @Part thumb: MultipartBody.Part?,
//                              @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<RoomCreated>>
//
//    @GET("rooms/{id}")
//    suspend fun roomDetail(@Path("id") id : Int,
//                           @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Room>>
//
//    @POST("rooms/{id}/leave")
//    suspend fun leaveRoom(@Path("id") id : Int,
//                          @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Any>>
//
//    @FormUrlEncoded
//    @POST("rooms/join")
//    suspend fun joinRoom(@Field("code") code : String,
//                         @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Any>>
//
//    @GET("leaders/energy")
//    suspend fun leaderCalories(@Query("limit") limit : Int,
//                               @Query("page") page : Int,
//                               @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<ListPaging<MostLeader>>>
//
//    @GET("leaders/duration")
//    suspend fun leaderTime(@Query("limit") limit : Int,
//                           @Query("page") page : Int,
//                           @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<ListPaging<MostLeader>>>
//
//    @GET("leaders/workout")
//    suspend fun leaderWorkout(@Query("limit") limit : Int,
//                              @Query("page") page : Int,
//                              @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<ListPaging<MostLeader>>>
//
//    @POST("rooms/{id}/kick")
//    @FormUrlEncoded
//    suspend fun kickUserOutRoom(@Path("id") id : Int,
//                                @Field("member_id") memberId : Int,
//                                @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<Room>>
//
//    @POST("rooms/{id}/createCode")
//    suspend fun createCodeForJoinRoom(@Path("id") id : Int,
//                                      @Header("Authorization") token : String = "Bearer "+ AppPreferences.token) : Response<BaseResponse<RoomCode>>
}