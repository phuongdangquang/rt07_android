package com.ow.rt07.ui.main.maker.recorder

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.ow.rt07.MainActivity
import com.ow.rt07.R
import com.ow.rt07.databinding.RecorderFragmentBinding
import com.ow.rt07.utils.base.BaseFragment
import java.io.File
import java.util.*

private const val LOG_TAG = "Recorder Fragment"

class RecorderFragment : BaseFragment() , DialogSaveRecordInterface {

    private lateinit var binding:RecorderFragmentBinding
    private lateinit var viewModel: RecorderViewModel

    private var recordingTime: Long = 0
    private var resumePlayingTime: Long = 0
    private var timer = Timer()
    lateinit var playingTimer : CountDownTimer

    private val requiredPermission = arrayOf(
        android.Manifest.permission.RECORD_AUDIO,
        android.Manifest.permission.READ_EXTERNAL_STORAGE
    )

    private val recordingFilePath: String by lazy {
        "${context?.externalCacheDir?.absolutePath}/recording.3gp"
    }

    private var recorder: MediaRecorder? = null
    private var player: MediaPlayer? = null

    private var state = StateRecorder.BEFORE_RECORDING
        set(value) {
            field = value

            updateUIWithState(value)
            updateIconWithState(value)
        }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(LOG_TAG,"On Create View !")

        (activity as MainActivity).hideBottomNavigation()

        viewModel = ViewModelProvider(this).get(RecorderViewModel::class.java)
        binding = RecorderFragmentBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.d(LOG_TAG,"On Destroy !")
        deleteFromExternalStorage("recording.3gp")
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        val audioRecordPermissionGranted =
            requestCode == REQUEST_RECORD_AUDIO_PERMISSION && grantResults.firstOrNull() == PackageManager.PERMISSION_GRANTED

        if (!audioRecordPermissionGranted) {
//            finish()
        }
    }

    override fun setUpView() {
        super.setUpView()

        requestAudioPermission()
        initView()
        bindView()
        initVariable()
    }

    private fun checkIfAlreadyHavePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.RECORD_AUDIO)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestAudioPermission() {
        requestPermissions(requiredPermission, REQUEST_RECORD_AUDIO_PERMISSION)
    }

    private fun initView() {
        updateIconWithState(state)
        updateUIWithState(state)
    }

    private fun bindView() {
        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.btnSave.setOnClickListener {
            Log.d(LOG_TAG,"Save")

            var dialog = DialogSaveRecordFragment(this)
            dialog.show(requireActivity().supportFragmentManager, "saveRecordFragment")
        }

        binding.btnBannerIap.setOnClickListener {
            Log.d(LOG_TAG,"IAP")
        }

        binding.btnGoToMaker.setOnClickListener {
            Log.d(LOG_TAG,"Go To Maker")
        }

        binding.btnCreateRecorder.setOnClickListener {
            Log.d(LOG_TAG,"Reset Record")

            stopPlaying()
            resetTimer()
            state = StateRecorder.BEFORE_RECORDING
        }

        binding.btnRecord.setOnClickListener {

            if (!checkIfAlreadyHavePermission()){
                Log.d(LOG_TAG,"Permission is not Granted !")
            }else {
                when (state) {
                    StateRecorder.BEFORE_RECORDING -> {
                        startRecoding()
                    }
                    StateRecorder.ON_RECORDING -> {
                        stopRecording()
                    }
                }
            }
        }

        binding.btnPlayRecord.setOnClickListener {
            Log.d(LOG_TAG,"Record")

            when (state) {
                StateRecorder.AFTER_RECORDING -> {
                    startPlaying()
                }
                StateRecorder.ON_PLAYING -> {
                    pausePlaying()
                }
                StateRecorder.ON_PAUSE_PLAYING -> {
                    resumePlaying()
                }
            }
        }
    }

    private fun initVariable() {
        state = StateRecorder.BEFORE_RECORDING
    }

    private fun startRecoding() {
        recorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
            setOutputFile(recordingFilePath)
            prepare()
        }

        recorder?.start()
        startRecordTimer()
        state = StateRecorder.ON_RECORDING
    }

    private fun stopRecording() {
        recorder?.run {
            stop()
            release()
        }
        stopTimer()
        recorder = null
        state = StateRecorder.AFTER_RECORDING
    }

    private fun saveFileRecorder(name:String) {

        Log.d(LOG_TAG,"Path : ${context?.externalCacheDir?.absolutePath}")
        Log.d(LOG_TAG,"New File Path : ${context?.externalCacheDir?.absolutePath}/${name}.3gp")

        var output = "${context?.externalCacheDir?.absolutePath}/${name}.3gp"
        recorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
            setOutputFile(output)
            prepare()
        }
    }

    private fun startPlaying() {
        player = MediaPlayer().apply {
            setDataSource(recordingFilePath)
            prepare()
        }
        player?.start()
        binding.txtTimeLeft.text = convertTime(recordingTime)
        startPlayerTimer(recordingTime)
        state = StateRecorder.ON_PLAYING
    }

    private fun pausePlaying() {
        player?.pause()
        playingTimer.cancel()
        state = StateRecorder.ON_PAUSE_PLAYING
    }

    private fun resumePlaying() {
        player?.start()
        startPlayerTimer(resumePlayingTime)
        state = StateRecorder.ON_PLAYING
    }

    private fun stopPlaying() {

        playingTimer.onFinish()
        playingTimer.cancel()

        player?.release()
        player = null
        state = StateRecorder.AFTER_RECORDING
    }

    private fun deleteFromExternalStorage(fileName: String?) {
        val fullPath = context?.externalCacheDir?.absolutePath
        try {
            val file = File(fullPath, fileName)
            if (file.exists()) file.delete()
        } catch (e: Exception) {
            Log.e("App", "Exception while deleting file " + e.message)
        }
    }

    private fun updateIconWithState(state : StateRecorder){
        when(state){
            StateRecorder.BEFORE_RECORDING->{
                binding.imgRecord.setImageResource(R.drawable.icon_record_start_record)
            }
            StateRecorder.ON_RECORDING -> {
                binding.imgRecord.setImageResource(R.drawable.icon_record_stop_record)
            }
            StateRecorder.AFTER_RECORDING -> {
                binding.imgRecord.setImageResource(R.drawable.icon_record_play)
            }
            StateRecorder.ON_PLAYING -> {
                binding.imgRecord.setImageResource(R.drawable.icon_record_pause)
            }
            StateRecorder.ON_PAUSE_PLAYING -> {
                binding.imgRecord.setImageResource(R.drawable.icon_record_play)
            }
        }
    }

    private fun updateUIWithState(state: StateRecorder){
        when (state){
            StateRecorder.BEFORE_RECORDING->{
                binding.makerView.visibility = View.INVISIBLE
                binding.resetRecordView.visibility = View.INVISIBLE
                binding.btnRecord.visibility = View.VISIBLE
                binding.btnPlayRecord.visibility = View.INVISIBLE
                binding.btnSave.visibility = View.INVISIBLE
            }

            StateRecorder.AFTER_RECORDING -> {
                binding.makerView.visibility = View.VISIBLE
                binding.resetRecordView.visibility = View.VISIBLE
                binding.btnRecord.visibility = View.INVISIBLE
                binding.btnPlayRecord.visibility = View.VISIBLE
                binding.btnSave.visibility = View.VISIBLE
                binding.elapsedView.visibility = View.INVISIBLE
                binding.timeLeftView.visibility = View.INVISIBLE
                binding.txtTimeLeft.text = "00:00"
                binding.txtTimeElapsed.text = "00:00"
            }

            StateRecorder.ON_PLAYING -> {
                binding.elapsedView.visibility = View.VISIBLE
                binding.timeLeftView.visibility = View.VISIBLE
            }
        }
    }

    private fun startPlayerTimer(time:Long){

        Log.d(LOG_TAG,"Duration : $recordingTime")

        playingTimer = object : CountDownTimer((time * 1000), 1000) {
            override fun onTick(millisUntilFinished: Long) {

                val timeLeft = (millisUntilFinished / 1000)
                val timeElapsed = recordingTime - timeLeft

                resumePlayingTime = timeLeft

                binding.txtTimeLeft.text = convertTime(timeLeft)
                binding.txtTimeElapsed.text = convertTime(timeElapsed)
            }
            override fun onFinish() {
                state = StateRecorder.AFTER_RECORDING
            }
        }.start()
    }

    private fun startRecordTimer(){
        timer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                recordingTime += 1
                updateDisplay()
            }
        }, 1000, 1000)
    }

    private fun stopTimer(){
        timer.cancel()
        timer = Timer()
    }
    private fun resetTimer() {
        timer.cancel()
        timer = Timer()
        recordingTime = 0
        binding.txtTimeRecord.text = "00:00"
    }

    private fun updateDisplay(){

        convertTime(recordingTime)
        var timerRecord:String = convertTime(recordingTime)
        (context as Activity).runOnUiThread {
            binding.txtTimeRecord.text = "$timerRecord"
        }
    }

    private fun convertTime(timeInput: Long): String {
        val minutes = timeInput / (60)
        val seconds = timeInput % 60
        return String.format("%d:%02d", minutes, seconds)
    }

    companion object {
        private const val REQUEST_RECORD_AUDIO_PERMISSION = 201
    }

    override fun onSavedWithName(name: String) {
        saveFileRecorder(name)
    }
}

