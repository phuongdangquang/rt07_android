package com.ow.rt07.ui.main.wallpapers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updateLayoutParams
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.ow.rt07.databinding.ActivityDetailWallpaperBinding
import com.ow.rt07.model.wallpaper.ItemX

class DetailWallpaperActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailWallpaperBinding
    var wallpapers: ArrayList<ItemX> = arrayListOf()
    lateinit var adapter: AdapterDetailWallpaperFragment
    lateinit var viewModel: DetailWallpaperActivityViewModel
    var urlLoadMore: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(DetailWallpaperActivityViewModel::class.java)
        binding = ActivityDetailWallpaperBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpView()
        bind()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.detailWallpaperViewpager.unregisterOnPageChangeCallback(object :ViewPager2.OnPageChangeCallback(){})
    }

    fun setUpView(){
        getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        viewPagerOnPageChangeCallBack()
      //get Data
        val args = this.getIntent().getExtras()
        val currentIndex: Int = args?.get("current_position") as Int
        wallpapers = args?.let {
            it.getParcelableArrayList("wallpapers")!!
        }
        urlLoadMore = args?.let {
            it.getString("load_more_url")!!
        }
        Log.e("url truoc khi call api", urlLoadMore)
        val isShow: Boolean = args?.get("is_show") as Boolean
        //config Adapter
        adapter = AdapterDetailWallpaperFragment(wallpapers, isShow)
        binding.detailWallpaperViewpager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        binding.detailWallpaperViewpager.adapter = adapter
        binding.detailWallpaperViewpager.setCurrentItem(currentIndex, false)
        actionBack()
        //configLayout
        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, insets ->
            Log.e("Insert",""+ insets.systemWindowInsetTop)
            Log.e("Insert", ""+ insets.systemWindowInsetBottom)
            if (insets.systemWindowInsetBottom > 0){
                binding.btnBackDetailWallpaper.setMarginTop(insets.systemWindowInsetTop)
            }
            insets.consumeSystemWindowInsets()
        }

    }

    fun viewPagerOnPageChangeCallBack(){
        binding.detailWallpaperViewpager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                if (position == wallpapers.size - 2) {
                    Log.e("loadMore", "loadmore")
                    if (urlLoadMore != ""){
                        viewModel.loadMoreItem(urlLoadMore)
                    }
                }
                super.onPageSelected(position)
            }
        })
    }

    fun bind(){
        viewModel.wallpapers.observe(this){
            it?.let { cate ->
                wallpapers.addAll(cate.data)
                adapter.notifyDataSetChanged()
                urlLoadMore = cate.load_more_item
                Log.e("wallpaper=======", ""+ cate.load_more_item)
                Log.e("url sau khi call api", urlLoadMore)
                Log.e("url=======", ""+ urlLoadMore)
            }
        }
    }

    fun View.setMarginTop(marginTop: Int) {
        val menuLayoutParams = this.layoutParams as ViewGroup.MarginLayoutParams
        menuLayoutParams.setMargins(0, marginTop, 0, 0)
        this.layoutParams = menuLayoutParams
    }

    fun actionBack() {
        binding.btnBackDetailWallpaper.setOnClickListener {
            finish()
        }
    }

}