package com.ow.rt07.ui.main.wallpapers

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.databinding.RowWallpaperBinding
import com.ow.rt07.model.wallpaper.BaseData
import com.ow.rt07.model.wallpaper.CateWallpaper
import com.ow.rt07.model.wallpaper.MenuWallpaper
import com.ow.rt07.model.wallpaper.Wallpapers
import com.ow.rt07.network.RTApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdapterWallpaper(var wallpaper: Wallpapers = Wallpapers()) :
    RecyclerView.Adapter<AdapterWallpaper.WallpaperHolder>() {

    class WallpaperHolder(val binding: RowWallpaperBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)

    lateinit var adapterItemWallpaper: AdapterItemWallpaper

    override fun getItemViewType(position: Int): Int {
        return if (wallpaper.categories.size > 0) {
            position
        } else {
            position + 1
        }
    }

    fun setContent(value: Wallpapers) {
        wallpaper = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WallpaperHolder {
        val binding =
            RowWallpaperBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        binding.btnSeeAll.setOnClickListener {
            val action =
                WallpaperFragmentDirections.actionToCategoryWallpaperFragment(
                    wallpaper.categories[viewType].see_all,
                    wallpaper.categories[viewType].name
                )
            findNavController(parent).navigate(action)
        }
        binding.txtTitleCateWallpaper.text = wallpaper.categories[viewType].name

        adapterItemWallpaper = AdapterItemWallpaper(
            wallpaper.categories[viewType].items,
            object : AdapterRecyclerViewInterface {
                override fun onItemClicked(index: Int) {
                    postView(wallpaper.categories[viewType].items[index].id)
                    val intent = Intent(parent.context, DetailWallpaperActivity::class.java)
                    val bundle = Bundle()
                    bundle.putBoolean("is_show", true)
                    bundle.putInt("current_position", index)
                    bundle.putParcelableArrayList(
                        "wallpapers",
                        wallpaper.categories[viewType].items
                    )
                    bundle.putString(
                        "load_more_url",
                        wallpaper.categories[viewType].load_more_item
                    )

                    intent.putExtras(bundle)
                    parent.context.startActivity(intent)

                }
            })
        binding.recyclerViewItemWallpaper.layoutManager =
            LinearLayoutManager(
                parent.context,
                LinearLayoutManager.HORIZONTAL,
                false
            )
        binding.recyclerViewItemWallpaper.adapter = adapterItemWallpaper
        return WallpaperHolder(binding, parent)
    }

    fun postView(id: Int){
        var rtApi: RTApi = RTApi.invoke()
        rtApi.postView(id).enqueue(object : Callback<BaseData> {
            override fun onResponse(call: Call<BaseData>, response: Response<BaseData>) {
                response.body()?.let {
                    Log.e("success", it.message)
                }
            }

            override fun onFailure(call: Call<BaseData>, t: Throwable) {
                Log.e("error", "" + t)
            }
        })

    }

    override fun onBindViewHolder(holder: WallpaperHolder, position: Int) {
//        adapterItemWallpaper.setContent(wallpaper.categories[position].items)
    }

    override fun getItemCount(): Int {
        return wallpaper.categories.size
    }
}
