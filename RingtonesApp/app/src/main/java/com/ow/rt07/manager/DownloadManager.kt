package com.ow.rt07.manager

import android.app.Application
import android.content.Context
import android.util.Log
import java.io.File

interface DownloadManagerInterface {
    fun downloadProgress(percent:Float)
    fun downloadCompleted()
    fun downloadError()
}
class DownloadManager(private val context : Context) {

}