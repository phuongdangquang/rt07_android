package com.ow.rt07.utils.audioplayer.exception

class PlayerDataSourceException : AudioException() {
    override val type: Int
        get() = PLAYER_DATA_SOURCE_EXCEPTION
}