package com.ow.rt07.ui.main.wallpapers

import android.Manifest
import android.content.ContentValues
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import com.andrefrsousa.superbottomsheet.SuperBottomSheetFragment
import com.ow.rt07.databinding.ChooseQualityWallpaperFragmentBinding
import com.ow.rt07.model.wallpaper.ItemX
import com.squareup.picasso.Picasso
import java.io.File
import java.io.FileOutputStream
import com.devhoony.lottieproegressdialog.LottieProgressDialog
import com.ow.rt07.R
import com.ow.rt07.model.wallpaper.BaseData
import com.ow.rt07.network.RTApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.io.IOException
import java.io.OutputStream
import java.net.HttpURLConnection
import java.net.URL

interface DownloadInterface {
    fun onClickDownload(success: Boolean, bitmap: Bitmap) {}
}

class ChooseWallpaperQualityFragment(
    val itemList: ArrayList<ItemX> = arrayListOf(),
    val currentPostion: Int = 0,
    val listener: DownloadInterface
) :
    SuperBottomSheetFragment() {

    private val REQ_CODE_STORAGE_PERMMISION = 2
    private lateinit var binding: ChooseQualityWallpaperFragmentBinding
    var loading: LottieProgressDialog? = null
    var success: LottieProgressDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = ChooseQualityWallpaperFragmentBinding.inflate(inflater, container, false)

//        binding.btnFullhd.setOnClickListener {
//            var dialog = DialogErrorFragment()
//            dialog.show(requireActivity().supportFragmentManager, "customDialog")
//        }

        if (loading == null) {
            loading = context?.let {
                LottieProgressDialog(
                    context = it,
                    isCancel = false,
                    dialogWidth = 160,
                    dialogHeight = 160,
                    animationViewWidth = null,
                    animationViewHeight = null,
                    fileName = "loading.json",
                    title = "   Progressing ...",
                    titleVisible = View.VISIBLE
                )
            }

            success = context?.let {
                LottieProgressDialog(
                    context = it,
                    isCancel = false,
                    dialogWidth = 120,
                    dialogHeight = 120,
                    animationViewWidth = null,
                    animationViewHeight = null,
                    fileName = "success.json",
                    title = "",
                    titleVisible = View.INVISIBLE
                )
            }
        }
        saveImg4K()
        saveImgFullHD()
        return binding.root
    }

    fun saveImgFullHD() {
        binding.btnFullhd.setOnClickListener {
            checkStoragePermission()
        }
    }

    fun saveImg4K() {
        binding.btn4k.setOnClickListener {
            checkStoragePermission()
//            postDownload(itemList[currentPostion].id)
//            saveImage(itemList[currentPostion].image_4k)
        }
    }


    fun saveImageToStorage(bitmap: Bitmap) {
//        var bitmap: Bitmap =
//            (holder.binding.imgDetailWallpaper.drawable as BitmapDrawable).bitmap
        //Generating a file name
        val filename = "${System.currentTimeMillis()}.png"
        //Output stream
        var fos: OutputStream? = null
        //For devices running android >= Q
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            //getting the contentResolver
            activity?.contentResolver?.also { resolver ->
                //Content resolver will process the contentvalues
                val contentValues = ContentValues().apply {
                    //putting file information in content values
                    put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
                    put(MediaStore.MediaColumns.MIME_TYPE, "image/png")
                    put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DCIM)
                }
                //Inserting the contentValues to contentResolver and getting the Uri
                val imageUri: Uri? =
                    resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
                //Opening an outputstream with the Uri that we got
                fos = imageUri?.let { resolver.openOutputStream(it) }
            }
        } else {
            //These for devices running on android < Q
            //So I don't think an explanation is needed here
            val imagesDir =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
            val image = File(imagesDir, filename)
            fos = FileOutputStream(image)
        }
        fos?.use {
            //Finally writing the bitmap to the output stream that we opened
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, it)
//            loading?.hide()
//            this.dismiss()
//            success?.show()
//            Handler().postDelayed({
//                success?.hide()
//            }, 2000)
            listener.onClickDownload(true, bitmap)
            this.dismiss()
        }
    }

    fun postDownload(id: Int) {
        var rtApi: RTApi = RTApi.invoke()
        rtApi.postDownload(id).enqueue(object : Callback<BaseData> {
            override fun onResponse(call: Call<BaseData>, response: Response<BaseData>) {
                response.body()?.let {
                    Log.e("success", it.message)
                }
            }

            override fun onFailure(call: Call<BaseData>, t: Throwable) {
                Log.e("error", "" + t)
            }
        })

    }

    fun saveImage(imgaeQuality: String) {
        Picasso.get().load(imgaeQuality)
            .into(object : com.squareup.picasso.Target {
                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    bitmap?.let {
                        saveImageToStorage(bitmap)
                    }
                }

                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
            })
    }


    private fun checkStoragePermission() {
        val permissions = ArrayList<String>()
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_DENIED || checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_DENIED
        ) {
            requestPermissions(permissions.toTypedArray(), REQ_CODE_STORAGE_PERMMISION)
        } else {
            postDownload(itemList[currentPostion].id)
            saveImage(itemList[currentPostion].image)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQ_CODE_STORAGE_PERMMISION) {
            var denied = false
            for (i in permissions.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    denied = true
                    break
                }
            }

            if (denied) {
                Toast.makeText(
                    requireContext(),
                    getString(R.string.permission_error),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                postDownload(itemList[currentPostion].id)
                saveImage(itemList[currentPostion].image)
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }


    override fun getPeekHeight(): Int {
        return 600
    }

    override fun getCornerRadius(): Float {
        return 20f
    }

    override fun getExpandedHeight(): Int {
        return -2
    }
}



