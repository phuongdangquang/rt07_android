package com.ow.rt07.ui.main.wallpapers

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import co.lujun.androidtagview.TagView
import com.ow.rt07.R
import com.ow.rt07.databinding.ItemDetailWallpaperBinding
import com.ow.rt07.model.wallpaper.ItemX
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception

class AdapterDetailWallpaperFragment(var itemList: ArrayList<ItemX>, val isShow: Boolean) :
    RecyclerView.Adapter<AdapterDetailWallpaperFragment.DetailWallpaperHolder>(),
    DownloadInterface {

    class DetailWallpaperHolder(val binding: ItemDetailWallpaperBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)

    fun setContent(value: ArrayList<ItemX>) {
        itemList.addAll(value)
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DetailWallpaperHolder {
        val binding =
            ItemDetailWallpaperBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DetailWallpaperHolder(binding, parent)
    }

    override fun onBindViewHolder(holder: DetailWallpaperHolder, position: Int) {
        holder.binding.loadingView.visibility = View.VISIBLE
        Log.e("cout-------------", ""+itemList.size)
        if (isShow == true){
            holder.binding.tagView.visibility = View.VISIBLE
        }else{
            holder.binding.tagView.visibility = View.INVISIBLE
        }

        if (itemList[position].vip == 0) {
            holder.binding.icProWallpaper.visibility = View.INVISIBLE
        } else {
            holder.binding.icProWallpaper.visibility = View.VISIBLE
        }

        val views: Int = itemList[position].view
        val typeface = ResourcesCompat.getFont(holder.parent.context, R.font.helv_medium)

        val tagList = itemList[position].tag.split(", ")
        val tags = arrayListOf<String>()

        for (i in tagList){
            tags.add("#" + i)
        }

        Picasso.get().load(itemList[position].image)
            .into(holder.binding.imgDetailWallpaper, object : Callback{
                override fun onSuccess() {
                    holder.binding.loadingView.visibility = View.INVISIBLE
                }

                override fun onError(e: Exception?) {

                }
            })

        holder.binding.txtViews.text = "$views views"
        //config tagList
        holder.binding.tagView.tagTextColor = Color.WHITE
        holder.binding.tagView.tagBackgroundColor = Color.TRANSPARENT
        holder.binding.tagView.tagBorderColor = Color.TRANSPARENT
        holder.binding.tagView.backgroundColor = Color.TRANSPARENT
        holder.binding.tagView.borderColor = Color.TRANSPARENT
        holder.binding.tagView.borderWidth = 0F
        holder.binding.tagView.setTagTypeface(typeface)
        holder.binding.tagView.removeAllTags()
        holder.binding.tagView.setTags(tags)

        holder.binding.tagView.setOnTagClickListener(object : TagView.OnTagClickListener {
            override fun onTagClick(position: Int, text: String) {
                val tagText = holder.binding.tagView.getTagText(position).replace("#", "")
                val intent = Intent(holder.parent.context, SearchWallpaperActivity::class.java)
                intent.putExtra("tag",tagText)
                holder.parent.context.startActivity(intent)
            }
            override fun onTagLongClick(position: Int, text: String?) {}
            override fun onSelectedTagDrag(position: Int, text: String?) {}
            override fun onTagCrossClick(position: Int) {}
        })

        //action download
        holder.binding.btnDownloadWallpaper.setOnClickListener {
            ChooseWallpaperQualityFragment(itemList, position, object : DownloadInterface {
                override fun onClickDownload(success: Boolean, bitmap: Bitmap) {
                    if (success == true) {
                        SetWallpaperSheet(bitmap,itemList[position].image).show(
                            (holder.parent.context as AppCompatActivity).supportFragmentManager,
                            "TutorialSetWallpaperFragment"
                        )
                    }
                }
            }).show(
                (holder.parent.context as AppCompatActivity).supportFragmentManager,
                "ChooseWallpaperQualityFragment"
            )
        }

    }

    override fun getItemCount(): Int {
        return itemList.size
    }
}









