package com.ow.rt07.ui.main.wallpapers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.ow.rt07.R
import com.ow.rt07.databinding.ErrorDialogFragmentBinding

class DialogErrorFragment: DialogFragment() {

    lateinit var binding: ErrorDialogFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.FullScreenDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ErrorDialogFragmentBinding.inflate(inflater, container, false)
        binding.btnCloseDialog.setOnClickListener {
            dismiss()
        }
        return binding.root
    }
}