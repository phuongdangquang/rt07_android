package com.ow.rt07.ui.main.maker.info

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andrefrsousa.superbottomsheet.SuperBottomSheetFragment
import com.ow.rt07.databinding.InfoFragmentBinding

class InfoFragment : SuperBottomSheetFragment(){

    private lateinit var binding:InfoFragmentBinding
    private lateinit var viewModel: InfoViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        viewModel = ViewModelProvider(this).get(InfoViewModel::class.java)
        binding = InfoFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    //MARK: - SuperBottomSheetFragment

    override fun getCornerRadius(): Float {
        return 20f
    }

    override fun getExpandedHeight(): Int {
        return -2
    }
}