package com.ow.rt07.ui.main.ringtones

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.databinding.ItemCategoryRingtonesBinding
import com.ow.rt07.databinding.ItemViewAllCategoryBinding
import com.ow.rt07.model.ringtone.CategoryRingtone
import com.ow.rt07.ui.main.wallpapers.AdapterRecyclerViewInterface
import com.squareup.picasso.Picasso


class AdapterItemAllCategoryRingtones(
    val category: List<CategoryRingtone>,
    val listener: AdapterRecyclerViewInterface
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class AllCateHolder(val binding: ItemViewAllCategoryBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)

    class ItemCateHolder(val binding: ItemCategoryRingtonesBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)


    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return 1
        } else if (position == 1) {
            return 2
        } else {
            return 2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 1) {
            val binding =
                ItemViewAllCategoryBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return AllCateHolder(binding, parent)

        } else if (viewType == 2) {
            val binding = ItemCategoryRingtonesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            return ItemCateHolder(binding, parent)
        } else {
            val binding = ItemCategoryRingtonesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            return ItemCateHolder(binding, parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is AllCateHolder -> {
                holder.binding.root.setOnClickListener {
                    val action = RingtonesFragmentDirections.actionNavigationRingtonesToNavigationAllCategoryRingtoneFragment(
                        category.toTypedArray()
                    )
                    Navigation.findNavController(holder.itemView).navigate(action)
                }
            }

            is ItemCateHolder -> {
                Picasso.get().load(category[position].thumb).into(holder.binding.imgCategory)
                holder.binding.txtTileCategory.text = category[position].name
                holder.binding.root.setOnClickListener {
                    listener.onItemClicked(position)
                }
            }

        }
    }

    override fun getItemCount(): Int {
        return category.size
    }


}