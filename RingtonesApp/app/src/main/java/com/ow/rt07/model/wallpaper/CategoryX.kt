package com.ow.rt07.model.wallpaper

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CategoryX(
    val id: Int = 0,
    val items: ArrayList<ItemX> = arrayListOf<ItemX>(),
    val load_more_item: String,
    val name: String,
    val see_all: String
): Parcelable