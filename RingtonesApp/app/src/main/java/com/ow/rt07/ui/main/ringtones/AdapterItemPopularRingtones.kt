package com.ow.rt07.ui.main.ringtones

import android.media.AudioManager
import android.media.MediaPlayer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.R
import com.ow.rt07.databinding.ItemDetailCategoryRingtonesBinding
import com.ow.rt07.model.ringtone.Ringtone
import com.ow.rt07.model.wallpaper.Wallpapers
import com.ow.rt07.ui.main.wallpapers.AdapterRecyclerViewInterface
import com.ow.rt07.utils.audioplayer.RTPlayerCallback
import com.ow.rt07.utils.minute
import java.io.IOException
import kotlin.math.max
import kotlin.time.Duration.Companion.minutes

interface ItemRingtoneInterFace {
    fun onClickPlayRingtone(position: Int) {}
    fun onClickItemRingtone(position: Int){}
}

class AdapterItemPopularRingtones(
    var itemsPopular: ArrayList<Ringtone>,
    val listener: AdapterRecyclerViewInterface,
    val listenerClickPlay: ItemRingtoneInterFace,
) : RecyclerView.Adapter<AdapterItemPopularRingtones.RowItemPopular>() {

    val colorList: ArrayList<Int> = arrayListOf(
        R.drawable.color_1,
        R.drawable.color_2,
        R.drawable.color_3,
        R.drawable.color_4,
        R.drawable.color_5,
        R.drawable.color_6,
        R.drawable.color_7,
        R.drawable.color_8,
        R.drawable.color_9,
        R.drawable.color_10,
        R.drawable.color_11,
        R.drawable.color_12,
        R.drawable.color_13,
        R.drawable.color_14,
        R.drawable.color_15,
        R.drawable.color_16,
        R.drawable.color_17,
        R.drawable.color_18,
        R.drawable.color_19,
        R.drawable.color_20
    )


    class RowItemPopular(val binding: ItemDetailCategoryRingtonesBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root) {

    }

//    fun setContent(value: ArrayList<Ringtone>) {
//        itemsPopular.addAll(value)
//        notifyDataSetChanged()
//    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowItemPopular {
        val binding =
            ItemDetailCategoryRingtonesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )

        return RowItemPopular(binding, parent)
    }

    override fun onBindViewHolder(holder: RowItemPopular, position: Int) {
        val ringtone = itemsPopular[position]
        if (position < 20) {
            holder.binding.viewPlayRingtone.setForeground(
                holder.parent.context.resources.getDrawable(
                    colorList[position]
                )
            )
        } else {
            val idx = position % 20
            holder.binding.viewPlayRingtone.setForeground(
                holder.parent.context.resources.getDrawable(
                    colorList[idx]
                )
            )
        }


        holder.binding.circularProgressBar.apply {
            progressMax = ringtone.duration.toFloat()
        }
        setStatus(holder, ringtone)

        holder.binding.txtNameSongRingtones.text = itemsPopular[position].title
        holder.binding.txtDuration.text = itemsPopular[position].duration.minute()


        holder.binding.root.setOnClickListener {
            listener.onItemClicked(position)
        }

        holder.binding.actionPlay.setOnClickListener {
            holder.binding.btnPlay.visibility = View.VISIBLE
            listenerClickPlay.onClickPlayRingtone(position)

        }

    }


    fun setStatus(holder: RowItemPopular, ringtone: Ringtone) {

        if (ringtone.statusPlay == null) {
            //default
            holder.binding.btnPlay.setImageResource(R.drawable.ic_play_ringtone_home)
            holder.binding.btnPlay.visibility = View.VISIBLE
            holder.binding.loading.visibility = View.INVISIBLE
            holder.binding.circularProgressBar.progress = 0f
        } else {
            when (ringtone.statusPlay) {
                "pause" -> {
                    Log.e(
                        "Pause","pause ringtone"
                                )
                    holder.binding.loading.visibility = View.INVISIBLE
                    holder.binding.btnPlay.visibility = View.VISIBLE
                    holder.binding.btnPlay.setImageResource(R.drawable.ic_play_ringtone_home)

                }

                "processing" -> {
                    Log.e("processing", "" + ringtone.process)
                    holder.binding.btnPlay.setImageResource(R.drawable.ic_pause_ringtone_home)
                    holder.binding.btnPlay.visibility = View.VISIBLE
                    holder.binding.loading.visibility = View.INVISIBLE
                    holder.binding.circularProgressBar.progress = ringtone.process.toFloat()
                }

                "prepared" -> {
                    Log.e("status Prepare", "")
                    holder.binding.btnPlay.setImageResource(R.drawable.ic_pause_ringtone_home)
                    holder.binding.btnPlay.visibility = View.VISIBLE
                    holder.binding.loading.visibility = View.INVISIBLE
                    holder.binding.circularProgressBar.progress = 0f
                }

                "preparing" ->{
                    Log.e("preparing", "")
                    holder.binding.btnPlay.setImageResource(R.drawable.ic_pause_ringtone_home)
                    holder.binding.btnPlay.visibility = View.INVISIBLE
                    holder.binding.loading.visibility = View.VISIBLE
                    holder.binding.circularProgressBar.progress = 0f
                }
            }
        }


    }

    override fun getItemCount(): Int {
        return itemsPopular.size
    }
}