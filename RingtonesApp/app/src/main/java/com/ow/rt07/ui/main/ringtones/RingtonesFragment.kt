package com.ow.rt07.ui.main.ringtones

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.MainActivity
import com.ow.rt07.R
import com.ow.rt07.databinding.RingtonesFragmentBinding
import com.ow.rt07.model.ringtone.CategoryRingtone
import com.ow.rt07.model.ringtone.PopularRingtone
import com.ow.rt07.model.ringtone.Ringtone
import com.ow.rt07.network.RTApi
import com.ow.rt07.ui.main.wallpapers.AdapterRecyclerViewInterface
import com.ow.rt07.ui.main.wallpapers.AdapterSuggestKeyWord
import com.ow.rt07.utils.audioplayer.exception.AudioException
import com.ow.rt07.utils.base.BaseFragment
import kotlinx.android.synthetic.main.row_popular_ringtones.*
import okhttp3.internal.notify
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RingtonesFragment : BaseFragment(), AdapterSuggestKeyWord.SuggestKeywordClickInterface,
    ItemRingtoneInterFace {

    private lateinit var viewModel: RingtonesViewModel
    private lateinit var binding: RingtonesFragmentBinding
    lateinit var adapterRingtonesCategory: AdapterRingtonesCategory
    var load_more_url: String = ""
    var audioManager: PlayerManager? = null
    var popular: ArrayList<Ringtone> = arrayListOf()
    var categories: ArrayList<CategoryRingtone> = arrayListOf()
    var currentPosition: Int = 0
    var currrentTime: Long = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(RingtonesViewModel::class.java)
        binding = RingtonesFragmentBinding.inflate(inflater, container, false)
        (activity as MainActivity).showBottomNavigation()
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e("onCreate", "")
        adapterRingtonesCategory = AdapterRingtonesCategory(categories, popular, this)

    }

    override fun setUpView() {
        super.setUpView()

        (activity as MainActivity).showBottomNavigation()
        binding.loadMoreView.visibility = View.GONE
        binding.btnCancel.visibility = View.GONE
        binding.recylerViewRingtones.visibility = View.VISIBLE
        binding.recyclerViewSuggestRingtones.visibility = View.INVISIBLE
        binding.recyclerViewSearchRingtones.visibility = View.INVISIBLE
        binding.recylerViewRingtones.layoutManager =
            LinearLayoutManager(
                this.context,
                LinearLayoutManager.VERTICAL,
                false
            )
        binding.recylerViewRingtones.adapter = adapterRingtonesCategory
        loadMore()
        getKeywordSuggest()
        getAllCategory()
        action()
        audioSetData()
    }

    override fun bind() {
        super.bind()

        viewModel.categoryRingtones.observe(this) {
            doAsync {
                it?.let {
                        cate -> categories.addAll(cate.data)
                }
                uiThread {

                    adapterRingtonesCategory.notifyDataSetChanged()

                    viewModel.getDataPopular()
                }
            }
        }
        viewModel.populars.observe(this) {
            it?.let {
                load_more_url = it.next_page_url
                doAsync {
                    popular.addAll(it.data)
                    uiThread {
                        binding.loadingView.visibility = View.INVISIBLE

                        adapterRingtonesCategory.adapterRingtone?.notifyDataSetChanged()
                        binding.loadMoreView.visibility = View.GONE
//                        binding.loadMoreView.visibility = View.GONE

                    }
                }

            }
        }

    }

    fun getKeywordSuggest() {
//        viewModel.getLiveDataSuggestKeyword().observe(viewLifecycleOwner, Observer {
//            if (viewModel.categoryRingtones.value != null) {
//                binding.recyclerViewSuggestRingtones.layoutManager =
//                    LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
//                binding.recyclerViewSuggestRingtones.adapter =
//                    AdapterSuggestKeyWord(
//                        viewModel.categoryRingtones.value!!.data,
//                        viewModel.suggestKeyword.value!!.keyword,"ringtone" , this)
//            }
//        })
//        viewModel.getSuggetKeyword()
    }

    fun getAllCategory() {
        if (popular.isEmpty()) {
            binding.loadingView.visibility = View.VISIBLE
            viewModel.getDataAllCategory()

        }
    }



    fun loadMore() {
        binding.recylerViewRingtones.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!binding.recylerViewRingtones.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    Log.e("load more", " load more")
                    if (load_more_url != "") {
                        binding.loadMoreView.visibility = View.VISIBLE
                       viewModel.loadMoreRingtone(load_more_url)

                    }
                }
            }
        })
    }

//    fun getRingtoneMore(url: String) {
//
//        val rtApi: RTApi = RTApi.invoke()
//        rtApi.loadMoreRingtone(url).enqueue(object : Callback<PopularRingtone> {
//            override fun onResponse(
//                call: Call<PopularRingtone>,
//                response: Response<PopularRingtone>
//            ) {
//                response.body()?.let {
////                    adapterRingtonesCategory.setContentPopular(it.data)
//                    load_more_url = it.next_page_url
//
//                    doAsync {
//                        popular.addAll(it.data)
//                        uiThread {
//                            adapterRingtonesCategory.notifyDataSetChanged()
//                            binding.loadMoreView.visibility = View.GONE
//                        }
//                    }
//
//                }
//            }
//
//            override fun onFailure(call: Call<PopularRingtone>, t: Throwable) {
//                Log.e("error", "" + t)
//            }
//        })
//    }

    fun searchRingtone(keyword: String) {
        viewModel.getLiveDataSearchRingtone(keyword).observe(viewLifecycleOwner, Observer {
            binding.recyclerViewSearchRingtones.layoutManager =
                LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
            binding.recyclerViewSearchRingtones.adapter =
                AdapterDetailCategoryRingtones(it!!.data, object : AdapterRecyclerViewInterface {
                    override fun onItemClicked(index: Int) {
                    }
                })
        })
    }


    //MARK: XU LY AUDIO

    override fun onClickPlayRingtone(position: Int) {
        if (currentPosition != position) {
            //xoa trang thai truoc khi gan
            stopMedia()
            //gai lai gia tri moi
            currentPosition = position
            if (currrentTime > 0) {
                audioManager?.playOrPause()
            } else {
                audioManager?.setData(popular[currentPosition].m4r)
            }
        } else {
            if (currrentTime > 0) {
                audioManager?.playOrPause()
            } else {
                audioManager?.setData(popular[currentPosition].m4r)
            }
        }


        Log.e("Play", "PLay")
    }

    fun stopMedia() {
        audioManager?.release()
        val holder: AdapterItemPopularRingtones.RowItemPopular =
            recyclerViewPopularRingtones.findViewHolderForAdapterPosition(currentPosition) as AdapterItemPopularRingtones.RowItemPopular
        popular[currentPosition].statusPlay = null
        popular[currentPosition].process = 0
        adapterRingtonesCategory.adapterRingtone?.setStatus(holder, popular[currentPosition])
        currrentTime = 0
    }

    override fun onClickItemRingtone(position: Int) {
        stopMedia()
        val action =
            RingtonesFragmentDirections.actionNavigationRingtoneToNavigationDetailRingtone(
                popular.toTypedArray(), position
            )
        findNavController().navigate(action)
    }

    fun audioSetData() {
        audioManager = context?.let {
            PlayerManager(it, object : PlayerManagerCallback {
                override fun onPlay() {
                    Log.e("start", "start")
                    (context as Activity).runOnUiThread {
                    }
                }

                override fun onComplete() {
                    Log.e("onStopPlay", "")
                    (context as Activity).runOnUiThread {
                        stopMedia()
                    }
                }

                override fun onPausePlay() {
                    Log.e("onPausePlay", "")
                    (context as Activity).runOnUiThread {
                        val holder: AdapterItemPopularRingtones.RowItemPopular =
                            recyclerViewPopularRingtones.findViewHolderForAdapterPosition(
                                currentPosition
                            ) as AdapterItemPopularRingtones.RowItemPopular
                        popular[currentPosition].statusPlay = "pause"
                        adapterRingtonesCategory.adapterRingtone?.setStatus(
                            holder,
                            popular[currentPosition]
                        )
                    }
                }

                override fun onSeekPlay() {
                    (context as Activity).runOnUiThread {
                    }
                }

                override fun onPrepared() {
                    (context as Activity).runOnUiThread {
                        val holder: AdapterItemPopularRingtones.RowItemPopular =
                            recyclerViewPopularRingtones.findViewHolderForAdapterPosition(
                                currentPosition
                            ) as AdapterItemPopularRingtones.RowItemPopular
                        popular[currentPosition].statusPlay = "prepared"
                        adapterRingtonesCategory.adapterRingtone?.setStatus(
                            holder,
                            popular[currentPosition]
                        )
                        audioManager?.playOrPause()
                    }
                }


                override fun onPreparing() {
                    (context as Activity).runOnUiThread {

                        val holder: AdapterItemPopularRingtones.RowItemPopular =
                            recyclerViewPopularRingtones.findViewHolderForAdapterPosition(
                                currentPosition
                            ) as AdapterItemPopularRingtones.RowItemPopular
                        popular[currentPosition].statusPlay = "preparing"
                        adapterRingtonesCategory.adapterRingtone?.setStatus(
                            holder,
                            popular[currentPosition]
                        )
                    }
                }
                override fun onPlayProgress(mills: Long) {
                    currrentTime = mills
                    Log.e("onPlayProgress", "" + mills)
                    val nowTime = currrentTime / 1000
                    (context as Activity).runOnUiThread {
                        val holder: AdapterItemPopularRingtones.RowItemPopular =
                            recyclerViewPopularRingtones.findViewHolderForAdapterPosition(
                                currentPosition
                            ) as AdapterItemPopularRingtones.RowItemPopular
                        popular[currentPosition].statusPlay = "processing"
                        popular[currentPosition].process = nowTime.toInt()
                        adapterRingtonesCategory.adapterRingtone?.setStatus(
                            holder,
                            popular[currentPosition]
                        )
                    }
                }

                override fun onError(throwable: AudioException?) {
                    Log.e("onplay", "")
                }
            })
        }
    }


    //MARK: ACTION
    override fun tagClick(keyword: String) {
        searchRingtone(keyword)
        binding.searchBar.setText(keyword)
        binding.recyclerViewSuggestRingtones.visibility = View.INVISIBLE
        binding.recylerViewRingtones.visibility = View.INVISIBLE
        binding.recyclerViewSearchRingtones.visibility = View.VISIBLE
    }

    fun action() {
        actionCancel()
        searchViewClickListener()
        actionSetting()
        actionMyTones()
    }

    fun actionSetting() {
        binding.btnSettings.setOnClickListener {
            val action = RingtonesFragmentDirections.actionNavigationRingtonesToNavigationSettings()
            findNavController().navigate(action)
        }
        Log.d("Settings Fragment", "Settings")
    }

    fun actionMyTones() {
        binding.btnMyTones.setOnClickListener {
            val action = RingtonesFragmentDirections.actionNavigationRingtonesToMyToneFragment()
            findNavController().navigate(action)
        }
        Log.d("Maker Fragment", "My Tones")
    }

    fun actionCancel() {
        binding.btnCancel.setOnClickListener {
            binding.searchBar.text = null
            binding.searchBar.clearFocus()
            hideKeyboard()
            binding.btnCancel.visibility = View.GONE
            binding.recylerViewRingtones.visibility = View.VISIBLE
            binding.recyclerViewSearchRingtones.visibility = View.INVISIBLE
            binding.recyclerViewSuggestRingtones.visibility = View.INVISIBLE
            binding.navigationView.visibility = View.VISIBLE
        }
    }

    fun actionHideBtnCancel() {
        if (binding.searchBar.isVisible) {
            val animation = AnimationUtils.loadAnimation(context, R.anim.exit_to_right)
            binding.btnCancel.startAnimation(animation)
        }
    }

    fun actionShowBtnCancel() {
        if (!binding.btnCancel.isVisible) {
            val animation = AnimationUtils.loadAnimation(context, R.anim.enter_from_right)
            binding.btnCancel.startAnimation(animation)
        }
    }

    fun searchViewClickListener() {
        binding.searchBar.apply {
            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_SEARCH -> {
                        hideKeyboard()
                        //action submit
                        Log.e("submit", "submittt")
                        searchRingtone(binding.searchBar.text.toString())
                        binding.recylerViewRingtones.visibility = View.INVISIBLE
                        binding.recyclerViewSuggestRingtones.visibility = View.INVISIBLE
                        binding.recyclerViewSearchRingtones.visibility = View.VISIBLE
                        binding.btnCancel.visibility = View.GONE
                        actionHideBtnCancel()
                    }
                }
                false
            }
            doOnTextChanged { text, _, _, _ ->
                text?.let {
                    Log.e("Search", "on Text Change")
                }
            }
        }
        binding.searchBar.setOnFocusChangeListener(OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                Log.e("clickkkk", "clickkkkkk")
                actionShowBtnCancel()
                binding.btnCancel.visibility = View.VISIBLE
                binding.recyclerViewSuggestRingtones.visibility = View.VISIBLE
                binding.recylerViewRingtones.visibility = View.INVISIBLE
                binding.recyclerViewSearchRingtones.visibility = View.INVISIBLE
                binding.navigationView.visibility = View.GONE
            }
        })
    }


}