package com.ow.rt07.ui.main.maker.choosefile.wifitransfer

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.ow.rt07.MainActivity
import com.ow.rt07.databinding.WifiTransferFragmentBinding
import com.ow.rt07.utils.base.BaseFragment

class WifiTransferFragment : BaseFragment() {

    private lateinit var binding:WifiTransferFragmentBinding
    private lateinit var viewModel: WifiTransferViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        Log.d("Wifi Transfer Fragment", "On Create View !")

        (activity as MainActivity).hideBottomNavigation()
        viewModel = ViewModelProvider(this).get(WifiTransferViewModel::class.java)
        binding = WifiTransferFragmentBinding.inflate(inflater, container,false)
        return binding.root
    }

    override fun setUpView() {
        super.setUpView()

        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.btnDone.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    override fun bind() {
        super.bind()

    }
}