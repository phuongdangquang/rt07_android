package com.ow.rt07.utils

import android.content.Context
import com.ow.rt07.R

object DeviceUtils {
    @JvmStatic
    fun isTabletDevice(context: Context): Boolean {
        return context.resources.getBoolean(R.bool.tablet)
    }
}