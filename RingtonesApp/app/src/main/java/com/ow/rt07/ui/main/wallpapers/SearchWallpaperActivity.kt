package com.ow.rt07.ui.main.wallpapers

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.R
import com.ow.rt07.config.Util
import com.ow.rt07.databinding.ActivitySearchWallpaperBinding
import com.ow.rt07.model.wallpaper.ItemX
import com.wallpaper.WallpaperApplyTask.context

class SearchWallpaperActivity : AppCompatActivity() {
    lateinit var binding: ActivitySearchWallpaperBinding
    lateinit var viewModel: SearchWallpaperViewModel
    var tag: String? = ""
    lateinit var adapterCategoryWallpaper: AdapterCategoryWallpaper
    var wallpapers: ArrayList<ItemX> = arrayListOf()
    var ratio: String = "16:9"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchWallpaperBinding.inflate(layoutInflater)
        setContentView(binding.root)
        applicationContext.let {
            ratio = Util.getRatio(it)
        }
        viewModel = ViewModelProvider(this).get(SearchWallpaperViewModel::class.java)
        tag = intent.getStringExtra("tag")
        setUpView()
        bind()
    }

    fun setUpView() {
        binding.lbTitle.text = "#$tag"
        adapterCategoryWallpaper = AdapterCategoryWallpaper(wallpapers,
            object : AdapterRecyclerViewInterface {
                override fun onItemClicked(index: Int) {
                    val intent =
                        Intent(this@SearchWallpaperActivity, DetailWallpaperActivity::class.java)
                    val bundle = Bundle()
                    bundle.putBoolean("is_show", false)
                    bundle.putInt("current_position", index)
                    bundle.putParcelableArrayList(
                        "wallpapers",
                        wallpapers
                    )
                    intent.putExtras(bundle)
                    this@SearchWallpaperActivity.startActivity(intent)
                }
            })
        binding.recyclerViewCategoryWallpaper.layoutManager = GridLayoutManager(this, 2)
        binding.recyclerViewCategoryWallpaper.adapter = adapterCategoryWallpaper
    }

    fun bind() {
        tag?.let { searchWallpaper(it) }
        actionBack()
    }

    fun searchWallpaper(keyword: String) {
        viewModel.getWallpaper(keyword, ratio)
            .observe(this, Observer {
                it?.let {
                    adapterCategoryWallpaper.setContent(it.wallpapers)
                    binding.loadingView.visibility = View.GONE
                }
            })
    }

    fun actionBack() {
        binding.btnBackSearchWallpaper.setOnClickListener {
            finish()
        }
    }
}