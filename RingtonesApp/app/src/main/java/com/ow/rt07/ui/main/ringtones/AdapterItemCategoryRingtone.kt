package com.ow.rt07.ui.main.ringtones

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.databinding.ItemAllCategoryRingtoneBinding
import com.ow.rt07.databinding.ItemCategoryRingtonesBinding
import com.ow.rt07.model.ringtone.CategoryRingtone
import com.ow.rt07.ui.main.wallpapers.AdapterRecyclerViewInterface
import com.squareup.picasso.Picasso

class AdapterItemCategoryRingtone(val category: Array<CategoryRingtone>, val listener: AdapterRecyclerViewInterface) : RecyclerView.Adapter<AdapterItemCategoryRingtone.ItemCategoryHolder>() {

    class ItemCategoryHolder(val binding: ItemAllCategoryRingtoneBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemCategoryHolder {
        val binding =
            ItemAllCategoryRingtoneBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemCategoryHolder(binding, parent)
    }

    override fun onBindViewHolder(holder: ItemCategoryHolder, position: Int) {
        Picasso.get().load(category[position].thumb).into(holder.binding.imgCategory)
        holder.binding.txtTileCategory.text = category[position].name

        holder.binding.root.setOnClickListener{
            listener.onItemClicked(position)
        }
    }

    override fun getItemCount(): Int {
        return category.size
    }
}