package com.ow.rt07.config

import android.content.Context
import android.os.Build
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowManager

public class Util {
 companion object{
     fun getRatio(context: Context): String{
         val metrics: DisplayMetrics = context.resources.displayMetrics
         val width = metrics.widthPixels
         val height = metrics.heightPixels + getNavigationBarHeight(context)
         val ratio = ((height / width) * 10)
         Log.e("heightpixel",""+ height)
         Log.e("widthpixel",""+ width)
         Log.e("ratioScreen",""+ ratio)
         if (ratio in 14..17 ) {
             return "16:9"
         }else if (ratio <= 13) {
             return "4:3"
         }else if (ratio >= 18) {
             return "18:9"
         }
         return "16:9"
     }

     private fun getNavigationBarHeight(context: Context): Int {
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
             val metrics = DisplayMetrics()
             val display = (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager)
                 .defaultDisplay
             display.getMetrics(metrics)
             val usableHeight = metrics.heightPixels
             display.getRealMetrics(metrics)
             val realHeight = metrics.heightPixels
             return if (realHeight > usableHeight) realHeight - usableHeight else 0
         }
         return 0
     }
 }

}