package com.ow.rt07.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Category(
    val desc: String = "",
    val icon:String = "",
    val id:Int = 0,
    val iso:String = "",
    val name:String = "",
    val total:String = "",
    var thumb_land : String = ""
): Parcelable