package com.ow.rt07.ui.main.maker

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.ow.rt07.MainActivity
import com.ow.rt07.databinding.MakerFragmentBinding
import com.ow.rt07.utils.safeNavigate
import com.ow.rt07.utils.base.BaseFragment

class MakerFragment : BaseFragment() {

    private lateinit var viewModel: MakerViewModel
    private lateinit var binding: MakerFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("Maker Fragment", "On Create View !")

        (activity as MainActivity).showBottomNavigation()
        viewModel = ViewModelProvider(this).get(MakerViewModel::class.java)
        binding = MakerFragmentBinding.inflate(inflater, container,false)
        return binding.root
    }

    override fun setUpView() {
        super.setUpView()

        binding.btnRingtoneMaker.setOnClickListener{
            Log.d("Maker Fragment","Ringtone Maker")
            val action = MakerFragmentDirections.actionToChooseFile()
            findNavController().safeNavigate(action)
        }

        binding.btnMixMatch.setOnClickListener {
            Log.d("Maker Fragment","Mix Match")
             val action = MakerFragmentDirections.actionToMixMatch()
            findNavController().safeNavigate(action)
        }

        binding.btnRecord.setOnClickListener {
            Log.d("Maker Fragment","Recorder")
            val action = MakerFragmentDirections.actionToRecorder()
            findNavController().safeNavigate(action)
        }
    }

    override fun bind() {
        super.bind()
    }

}