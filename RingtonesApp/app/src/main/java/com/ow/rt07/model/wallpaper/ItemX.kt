package com.ow.rt07.model.wallpaper

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemX(
    val id: Int,
    val image: String,
    val image_4k: String,
    val image_thumb: String,
    val name: String,
    val reward_ads: Int,
    val tag: String,
    val view: Int,
    val vip: Int
): Parcelable