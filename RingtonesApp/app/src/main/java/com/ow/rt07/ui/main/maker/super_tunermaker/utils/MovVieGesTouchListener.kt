package com.ow.rt07.ui.main.maker.super_tunermaker.utils

import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View

enum class ONSWIPE {
    NONESWIPE,
    RIGHT,
    LEFT,
}

private const val LOG_TAG = "MovVieGesTouchListener"

interface ViewParamsChanged {
    fun onViewParamsChanged (leftMargin: Float, topMargin: Float, width: Int, height: Int, onSwipe:ONSWIPE)
}

interface TouchDetect {
    fun isTouch ()
}

class MovVieGesTouchListener : View.OnTouchListener {

    var onSwipe:ONSWIPE = ONSWIPE.NONESWIPE
    var listener:ViewParamsChanged? = null
    var listenerTouch:TouchDetect? = null
    var mGestureDetector: GestureDetector
    lateinit var mView: View
    private var screenHeight = 0
    private var screenWidth = 0

    constructor(mRoot: View, mView: View, lis: ViewParamsChanged, lisTouchDetect:TouchDetect) {

        listener = lis
        listenerTouch = lisTouchDetect
        mGestureDetector = GestureDetector(mView.context,mGestureListener)
        this.mView = mView
        mRoot.viewTreeObserver
            .addOnGlobalLayoutListener{
                screenHeight = mRoot.height
                screenWidth = mRoot.width
        }
    }


    override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
        listenerTouch?.isTouch()
        return mGestureDetector.onTouchEvent(p1)
    }

    private val mGestureListener = object : GestureDetector.SimpleOnGestureListener() {
        private var dX: Float = 0.toFloat()
        private var dY: Float = 0.toFloat()

        override fun onDown(e: MotionEvent): Boolean {
            dX = mView.x - e.rawX
            dY = mView.y - e.rawY
            return true
        }


        override fun onLongPress(e: MotionEvent?) {
            super.onLongPress(e)
        }

        override fun onSingleTapUp(e: MotionEvent?): Boolean {
            return false
        }

        override fun onScroll(
            e1: MotionEvent,
            e2: MotionEvent,
            distanceX: Float,
            distanceY: Float
        ): Boolean {
            var newX = e2.rawX + dX
            var newY = e2.rawY + dY
            // check if the view out of screen
            if (newX <= 0) {
                newX = 0f
            }
            if (newX >= screenWidth - mView!!.width) {
                newX = (screenWidth - mView.width).toFloat()
            }
            if (newY <= 0) {
                newY = 0f
            }
            if (newY >= screenHeight - mView.height) {
                newY = (screenHeight - mView.height).toFloat()
            }

            mView.x = newX

            try {
                val diffX = e2.x - e1.x
                onSwipe = if (diffX > 0) {
                    ONSWIPE.RIGHT
                } else {
                    ONSWIPE.LEFT
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
            }

            listener?.onViewParamsChanged(newX, newY, mView!!.width, mView!!.height, onSwipe)

            return true
        }

    }
}