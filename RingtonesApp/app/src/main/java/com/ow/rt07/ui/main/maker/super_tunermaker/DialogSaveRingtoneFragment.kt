package com.ow.rt07.ui.main.maker.super_tunermaker

import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.ow.rt07.R
import com.ow.rt07.databinding.SaveRingtoneDialogFragmentBinding

interface DialogSaveRingtoneInterface {
    fun onSavedWithName(name:String)
}

private const val LOG_TAG = "Dialog Save RingtoneFragment"

class DialogSaveRingtoneFragment(private val listener: DialogSaveRingtoneInterface): DialogFragment() {

    lateinit var binding: SaveRingtoneDialogFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.FullScreenDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SaveRingtoneDialogFragmentBinding.inflate(inflater, container, false)

        binding.btnCancel.setOnClickListener {
            dismiss()
        }
        binding.btnSave.setOnClickListener {
            hideKeyboard()
            dismiss()
            listener.onSavedWithName(binding.tftName.text.toString())
        }

        if (arguments != null) {
            val mArgs = arguments
            var nameA = mArgs!!.getString("title")

            Log.d(LOG_TAG,"Name : $nameA")

            if (nameA != null) {
                binding.tftName.text = nameA.toEditable()
            }
        }

        return binding.root
    }

    private fun hideKeyboard(){
        val inputMethodManager = ContextCompat.getSystemService(requireContext(), InputMethodManager::class.java)
        inputMethodManager?.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    fun String.toEditable(): Editable =  Editable.Factory.getInstance().newEditable(this)
}