package com.ow.rt07.ui.main.wallpapers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ow.rt07.model.wallpaper.CateWallpaper
import com.ow.rt07.network.RTApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailWallpaperActivityViewModel: ViewModel() {

    var wallpapers: MutableLiveData<CateWallpaper?> = MutableLiveData()

    fun loadMoreItem(url: String) {
        val rtApi: RTApi = RTApi.invoke()
        rtApi.loadMoreWallpaper(url).enqueue(object : Callback<CateWallpaper> {
            override fun onResponse(call: Call<CateWallpaper>, response: Response<CateWallpaper>) {
                wallpapers.postValue(response.body())
            }
            override fun onFailure(call: Call<CateWallpaper>, t: Throwable) {
                wallpapers.postValue(null)
            }
        })
    }

}