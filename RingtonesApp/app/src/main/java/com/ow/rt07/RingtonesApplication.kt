package com.ow.rt07

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.LifecycleObserver
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.ow.rt07.manager.AppPreferences

private const val LOG_TAG = "RingtonesApplication"

class RingtonesApplication : MultiDexApplication(), LifecycleObserver {


    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)

    }
    override fun onCreate() {
        super.onCreate()

        Log.e(LOG_TAG, "onCreate")

        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(this));
        appInstance = this

        AppPreferences.init(this)
    }

    companion object {

        lateinit var appInstance: RingtonesApplication
            private set

        val appContext: Context by lazy {
            appInstance.applicationContext
        }
    }
}