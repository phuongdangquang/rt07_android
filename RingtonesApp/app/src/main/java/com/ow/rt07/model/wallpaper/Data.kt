package com.ow.rt07.model.wallpaper

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Data(
    val wallpapers: Wallpapers,
    val keyword: String,
): Parcelable

