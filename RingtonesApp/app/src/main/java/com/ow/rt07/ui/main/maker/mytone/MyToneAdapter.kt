package com.ow.rt07.ui.main.maker.mytone

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.databinding.MytoneItemBinding
import com.ow.rt07.utils.FileUtil
import com.ow.rt07.utils.minute
import java.io.File

interface MyToneAdapterInterface {
    fun onItemClicked(index: Int, title: String)
}

class MyToneAdapter(var context:Context, private val listener: MyToneAdapterInterface) : RecyclerView.Adapter<MyToneAdapter.MyToneItemViewHolder>() {

    private var items : Array<out File>? = null

    class MyToneItemViewHolder(val binding:MytoneItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyToneItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = MytoneItemBinding.inflate(layoutInflater,parent,false)
        return MyToneItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyToneItemViewHolder, position: Int) {
        val item = items?.get(position)
        if (item != null) {

            holder.binding.txtTitle.text = item.name
            holder.binding.txtDuration.text = FileUtil.audioFileDuration(context, item.path).minute()
        }

        holder.binding.contentView.setOnClickListener {

            Log.d("MyToneAdapter", "Click ....")

            item?.let { it1 -> listener.onItemClicked(position, it1?.name) }
        }
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    fun setItems(value: Array<out File>?){
        items = value
        notifyDataSetChanged()
    }
}