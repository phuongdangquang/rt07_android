package com.ow.rt07.ui.main.ringtones

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.databinding.ItemDetailCategoryRingtonesBinding
import com.ow.rt07.model.ringtone.Ringtone
import com.ow.rt07.ui.main.wallpapers.AdapterRecyclerViewInterface

class AdapterDetailCategoryRingtones(val ringtones: List<Ringtone>, val listener: AdapterRecyclerViewInterface): RecyclerView.Adapter<AdapterDetailCategoryRingtones.RowItemDetailCateHolder>() {

    class RowItemDetailCateHolder(val binding: ItemDetailCategoryRingtonesBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowItemDetailCateHolder {
        val binding =
            ItemDetailCategoryRingtonesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RowItemDetailCateHolder(binding, parent)
    }

    override fun onBindViewHolder(holder: RowItemDetailCateHolder, position: Int) {
        holder.binding.txtNameSongRingtones.text = ringtones[position].title
        holder.binding.root.setOnClickListener {
            listener.onItemClicked(position)
        }
    }

    override fun getItemCount(): Int {
        return ringtones.size
    }
}