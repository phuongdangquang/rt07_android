package com.ow.rt07.ui.main.wallpapers

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.MainActivity
import com.ow.rt07.databinding.CategoryWallpaperFragmentBinding
import com.ow.rt07.model.wallpaper.CateWallpaper
import com.ow.rt07.network.RTApi
import com.ow.rt07.utils.base.BaseFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CategoryWallpaperFragment : BaseFragment() {

    private lateinit var binding: CategoryWallpaperFragmentBinding
    lateinit var viewModel: CategoryWallpaperViewModel
    val args: CategoryWallpaperFragmentArgs by navArgs()
    var wallpapers: CateWallpaper = CateWallpaper()
    lateinit var adapterCategoryWallpaper: AdapterCategoryWallpaper
    var loadMoreURL: String = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(CategoryWallpaperViewModel::class.java)
        binding = CategoryWallpaperFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun setUpView() {
        super.setUpView()
        (activity as MainActivity).hideBottomNavigation()
        binding.loadMoreView.visibility = View.GONE
        binding.lbTitle.text = args.titleCategory
        loadMore()
        actionBack()
        if (wallpapers == CateWallpaper()){
            getCateWallpaper(args.urlSeeAll)
        }
        adapterCategoryWallpaper =
            AdapterCategoryWallpaper(wallpapers.data, object : AdapterRecyclerViewInterface {
                override fun onItemClicked(index: Int) {
                    val intent = Intent(context, DetailWallpaperActivity::class.java)
                    val bundle = Bundle()
                    bundle.putBoolean("is_show", true)
                    bundle.putInt("current_position", index)
                    bundle.putParcelableArrayList(
                        "wallpapers",
                        wallpapers.data
                    )
                    bundle.putString(
                        "load_more_url",
                        loadMoreURL
                    )
                    intent.putExtras(bundle)
                    context!!.startActivity(intent)
                }
            })
        binding.recyclerViewCategoryWallpaper.layoutManager = GridLayoutManager(context, 2)
        binding.recyclerViewCategoryWallpaper.adapter = adapterCategoryWallpaper
    }

    fun getCateWallpaper(url: String) {
        var rtApi: RTApi = RTApi.invoke()
        rtApi.getCateWallpaper(url).enqueue(object : Callback<CateWallpaper> {
            override fun onResponse(call: Call<CateWallpaper>, response: Response<CateWallpaper>) {
                response.body()?.let {
                    wallpapers.data.addAll(it.data)
                    loadMoreURL = it.load_more_item
//                    adapterCategoryWallpaper.loadMore(false)
                    adapterCategoryWallpaper.notifyDataSetChanged()
//                    if(wallpapers != CateWallpaper()){
                    binding.loadingView.visibility = View.GONE
                    binding.loadMoreView.visibility = View.GONE
//                    }
//                    adapterCategoryWallpaper.setContent(it.data)
                }
            }

            override fun onFailure(call: Call<CateWallpaper>, t: Throwable) {
                Log.e("error", "" + t)
            }
        })
    }

    fun loadMore() {
        binding.recyclerViewCategoryWallpaper.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!binding.recyclerViewCategoryWallpaper.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    Log.e("load more", " load more")
                    binding.loadMoreView.visibility = View.VISIBLE
                    if (loadMoreURL != "") {

                        getCateWallpaper(url = loadMoreURL)
//                        adapterCategoryWallpaper.loadMore(true)
                        adapterCategoryWallpaper.notifyDataSetChanged()
                    }
                }
            }
        })
    }


//    fun getCateWallpaper(url: String) {
//        viewModel.getWallpaperByCategory(url).observe(viewLifecycleOwner, Observer {
//            it?.let {
//                if (viewModel.categoryWallpaper!!.value != null){
//                wallpapers.addAll(it.data)
//                    adapterCategoryWallpaper =
//                        AdapterCategoryWallpaper(wallpapers, object : AdapterRecyclerViewInterface {
//                            override fun onItemClicked(index: Int) {
//                                val action =
//                                    CategoryWallpaperFragmentDirections.actionNavigationCategoryWallpaperToDetailWallpaperFragment(
//                                        wallpapers.toTypedArray(), index
//                                    )
//                                findNavController().navigate(action)
////                val extras = FragmentNavigatorExtras(view to "image_big")
////                findNavController().navigate(R.id.action_category_wallpaper_fragment_to_navigation_data_detail_wallpaper, null, null, extras)
//                            }
//                        })
//                    binding.recyclerViewCategoryWallpaper.layoutManager = GridLayoutManager(context, 2)
//                    binding.recyclerViewCategoryWallpaper.adapter = adapterCategoryWallpaper
//                }
//            }
//        })
//    }

    fun actionBack() {
        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }


}



