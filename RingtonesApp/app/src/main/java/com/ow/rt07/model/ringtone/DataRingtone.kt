package com.ow.rt07.model.ringtone

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataRingtone(
    val code: Int,
    val `data`: ArrayList<CategoryRingtone>,
    val message: String
): Parcelable