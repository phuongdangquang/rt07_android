package com.ow.rt07.network

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.http.Field
import java.io.File
import com.ow.rt07.network.RTApi



class RTRepository(private val api: RTApi) : SafeApiRequest() {

//    //User
//    suspend fun getUserInfo() = makeApiRequest {
//        api.getUserInfo()
//    }
//
//    /////// profile setting ////////////////
//
//    // merge
//    suspend fun mergeData(new_user_id : Int?= null) = makeApiRequest {
//        api.mergeData(new_user_id)
//    }
//    // upload avatar
//    suspend fun uploadAvatar(file: File) = makeApiRequest {
//        val requestFile: RequestBody = file.asRequestBody("multipart/form-data".toMediaTypeOrNull())
//        val avatar: MultipartBody.Part = MultipartBody.Part.createFormData("avatar", file.getName(), requestFile)
//        api.upLoadAvatar(avatar)
//    }
//    // favorite
//    suspend fun getFavorite() = makeApiRequest {
//        api.getFavorite()
//    }
//    // update password
//    suspend fun updatePassword(currentPass : String ,password : String , confirm_pass : String ) = makeApiRequest {
//        api.updatePassword(password,confirm_pass,currentPass)
//    }
//    // update updateInfo
//    suspend fun updateInfo(
//        about_name: String? = null,
//        gender: String? = null,
//        is_sync_health: String? = null,
//        height: String? = null,
//        dob: String? = null,
//        user_smart_reminder: Int? = null,
//        workout_time: String? = null,
//        attentions: String? = null,
//        workout_days: String? = null,
//        activity_level: String? = null,
//    ) = makeApiRequest {
//        api.updateInfo(about_name,gender,is_sync_health,height,dob,user_smart_reminder,workout_time,attentions,workout_days,activity_level)
//    }
//
//    suspend fun addDeviceToken(token:String) = makeApiRequest {
//        api.addDeviceToken(token)
//    }
//
//    // update workout setting
//    suspend fun updateWorkoutSet(warm_up_enabled : Int? = null ,
//                                 cool_down_enabled : Int? = null ,
//                                 voice_assistant_enabled : Int? = null,
//                                 voice : String? = null ,
//    ) = makeApiRequest {
//        api.updateWorkoutSet(warm_up_enabled,cool_down_enabled,voice_assistant_enabled,voice)
//    }
//
//    //////////////// intro ///////////////////
//    // login
//    suspend fun login(emailOrPhone : String,phone : String,password: String , login_type : String,previous_user_id : Int? = null ) = makeApiRequest {
//        api.login(emailOrPhone,phone,password,login_type,previous_user_id)
//    }
//    //verify email (request)
//    suspend fun verifyEmail(email : String) = makeApiRequest {
//        api.verifyEmail(email)
//    }
//    // request change password
//    suspend fun requestChangePassword(email : String,phone : String,request_type : String) = makeApiRequest {
//        api.requestChangePasswordForEmail(email,phone,request_type)
//    }
//    // verify Otp email
//    suspend fun verifyOtpEmail(email : String , code : Int) = makeApiRequest {
//        api.verifyOtpEmail(email,code)
//    }
//    suspend fun registerEmail(email: String , password: String,confirm_password: String,informationID: Int? = null,previous_user_id : Int? = null) = makeApiRequest{
//        api.registerEmail(email,password,confirm_password,informationID,previous_user_id)
//    }
//
//    suspend fun registerPhone(phone: String , password: String,confirm_password: String,informationID: Int? = null,previous_user_id : Int? = null) = makeApiRequest{
//        api.registerPhone(phone,password,confirm_password,informationID,previous_user_id)
//    }
//
//    //reset password
//    suspend fun resetPassword(code : String , password : String , confirm_password : String) = makeApiRequest {
//        api.resetPassword(code,password,confirm_password)
//    }
//
//    // create preparePlan
//    suspend fun preparePlan(gender : String, height : String , weight : String , goal : String ,
//                            age : String ,
//                            about_name : String ,
//                            is_sync_health : String ,
//                            sync_health_options : String ,
//                            workout_time : String ,
//                            attentions : String ,
//                            workout_days : String ,
//                            user_smart_reminder : Int ,
//                            activity_level : String ,
//                            step_goal : String ,
//                            daily_water : String
//    ) = makeApiRequest {
//        api.preparePlan(gender,
//            height,
//            weight,
//            goal,
//            age,
//            about_name,
//            is_sync_health,
//            sync_health_options,
//            workout_time,
//            attentions,
//            workout_days,
//            user_smart_reminder,
//            activity_level,
//            step_goal,
//            daily_water
//        )
//    }
//    suspend fun updatePreparePlan(gender : String, height : String , weight : String , goal : String ,
//                            age : String ,
//                            about_name : String ,
//                            is_sync_health : String ,
//                            sync_health_options : String ,
//                            workout_time : String ,
//                            attentions : String ,
//                            workout_days : String ,
//                            user_smart_reminder : Int ,
//                            activity_level : String ,
//                            step_goal : String ,
//                            daily_water : String,
//                                  user_id : Int? = null
//    ) = makeApiRequest {
//        api.updatePreparePlan(gender,
//            height,
//            weight,
//            goal,
//            age,
//            about_name,
//            is_sync_health,
//            sync_health_options,
//            workout_time,
//            attentions,
//            workout_days,
//            user_smart_reminder,
//            activity_level,
//            step_goal,
//            daily_water,
//            user_id
//        )
//    }
//    // access now
//    suspend fun accessNow(informationID : Int) = makeApiRequest {
//        api.accessNow(informationID)
//    }
//    //sociall login
//    suspend fun socialLogin(phone : String? = null , provider : String ,provider_id : String,token : String, email : String? = null, name : String? = null ,avatar : String? = null ,informationID : Int ? = null,previous_user_id : Int ? = null) = makeApiRequest {
//
//        api.socialLogin(phone,provider,provider_id,token,email,name,avatar,informationID,previous_user_id)
//    }
//    // logout
//    suspend fun logout() = makeApiRequest {
//        api.logout()
//    }
//    //////////////Home
//    suspend fun home() = makeApiRequest {
//        api.home()
//    }
//    suspend fun search(text:String,limit:Int) = makeApiRequest {
//        api.search(text,limit)
//    }
//    suspend fun suggestions(text: String) = makeApiRequest {
//        api.suggestions("http://google.com/complete/search?output=chrome&q=${text}&alt=json")
//    }
//
//    suspend fun plans() = makeApiRequest {
//        api.plans()
//    }
//    suspend fun planDetail(id:Int) = makeApiRequest {
//        api.planDetail(id)
//    }
//    suspend fun startPlan(id:Int) = makeApiRequest {
//        api.startPlan(id)
//    }
//    suspend fun resetPlan(id:Int) = makeApiRequest {
//        api.resetPlan(id)
//    }
//    suspend fun stopPlan(id:Int) = makeApiRequest {
//        api.stopPlan(id)
//    }
//    suspend fun customizedPlan() = makeApiRequest {
//        api.customizedPlan()
//    }
//    suspend fun reschedule(planId:Int,workoutId: Int,days:String,newDate:String) = makeApiRequest {
//        api.reschedulePlan(planId,workoutId,days,newDate)
//    }
//
//    //Library
//    suspend fun workouts(search: String? = null,
//                         sort:String? = null,
//                         dir:String? = null,
//                         perPage:Int? = null,
//                         page:Int? = null,
//                         bodyPart:Int? = null,
//                         muscle:Int? = null,
//                         level:Int? = null,
//                         duration:Int? = null) = makeApiRequest {
//        api.workouts(search,sort,dir,perPage,page,bodyPart,muscle,level,duration)
//    }
//
//    suspend fun trackerWorkout(date:String,
//                               energy:Int,
//                               duration:Int,
//                               details:String,
//                               workoutId:Int,
//                               customPlanId:String? = null,
//                               customized_plan_days:String? = null) = makeApiRequest {
//        api.trackerWorkout(date,energy,duration,details,workoutId,customPlanId,customized_plan_days)
//    }
//
//    suspend fun filterWorkout() = makeApiRequest {
//        api.filterWorkout()
//    }
//    suspend fun favoriteWorkout(id:Int) = makeApiRequest {
//        api.favoriteWorkout(id)
//    }
//    suspend fun removeExerciserFromFav(id:Int) = makeApiRequest {
//        api.removeExerciserFromFav(id)
//    }
//    suspend fun removeWorkOutFromFav(id:Int) = makeApiRequest {
//        api.removeWorkOutFromFav(id)
//    }
//    suspend fun workoutDetail(id:Int) = makeApiRequest {
//        api.workoutDetail(id)
//    }
//
//    suspend fun exercises(search: String? = null,
//                          sort:String? = null,
//                          dir:String? = null,
//                          page:Int? = null,
//                          per_page:Int? = null,
//                          bodyPart:Int? = null,
//                          level:Int? = null) = makeApiRequest {
//        api.exercises(search,sort,dir,page,per_page,bodyPart,level)
//    }
//
//    suspend fun exerciseDetail(id:Int) = makeApiRequest {
//        api.exerciseDetail(id)
//    }
//    suspend fun favoriteExercise(id:Int) = makeApiRequest {
//        api.favoriteExercise(id)
//    }
//    suspend fun downloadFile(url:String) = makeApiRequest {
//        api.downloadFile(url)
//    }
//
//    //MARK: - LOG
//
//    suspend fun log(from:String,to:String,weight_from:String,weight_to:String,today:String) = makeApiRequest {
//        api.log(from,to,weight_from,weight_to,today)
//    }
//    suspend fun getStepTracker(page:Int) = makeApiRequest {
//        api.getStepTracker(page)
//    }
//    suspend fun getWaterTracker(page:Int) = makeApiRequest {
//        api.getWaterTracker(page)
//    }
//    suspend fun getWeightTracker(page:Int) = makeApiRequest {
//        api.getWeightTracker(page)
//    }
//    suspend fun getWorkoutTracker(page:Int,metrics:String) = makeApiRequest {
//        //metrics : duration hoac energy
//        api.getWorkoutTracker(page,metrics)
//    }
//
//    suspend fun workoutHistory(from:String,to:String) = makeApiRequest {
//        api.workoutHistory(from,to)
//    }
//    suspend fun workoutHistoryTotal() = makeApiRequest {
//        api.workoutHistoryTotal()
//    }
//    suspend fun workoutHistoryDetail(from:String,to:String) = makeApiRequest {
//        api.workoutHistoryDetail(from,to)
//    }
//
//    // post steps
//    suspend fun steps(steps : Int? = null , date : String? = null) = makeApiRequest {
//        api.stepTracker(steps,date)
//    }
//    // post update info tracker
//    suspend fun updateInfoTracker(step_goal : Int? = null ,
//                                  start_weight : String? = null,
//                                  target_weight : String? = null,
//                                  daily_water : Int? = null ,
//                                  activity_level : Int? = null ,
//    ) = makeApiRequest {
//        api.updateInfoTracker(step_goal,start_weight,target_weight,daily_water,activity_level)
//    }
//    // post weight tracker
//    suspend fun weightTracker(weight : String? = null , date : String? = null) = makeApiRequest {
//        api.weightTracker(weight,date)
//    }
//
//    suspend fun waterTracker(amount:Int,date:String) = makeApiRequest {
//        api.waterTracker(amount,date)
//    }
//
//    //MARK: - LEADERBOARD
//    suspend fun leaderboard() = makeApiRequest {
//        api.leaderboard()
//    }
//
//    suspend fun medals() = makeApiRequest {
//        api.medals()
//    }
//    suspend fun rooms(page: Int) = makeApiRequest {
//        api.rooms(page = page)
//    }
//    suspend fun createNewRoom(name:String,type:Int,file: File?) = makeApiRequest {
//        val nameBody: RequestBody = name.toRequestBody("text/plain".toMediaTypeOrNull())
//        val typeBody: RequestBody = type.toString().toRequestBody("text/plain".toMediaTypeOrNull())
//
//        if (file != null) {
//            val requestFile: RequestBody =
//                file.asRequestBody("multipart/form-data".toMediaTypeOrNull())
//            val imageBody: MultipartBody.Part =
//                MultipartBody.Part.createFormData("thumb", file.getName(), requestFile)
//            api.createNewRoom(nameBody, typeBody, imageBody)
//        }else{
//            api.createNewRoom(nameBody, typeBody, null)
//        }
//    }
//
//    suspend fun roomDetail(id:Int) = makeApiRequest {
//        api.roomDetail(id)
//    }
//    suspend fun leaveRoom(id:Int) = makeApiRequest {
//        api.leaveRoom(id)
//    }
//
//    suspend fun joinRoom(code:String) = makeApiRequest {
//        api.joinRoom(code)
//    }
//
//    suspend fun leaderCalories(limit: Int = 20,page:Int) = makeApiRequest {
//        api.leaderCalories(limit,page)
//    }
//    suspend fun leaderTime(limit: Int = 20,page:Int) = makeApiRequest {
//        api.leaderTime(limit,page)
//    }
//    suspend fun leaderWorkout(limit: Int = 20,page:Int) = makeApiRequest {
//        api.leaderWorkout(limit,page)
//    }
//    suspend fun kickUserOutRoom(roomId:Int,memberId:Int) = makeApiRequest {
//        api.kickUserOutRoom(roomId,memberId)
//    }
//    suspend fun createCodeForJoinRoom(roomId:Int) = makeApiRequest {
//        api.createCodeForJoinRoom(roomId)
//    }
//
//    //MARK: - MEDALS
//
//    suspend fun activeMedal(id:Int) = makeApiRequest {
//        api.activeMedal(id)
//    }

    //MARK: - ====================================================

//    suspend fun getAllRingtone(page:Int) = makeApiRequest {
//        api.getAllCategory(page)
//    }
}