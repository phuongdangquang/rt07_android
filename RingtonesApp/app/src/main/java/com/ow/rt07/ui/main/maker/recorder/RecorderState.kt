package com.ow.rt07.ui.main.maker.recorder

enum class StateRecorder {
    BEFORE_RECORDING,
    ON_RECORDING,
    AFTER_RECORDING,
    ON_PAUSE_PLAYING,
    ON_PLAYING
}