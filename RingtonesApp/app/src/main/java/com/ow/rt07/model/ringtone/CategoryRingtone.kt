package com.ow.rt07.model.ringtone

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CategoryRingtone(
    val id: Int,
    val name: String,
    val thumb: String
): Parcelable