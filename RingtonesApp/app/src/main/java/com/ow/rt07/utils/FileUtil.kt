package com.ow.rt07.utils


import android.content.Context
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.util.Log
import com.ow.rt07.config.AppConfig
import java.io.File
import java.util.regex.Matcher
import java.util.regex.Pattern

private const val LOG_TAG = "Audio Effect"

object FileUtil {

    fun audioFileDuration(context:Context, path: String): Int {
        val uri: Uri = Uri.parse(path)
        Log.d(LOG_TAG,"URI : $uri")
        val mmr = MediaMetadataRetriever()
        mmr.setDataSource(context, uri)
        val durationStr: String? = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
        val second = durationStr?.toInt()?.div(1000)
        Log.d(LOG_TAG,"Duration File Cut : $second")
        return second ?: 0
    }

    //File
    fun getPath(context:Context, name:String) : String {
        val folder: File = File((context?.getExternalFilesDir(null)?.absolutePath ?: "") + File.separator + AppConfig.MY_TONE_FOLDER)
        Log.d(LOG_TAG,folder.path)
        var success = true
        if (!folder.exists()) {
            success = folder.mkdirs()
        }
        if (success) {
            // Do something on success
            Log.d(LOG_TAG,"Folder Created !")
        }
        Log.d(LOG_TAG,"Full path : ${context?.getExternalFilesDir(null)?.absolutePath + "/${AppConfig.MY_TONE_FOLDER}/${name}.mp3"}")

        return context?.getExternalFilesDir(null)?.absolutePath + "/${AppConfig.MY_TONE_FOLDER}/${name}.mp3"
    }

    fun getUniqueFileName(file: File): File? {
        var file = file
        val originalFile = file
        return try {
            while (file.exists()) {
                val newFileName = file.name
                var baseName = newFileName.substring(0, newFileName.lastIndexOf("."))
                val extension = getExtension(newFileName)
                val pattern: Pattern =
                    Pattern.compile("( \\(\\d+\\))\\.") // Find ' (###).' in the file name, if it exists
                val matcher: Matcher = pattern.matcher(newFileName)
                var strDigits = ""
                if (matcher.find()) {
                    baseName = baseName.substring(0, matcher.start(0)) // remove the (###)
                    strDigits = matcher.group(0) // grab the ### we'll want to increment
                    strDigits = strDigits.substring(
                        strDigits.indexOf("(") + 1,
                        strDigits.lastIndexOf(")")
                    ) // strip off the ' (' and ').' from the match
                    // increment the found digit and convert it back to a string
                    val count = strDigits.toInt()
                    strDigits = (count + 1).toString()
                } else {
                    strDigits = "1" // if there is no (###) match then start with 1
                }
                file =
                    File(file.parent + "/" + baseName + " (" + strDigits + ")" + extension) // put the pieces back together
            }
            file
        } catch (e: Error) {
            originalFile // Just overwrite the original file at this point...
        }
    }
}