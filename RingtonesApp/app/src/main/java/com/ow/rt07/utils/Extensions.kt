package com.ow.rt07.utils

import androidx.navigation.NavController
import androidx.navigation.NavDirections


private const val LOG_TAG = "Extension"

fun NavController.safeNavigate(direction: NavDirections) {
    currentDestination?.getAction(direction.actionId)?.run { navigate(direction) }
}

fun Int.minute(): String {
    val second: Int = this % 60
    var minute: Int = this / 60
    if (minute >= 60) {
        val hour = minute / 60
        minute %= 60
        return hour.toString() + ":" + (if (minute < 10) "0$minute" else minute) + ":" + if (second < 10) "0$second" else second
    }
    return minute.toString() + ":" + if (second < 10) "0$second" else second
}

fun getExtension(name: String): String {
    return name.substring(name.lastIndexOf("."))
}
