package com.ow.rt07.model.wallpaper

data class CategoryWallpaper(
    val id: Int,
    val items: List<Item>,
    val load_more_item: String,
    val name: String,
    val see_all: String
)

