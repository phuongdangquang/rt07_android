package com.ow.rt07.config;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import androidx.core.graphics.BitmapCompat;

import java.io.IOException;

public class SetWallpaperHelper {

    Context context;
    public SetWallpaperHelper(Context context){
        this.context = context;
    }


    public void setWallpaper(Bitmap bitmap,int type){
        WallpaperManager wallpaperManager = WallpaperManager.getInstance(context);
        wallpaperManager.setWallpaperOffsetSteps(1,1);
        try {
            if (type == 0){
                wallpaperManager.setBitmap(cropBitmapFromCenterAndScreenSize(bitmap));
            }else if (type == 1){

                wallpaperManager.setBitmap(cropBitmapFromCenterAndScreenSize(bitmap),null,true,WallpaperManager.FLAG_SYSTEM);
            }else if (type == 2){
                wallpaperManager.setBitmap(cropBitmapFromCenterAndScreenSize(bitmap),null,true,WallpaperManager.FLAG_LOCK );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private Bitmap cropBitmapFromCenterAndScreenSize(Bitmap bitmap) {
        float screenWidth, screenHeight;
        float bitmap_width = bitmap.getWidth(), bitmap_height = bitmap
                .getHeight();
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE))
                .getDefaultDisplay();

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels + getNavigationBarHeight();

        Log.i("TAG", "bitmap_width " + bitmap_width);
        Log.i("TAG", "bitmap_height " + bitmap_height);

        float bitmap_ratio = (float) (bitmap_width / bitmap_height);
        float screen_ratio = (float) (screenWidth / screenHeight);
        int bitmapNewWidth, bitmapNewHeight;

        Log.i("TAG", "bitmap_ratio " + bitmap_ratio);
        Log.i("TAG", "screen_ratio " + screen_ratio);

        if (screen_ratio > bitmap_ratio) {
            bitmapNewWidth = (int) screenWidth;
            bitmapNewHeight = (int) (bitmapNewWidth / bitmap_ratio);
        } else {
            bitmapNewHeight = (int) screenHeight;
            bitmapNewWidth = (int) (bitmapNewHeight * bitmap_ratio);
        }

        bitmap = Bitmap.createScaledBitmap(bitmap, bitmapNewWidth,
                bitmapNewHeight, true);

        Log.i("TAG", "screenWidth " + screenWidth);
        Log.i("TAG", "screenHeight " + screenHeight);
        Log.i("TAG", "bitmapNewWidth " + bitmapNewWidth);
        Log.i("TAG", "bitmapNewHeight " + bitmapNewHeight);

        int bitmapGapX, bitmapGapY;
        bitmapGapX = (int) ((bitmapNewWidth - screenWidth) / 2.0f);
        bitmapGapY = (int) ((bitmapNewHeight - screenHeight) / 2.0f);

        Log.i("TAG", "bitmapGapX " + bitmapGapX);
        Log.i("TAG", "bitmapGapY " + bitmapGapY);
        bitmap = Bitmap.createBitmap(bitmap,bitmapGapX,bitmapGapY,(int)screenWidth,(int)screenHeight);
//        bitmap = Bitmap.createBitmap(bitmap, bitmapGapX, bitmapGapY,
//                screenWidth,screenHeight);
        return bitmap;
    }

    private int getNavigationBarHeight() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE))
                    .getDefaultDisplay();
            display.getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
            display.getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;
            if (realHeight > usableHeight)
                return realHeight - usableHeight;
            else
                return 0;
        }
        return 0;
    }
}
