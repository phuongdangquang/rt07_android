package com.ow.rt07.ui.main.maker.super_tunermaker

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import com.ow.rt07.R
import com.ow.rt07.databinding.SuperTuneMakerFragmentBinding
import com.ow.rt07.manager.AppPreferences
import com.ow.rt07.ui.main.maker.info.InfoFragment
import com.ow.rt07.ui.main.maker.setRingtone.SetRingtoneBottomSheet
import com.ow.rt07.ui.main.maker.super_tunermaker.utils.MovVieGesTouchListener
import com.ow.rt07.ui.main.maker.super_tunermaker.utils.ONSWIPE
import com.ow.rt07.ui.main.maker.super_tunermaker.utils.TouchDetect
import com.ow.rt07.ui.main.maker.super_tunermaker.utils.ViewParamsChanged
import com.ow.rt07.utils.audioeffect.AudioEffect
import com.ow.rt07.utils.audioeffect.AudioEffectCallback
import com.ow.rt07.utils.audioeffect.AudioSaveEffectCallback
import com.ow.rt07.utils.audioplayer.RTAudioPlayer
import com.ow.rt07.utils.audioplayer.RTPlayerCallback
import com.ow.rt07.utils.audioplayer.exception.AudioException
import com.ow.rt07.utils.base.BaseFragment
import com.ow.rt07.utils.minute
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File
import kotlin.math.roundToInt

enum class MaximumDurationAllowToTrimAudioFile {
    DURATION_7_S,
    DURATION_10_S,
    DURATION_20_S,
    DURATION_30_S,
    DURATION_40_S,
}

private const val LOG_TAG = "SuperTuneMakerFragment"

class SuperTuneMakerFragment : BaseFragment(), DialogSaveRingtoneInterface {

    var startTime:Int = 0
    var trimmedTime:Int = 0
    var endTime:Int = 0
    var duration:Int = 0
    var isLoop:Boolean = false

    private lateinit var binding:SuperTuneMakerFragmentBinding
    private lateinit var viewModel: SuperTuneMakerViewModel

    private val args : SuperTuneMakerFragmentArgs by navArgs()
    private var audioEffect:AudioEffect? = null
    private var maximumDurationAllowToTrimAudioFile:Float = 40F
    private var minimumDurationAllowToTrimAudioFile:Float = 7F
    private var maximumDurationCanBeTrimAudioFile:Float = 40F
    private val pulBarWidth = 50
    private var stateOfMaximumDurationAllowToTrimAudioFile = MaximumDurationAllowToTrimAudioFile.DURATION_20_S
    private var maxDistance:Float = 0F
    private var minDistance:Float = 0F
    private var indicatorPositionXAtMusicBegin:Float = 0F

    var rootWidth:Int = 0
    var speed:Float? = null
    var isFadeIn: Boolean = false
    var isFadeOut: Boolean = false
    var isEffect: Boolean = false
    var player:RTAudioPlayer? = null

    private val progressThunder: ObjectAnimator by lazy {
        ObjectAnimator.ofFloat(binding.waveform, "progress", 0F, 100F).apply {
            interpolator = LinearInterpolator()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this)[SuperTuneMakerViewModel::class.java]
        binding = SuperTuneMakerFragmentBinding.inflate(inflater, container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpWaveFormView()
    }

    override fun setUpView() {
        super.setUpView()

        setUpRangerSeekbar()

        binding.alphabetHorizonScroll.isSmoothScrollingEnabled = true
        binding.alphabetHorizonScroll.viewTreeObserver.addOnScrollChangedListener {
            val scrollX: Int = binding.alphabetHorizonScroll.scrollX
            Log.d(LOG_TAG, "scrollX: $scrollX")

            if (scrollX != 0) {
                player?.release()
            }
            setUpTimer()
        }

        binding.txtSong.text = args.ringtone.title
        binding.txtSinger.text = args.ringtone.artist
        binding.btnBack.setOnClickListener {
            player?.release()
            findNavController().popBackStack()
        }

        binding.btnSave.setOnClickListener {
            Log.d("TAG","Save !")
            if (trimmedTime > 20F){
                if (AppPreferences.isPurchased){
                    val dialog = DialogSaveRingtoneFragment(this)
                    dialog.show(requireActivity().supportFragmentManager, "saveRingtoneFragment")
                }else{
                    Toast.makeText(context, "IAP is not Purchase !", Toast.LENGTH_LONG).show()
                }
            }else{
                val dialog = DialogSaveRingtoneFragment(this)
                val bundle = Bundle()
                bundle.putString("title", "${binding.txtSong.text}")
                dialog.arguments = bundle
                dialog.show(requireActivity().supportFragmentManager, "saveRingtoneFragment")
            }
        }

        binding.segmentSpeed.setOnPositionChangedListener {
            when (it){
                0 ->{
                    speed = null
                    player?.release()
                }
                1 ->{
                    speed = 1.0F
                    player?.release()
                }
                2 ->{
                    speed = 1.3F
                    player?.release()
                }
                3 ->{
                    speed = 1.7F
                    player?.release()
                }
                4 ->{
                    speed = 2.0F
                    player?.release()
                }
            }
        }

        binding.btnPlay.setOnClickListener {
            Log.d(LOG_TAG,"Play !")

            if (isFadeIn || isFadeOut || speed != null || isEffect){
                Log.d(LOG_TAG,"Create New File !")
                Log.d(LOG_TAG,"isFadeIn : $isFadeIn")
                Log.d(LOG_TAG,"isFadeOut : $isFadeOut")
                Log.d(LOG_TAG,"speed : $speed")
                Log.d(LOG_TAG,"isEffect : $isEffect")

                if (player?.mediaPlayer != null){
                    player?.playOrPause()
                }else{
                    loading?.show()
                    audioEffect?.cut(args.uri,
                        startTime,
                        endTime,
                        trimmedTime,
                        isFadeIn,
                        isFadeOut,
                        isEffect,
                        speed, object : AudioEffectCallback{
                            override fun success(path: String) {
                                loading?.hide()
                                player?.release()
                                player?.setData(path)
                                player?.playOrPause()
                            }
                        })
                }
            }else{
                if (player?.mediaPlayer != null){
                    player?.playOrPause()
                }else{
                    player?.setData(args.uri)
                    player?.seek(startTime.toLong() * 1000)
                }
            }
        }

        binding.btnFadeIn.setOnClickListener {
            Log.d(LOG_TAG,"Fade In !")
            player?.release()
            if (!isFadeIn){
                isFadeIn = true
                binding.imgFadein.setImageResource(R.drawable.ic_maker_fadein_selected)
            }else{
                isFadeIn = false
                binding.imgFadein.setImageResource(R.drawable.ic_maker_fadein)
            }
        }

        binding.btnFadeOut.setOnClickListener {
            Log.d(LOG_TAG,"Fade Out !")
            player?.release()
            if (!isFadeOut){
                isFadeOut = true
                binding.imgFadeout.setImageResource(R.drawable.ic_maker_fadeout_selected)
            }else{
                isFadeOut = false
                binding.imgFadeout.setImageResource(R.drawable.ic_maker_fadeout)
            }
        }

        binding.btnEffect.setOnClickListener {
            Log.d(LOG_TAG,"Effect !")
            player?.release()
            if (!isEffect){
                isEffect = true
                binding.imgEffect.setImageResource(R.drawable.ic_maker_effect_selected)
            }else{
                isEffect = false
                binding.imgEffect.setImageResource(R.drawable.ic_maker_effect)
            }
        }

        binding.btnLoop.setOnClickListener {
            Log.d(LOG_TAG,"Loop !")
            if (!isLoop){
                isLoop = true
                binding.imgLoop.setImageResource(R.drawable.ic_maker_loop_selected)
            }else{
                isLoop = false
                binding.imgLoop.setImageResource(R.drawable.ic_maker_loop)
            }
        }

        binding.btnInfo.setOnClickListener {
            Log.d(LOG_TAG,"Info !")
            InfoFragment().show(requireActivity().supportFragmentManager, "InfoFragment")
        }
    }

    override fun bind() {
        super.bind()
        bindToWaveFormView()
    }

    private fun setUpAudioEffect(){
        audioEffect = context?.let { AudioEffect(it) }
    }

    private fun setUpWaveFormView(){
        binding.waveform.onProgressChanged = { progress, byUser ->
            if (progress == 100F && !byUser) {
                binding.waveform.waveColor = context?.let { ContextCompat.getColor(it, R.color.waveColor) }!!
            }
        }
        duration = args.ringtone.duration / 1000
        val metrics = requireContext().resources.displayMetrics
        val deviceWidth = metrics.widthPixels
        val waveFormWidth = ((duration * deviceWidth) / 40)

        Log.d(LOG_TAG,"Audio Duration : $duration")
        Log.d(LOG_TAG,"Device Width : $deviceWidth")
        Log.d(LOG_TAG,"Wave Form Width : $waveFormWidth")

        val params: ViewGroup.LayoutParams = binding.waveform.layoutParams
        params.width = waveFormWidth
        binding.waveform.requestLayout()
        binding.rlRoot.post {
            val heightChum = binding.rlRoot.height / resources.displayMetrics.density
            binding.waveform.chunkHeight = (heightChum * 0.8).toInt()
        }
    }

    private fun bindToWaveFormView(){
        loading?.show()
        Handler(Looper.getMainLooper()).postDelayed({
            doAsync {
                Log.d(LOG_TAG,"Path : ${args.uri}")
                val file = File(args.uri).readBytes()
                binding.waveform.setRawData(file) {
                    progressThunder.start()
                }
                uiThread {
                    loading?.hide()

                    setUpDefaultDurationToTrimAudiFile()
                    setUpAudioEffect()
                    setUpPlayer()
                }
            }
        }, 3000)
    }

    private fun setUpDefaultDurationToTrimAudiFile(){
        stateOfMaximumDurationAllowToTrimAudioFile = MaximumDurationAllowToTrimAudioFile.DURATION_20_S
        binding.seekBarMaxLength.setProgress(33.333336F)
        maximumDurationAllowToTrimAudioFile = 20F
        setUpMakerView()
    }

    private fun convertStateOfMaximumDurationToSeconds(state:MaximumDurationAllowToTrimAudioFile){
        when (state){
            MaximumDurationAllowToTrimAudioFile.DURATION_7_S -> {
                maximumDurationAllowToTrimAudioFile = 7F
            }
            MaximumDurationAllowToTrimAudioFile.DURATION_10_S -> {
                maximumDurationAllowToTrimAudioFile = 10F
            }
            MaximumDurationAllowToTrimAudioFile.DURATION_20_S -> {
                maximumDurationAllowToTrimAudioFile = 20F
            }
            MaximumDurationAllowToTrimAudioFile.DURATION_30_S -> {
                maximumDurationAllowToTrimAudioFile = 30F
            }
            MaximumDurationAllowToTrimAudioFile.DURATION_40_S -> {
                maximumDurationAllowToTrimAudioFile = 40F
            }
        }
        setUpMakerView()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setUpMakerView(){
        val metrics = this.resources.displayMetrics
        rootWidth = metrics.widthPixels

        maxDistance = (maximumDurationAllowToTrimAudioFile * rootWidth) / maximumDurationCanBeTrimAudioFile - pulBarWidth/2
        minDistance = (minimumDurationAllowToTrimAudioFile * rootWidth) / maximumDurationCanBeTrimAudioFile

        // Set default position of Maker View

        binding.leftDragview.x = 0F
        val paramLeftDragView: ViewGroup.LayoutParams = binding.leftDragview.layoutParams
        paramLeftDragView.width = 25
        binding.leftDragview.requestLayout()

        binding.leftPullBar.x = 0F
        binding.indicator.x = (binding.leftPullBar.width/2).toFloat()

        binding.rightPullBar.x = maxDistance - pulBarWidth/2

        val whiteColumnInSideRightDragViewWidth = 4
        val rightDragviewMarginLeftAtXml = 24
        binding.rightDragview.x =  binding.rightPullBar.x + binding.rightPullBar.width / 2 - 2

        val paramsRightDragView: ViewGroup.LayoutParams = binding.rightDragview.layoutParams
        paramsRightDragView.width = (rootWidth - (maxDistance - whiteColumnInSideRightDragViewWidth - rightDragviewMarginLeftAtXml)).toInt()
        binding.rightDragview.requestLayout()

        Log.d(LOG_TAG,"Left Draggier X : ${binding.leftDragview.x}")
        Log.d(LOG_TAG,"Right Draggier X : ${binding.rightDragview.x}")

        setUpTimer()

        /* Set On Touch Listener */

        binding.leftPullBar.setOnTouchListener(MovVieGesTouchListener(binding.rlRoot, binding.leftPullBar, object :
            ViewParamsChanged {
            @SuppressLint("ClickableViewAccessibility")
            override fun onViewParamsChanged(
                leftMargin: Float,
                topMargin: Float,
                width: Int,
                height: Int,
                onSwipe: ONSWIPE
            ) {
                val distanceCurrent = binding.rightDragview.x - leftMargin + pulBarWidth / 2

                if (distanceCurrent >= minDistance){

                    val paramsLeftDragView: ViewGroup.LayoutParams = binding.leftDragview.layoutParams
                    paramsLeftDragView.width = leftMargin.toInt() + pulBarWidth / 2
                    binding.leftDragview.requestLayout()

                    if (onSwipe == ONSWIPE.RIGHT){
                        Log.d(LOG_TAG,"More than ${minimumDurationAllowToTrimAudioFile}s and -----> onSwipeRight")
                    }else if (onSwipe == ONSWIPE.LEFT){
                        if (distanceCurrent >= maxDistance + 25.0){
                            Log.d(LOG_TAG,"More than ${maximumDurationAllowToTrimAudioFile}s and -----> onSwipeLeft")

                            binding.rightPullBar.x = (binding.leftDragview.width + maxDistance - pulBarWidth)
                            binding.rightDragview.x = binding.rightPullBar.x + pulBarWidth/2
                            val paramRightDragView: ViewGroup.LayoutParams = binding.rightDragview.layoutParams
                            paramRightDragView.width = (rootWidth - binding.rightDragview.x).toInt()
                            binding.rightDragview.requestLayout()
                        }
                    }
                }else{
                    Log.d(LOG_TAG,"Equal ${minimumDurationAllowToTrimAudioFile}s")
                    Log.e(LOG_TAG,"Right PullBar Start Moving !")

                    if ((leftMargin + minDistance) <= binding.rlRoot.width){
                        //check if the view not out of screen
                        Log.e(LOG_TAG,"Still not out Of Screen !!")

                        binding.rightPullBar.x = leftMargin + minDistance - pulBarWidth
                        binding.rightDragview.x = binding.rightPullBar.x + pulBarWidth/2

                        val paramsLeftDragView: ViewGroup.LayoutParams = binding.leftDragview.layoutParams
                        paramsLeftDragView.width = binding.leftPullBar.x.toInt() + pulBarWidth/2
                        binding.leftDragview.requestLayout()

                    }else{
                        // check if the view out of screen
                        Log.e(LOG_TAG,"Stop Moving !!")

                        binding.leftPullBar.x = binding.rlRoot.width - minDistance - pulBarWidth/2 + 25
                        val paramsLeftDragView: ViewGroup.LayoutParams = binding.leftDragview.layoutParams
                        paramsLeftDragView.width = binding.leftPullBar.x.toInt() + pulBarWidth/2
                        binding.leftDragview.requestLayout()
                    }
                }

                binding.indicator.x = binding.leftPullBar.x + pulBarWidth/2

                setUpTimer()
            }
        },
        object : TouchDetect{
            override fun isTouch() {
                player?.release()
            }
        }))

        binding.rightPullBar.setOnTouchListener(MovVieGesTouchListener(binding.rlRoot, binding.rightPullBar, object : ViewParamsChanged{
            override fun onViewParamsChanged(
                leftMargin: Float,
                topMargin: Float,
                width: Int,
                height: Int,
                onSwipe: ONSWIPE
            ) {
                binding.rightDragview.x = binding.rightPullBar.x + pulBarWidth / 2
                val paramRightDragView: ViewGroup.LayoutParams = binding.rightDragview.layoutParams
                paramRightDragView.width = rootWidth - leftMargin.toInt()
                binding.rightDragview.requestLayout()

                val distanceCurrent = rootWidth - ((binding.leftDragview.width) + (rootWidth - (binding.rightDragview.x) - pulBarWidth/2))

                Log.d(LOG_TAG,"Root Width X : $rootWidth")
                Log.d(LOG_TAG,"Left DragView Width : ${binding.leftDragview.width}")
                Log.d(LOG_TAG,"Right DragView X : ${binding.rightDragview.x}")
                Log.d(LOG_TAG,"Distance Current : $distanceCurrent")
                Log.d(LOG_TAG,"Max distance : $maxDistance")
                Log.d(LOG_TAG,"=================================")

                if (onSwipe == ONSWIPE.RIGHT){
                    Log.d(LOG_TAG,"Di chuyển sang phải")
                    if (distanceCurrent >= maxDistance){

                        Log.d(LOG_TAG,"More than ${maximumDurationAllowToTrimAudioFile}s and -----> onSwipeRight")

                        binding.leftPullBar.x = binding.rightDragview.x - maxDistance
                        binding.indicator.x = binding.leftPullBar.x + pulBarWidth / 2

                        val paramsLeftDragView: ViewGroup.LayoutParams = binding.leftDragview.layoutParams
                        paramsLeftDragView.width = binding.leftPullBar.x.toInt() + pulBarWidth/2
                        binding.leftDragview.requestLayout()
                    }
                }else if (onSwipe == ONSWIPE.LEFT){

                    if (distanceCurrent > minDistance){
                        Log.d(LOG_TAG,"More than ${minimumDurationAllowToTrimAudioFile}s and -----> onSwipeLeft")

                    }else if (distanceCurrent <= minDistance - 25){
                        Log.d(LOG_TAG,"Equal ${minimumDurationAllowToTrimAudioFile}s")
                        Log.e(LOG_TAG,"Left PullBar Start Moving !")

                        if (binding.leftPullBar.x <= 0){
                            // check if the view out of screen

                            Log.e(LOG_TAG,"Stop Moving !!")
                            binding.leftPullBar.x = 0F
                            binding.indicator.x = binding.leftPullBar.x + pulBarWidth/2

                            val paramsLeftDragView: ViewGroup.LayoutParams = binding.leftDragview.layoutParams
                            paramsLeftDragView.width = binding.leftPullBar.x.toInt() + pulBarWidth/2
                            binding.leftDragview.requestLayout()

                            binding.rightPullBar.x = minDistance - binding.rightPullBar.width
                            binding.rightDragview.x = binding.rightPullBar.x + pulBarWidth/2

                        }else{
                            //check if the view not out of screen

                            Log.e(LOG_TAG,"Still not out Of Screen !!")
                            binding.leftPullBar.x = binding.rightDragview.x - minDistance + pulBarWidth/2
                            binding.indicator.x = binding.leftPullBar.x + pulBarWidth/2

                            val paramsLeftDragView: ViewGroup.LayoutParams = binding.leftDragview.layoutParams
                            paramsLeftDragView.width = binding.leftPullBar.x.toInt() + pulBarWidth/2
                            binding.leftDragview.requestLayout()
                        }
                    }
                }

                setUpTimer()
            }
        },object : TouchDetect{
            override fun isTouch() {
                player?.release()
            }
        }))
    }

    private fun setUpTimer(){
//        Log.d(LOG_TAG,"Content OffSet X : ${binding.alphabetHorizonScroll.scrollX}")
        val contentOffSetCurrent  = binding.alphabetHorizonScroll.scrollX
        val scrollContentSize = (duration * rootWidth) / 40

        val leftMargin = binding.leftPullBar.x + contentOffSetCurrent
//        Log.d(LOG_TAG,"Left Margin : $leftMargin")
        val rightMargin = binding.rightDragview.x +  contentOffSetCurrent + pulBarWidth/2
//        Log.d(LOG_TAG,"Right Margin : $rightMargin")
//        Log.d(LOG_TAG,"Start Time : ${(duration * leftMargin / scrollContentSize).roundToInt()}")
//        Log.d(LOG_TAG,"End Time : ${(duration * rightMargin / scrollContentSize).roundToInt()}")
        startTime = (duration * leftMargin / scrollContentSize).roundToInt()
        endTime = (duration * rightMargin / scrollContentSize).roundToInt()

        trimmedTime = endTime - startTime
        binding.txtStartTime.text = startTime.minute()
        binding.txtEndTime.text = endTime.minute()
        binding.txtTrimmedTime.text = trimmedTime.minute()
    }

    private fun setUpRangerSeekbar(){

        binding.seekBarMaxLength.setOnRangeChangedListener(object : OnRangeChangedListener {

            override fun onRangeChanged(rangeSeekBar: RangeSeekBar, leftValue: Float, rightValue: Float, isFromUser: Boolean) {

//                Log.d(LOG_TAG,"Left Value : $leftValue")
                if (leftValue >= 0.0 && leftValue < 1.0){
                    Log.d(LOG_TAG,"Max Length : 10s")
                    stateOfMaximumDurationAllowToTrimAudioFile = MaximumDurationAllowToTrimAudioFile.DURATION_10_S
                }else if (leftValue > 33.0 && leftValue < 34.0){
                    Log.d(LOG_TAG,"Max Length : 20s")
                    stateOfMaximumDurationAllowToTrimAudioFile = MaximumDurationAllowToTrimAudioFile.DURATION_20_S
                }else if (leftValue > 66.0 && leftValue < 67.0){
                    Log.d(LOG_TAG,"Max Length : 30s")
                    stateOfMaximumDurationAllowToTrimAudioFile = MaximumDurationAllowToTrimAudioFile.DURATION_30_S
                }else if (leftValue > 99.0 && leftValue <= 100.0){
                    Log.d(LOG_TAG,"Max Length : 40s")
                    stateOfMaximumDurationAllowToTrimAudioFile = MaximumDurationAllowToTrimAudioFile.DURATION_40_S
                }
                convertStateOfMaximumDurationToSeconds(stateOfMaximumDurationAllowToTrimAudioFile)
            }

            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
                Log.d(LOG_TAG,"On Start Touch !")
                player?.release()
            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
                Log.d(LOG_TAG,"On Stop Touch !")
                player?.release()
            }
        })
    }

    //MARK: - AUDIO

    private fun setUpPlayer(){
        if (player == null){
            player = context?.let {
                RTAudioPlayer(it, object : RTPlayerCallback {

                    override fun onStartPlay() {
                        Log.d(LOG_TAG,"On Start !")
                        (context as Activity).runOnUiThread {
                            indicatorPositionXAtMusicBegin = binding.leftPullBar.x + pulBarWidth/2
                            binding.imgPlay.visibility = View.INVISIBLE
                            binding.imgPause.visibility = View.VISIBLE
                        }
                    }

                    override fun onStopPlay() {
                        Log.d(LOG_TAG,"On Stop !")

                        (context as Activity).runOnUiThread {
                            binding.indicator.visibility = View.INVISIBLE
                            binding.indicator.x = binding.leftPullBar.x + pulBarWidth/2

                            binding.imgPlay.visibility = View.VISIBLE
                            binding.imgPause.visibility = View.INVISIBLE
                        }
                    }

                    override fun onPausePlay() {
                        Log.d(LOG_TAG,"On Pause !")

                        (context as Activity).runOnUiThread {
                            binding.imgPlay.visibility = View.VISIBLE
                            binding.imgPause.visibility = View.INVISIBLE
                        }
                    }

                    override fun onSeekPlay() {
                        Log.d(LOG_TAG,"On Seek !")
                        (context as Activity).runOnUiThread {
                            binding.indicator.visibility = View.VISIBLE
                            binding.indicator.x = binding.leftPullBar.x + pulBarWidth/2
                            binding.imgPlay.visibility = View.VISIBLE
                            binding.imgPause.visibility = View.INVISIBLE
                        }
                        player?.seek(startTime.toLong() * 1000)
                    }

                    override fun onPlayProgress(mills: Long) {

                        val timeRunInPeriodTimeCutting: Int

                        if (isFadeIn || isFadeOut || speed != null || isEffect){
                            val nowTime =  (mills / 1000)

                            timeRunInPeriodTimeCutting = nowTime.toInt()

//                            Log.d(LOG_TAG,"Now Time Effect: $nowTime")
//                            Log.d(LOG_TAG,"Run with speed : $speed")
//                            Log.d(LOG_TAG,"End Time Effect: $trimmedTime")

                            val timeWithSpeed: Int = (trimmedTime / speed!!).toInt()

                            Log.d(LOG_TAG,"Run time in period : $trimmedTime --> ${(timeRunInPeriodTimeCutting)}")
                            Log.d(LOG_TAG,"Time speed : $timeWithSpeed")

                            if (timeRunInPeriodTimeCutting >= timeWithSpeed - 0.5) {
                                if (player != null && player?.isPlaying == true) {
                                    if (!isLoop){
                                        Log.d(LOG_TAG,"Do nothing !")
                                    }else{
                                        Log.d(LOG_TAG,"Run Loop Effect !")
                                        player?.playOrPause()
                                        player?.seek(0)
                                    }
                                }
                            }
                        }else{
                            val nowTime = (mills / 1000)
                            timeRunInPeriodTimeCutting = (nowTime - startTime).toInt()
                            if (nowTime >= endTime) {
                                if (player != null && player?.isPlaying == true) {
                                    if (!isLoop){
                                        player?.release()
                                    }else{
                                        Log.d(LOG_TAG,"Run Loop !")
                                        player?.playOrPause()
                                        player?.seek(startTime.toLong() * 1000)
                                    }
                                }
                            }
                        }

                        val theDistanceOfTheCuttingrArea = binding.rightDragview.x - binding.leftPullBar.x - pulBarWidth/2
                        val distancePerSecondThatIndicatorWillRun = (theDistanceOfTheCuttingrArea / trimmedTime.toFloat())
                        val positionXOfIndicator = indicatorPositionXAtMusicBegin + (distancePerSecondThatIndicatorWillRun * timeRunInPeriodTimeCutting.toFloat())

//                        Log.d(LOG_TAG,"Run time in period : $trimmedTime --> ${(timeRunInPeriodTimeCutting)}")
//                        Log.d(LOG_TAG,"The distance of the cutting area : $theDistanceOfTheCuttingrArea")
//                        Log.d(LOG_TAG,"Distance per second that indicator will run : $distancePerSecondThatIndicatorWillRun")
//                        Log.d(LOG_TAG,"Indicator position at music begin : $indicatorPositionXAtMusicBegin")
//                        Log.d(LOG_TAG,"Position x of indicator : $positionXOfIndicator")
//                        Log.d(LOG_TAG,"Right drag view x : ${binding.rightDragview.x}")
//                        Log.d(LOG_TAG,"=============================================\n\n")

                        (context as Activity).runOnUiThread {
                            if (player?.isPlaying == true){
                                binding.indicator.visibility = View.VISIBLE
                            }else{
                                binding.indicator.visibility = View.INVISIBLE
                            }

//                            if (speed != null){
////                                binding.indicator.x = positionXOfIndicator * speed!!
//                            }else{
//
//                            }

                            binding.indicator.x = positionXOfIndicator
                        }
                    }

                    override fun onError(throwable: AudioException?) {
                        Log.d(LOG_TAG,"Do Nothing !")
                    }
                })
            }
            player?.setData(args.uri)
        }
    }

    override fun onSavedWithName(name: String) {
        Log.d(LOG_TAG,"Name : $name")
        if (name != ""){
            loading?.show()
            audioEffect?.saveAudioEffectToMyTone(args.uri,
                startTime,
                endTime,
                trimmedTime,
                isFadeIn,
                isFadeOut,
                isEffect,
                speed,
                nameInput = name,
                object : AudioSaveEffectCallback{
                    override fun success() {
                        loading?.hide()
                        Log.d(LOG_TAG,"Save File Successful !")
                        SetRingtoneBottomSheet().show(requireActivity().supportFragmentManager, "SetRingtoneFragment")
                    }
                })
        }else{
            Toast.makeText(context, "Please Input New Name !", Toast.LENGTH_LONG).show()
        }
    }
}

