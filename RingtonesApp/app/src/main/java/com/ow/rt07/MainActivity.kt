package com.ow.rt07

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.ow.rt07.databinding.ActivityMainBinding
import com.ow.rt07.databinding.ActivitySplashBinding
import com.ow.rt07.utils.UIApplicationUtils
import com.wallpaper.WallpaperApplyTask

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var fragmentHost:NavHostFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        fragmentHost = supportFragmentManager.findFragmentById(R.id.nav_host_container) as NavHostFragment
        val navController = fragmentHost.navController
        binding.bottomNav.setupWithNavController(navController)



    }

    override fun onSupportNavigateUp(): Boolean {
        return fragmentHost.navController.navigateUp()
    }

    fun showBottomNavigation(){
        binding.bottomNav.visibility = View.VISIBLE
    }
    fun hideBottomNavigation(){
        binding.bottomNav.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
//        UIApplicationUtils.transparentStatusBar(this, true)
        Log.d("Main", "On Resume !")
    }
}