package com.ow.rt07.ui.main.ringtones

import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ow.rt07.model.Category
import com.ow.rt07.model.ringtone.CategoryRingtone
import com.ow.rt07.model.ringtone.DataRingtone
import com.ow.rt07.model.ringtone.PopularRingtone
import com.ow.rt07.model.wallpaper.Data
import com.ow.rt07.model.wallpaper.DataX
import com.ow.rt07.model.wallpaper.MenuWallpaper
import com.ow.rt07.network.BaseResponse
import com.ow.rt07.network.RTApi
import com.ow.rt07.network.RTRepository
import com.ow.rt07.utils.Coroutines
import com.ow.rt07.utils.base.BaseViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RingtonesViewModel : BaseViewModel() {

    var categoryRingtones: MutableLiveData<DataRingtone?> = MutableLiveData()
    var populars: MutableLiveData<PopularRingtone?> = MutableLiveData()
    var suggestKeyword: MutableLiveData<Data?>
    var ringtoneBySearch: MutableLiveData<PopularRingtone?> = MutableLiveData()


    init {
        suggestKeyword = MutableLiveData()
    }


    fun getLiveDataSearchRingtone(keyword: String): MutableLiveData<PopularRingtone?> {
        ringtoneBySearch = MutableLiveData()
        searchRingtone(keyword)
        return ringtoneBySearch as MutableLiveData<PopularRingtone?>
    }

    fun getDataAllCategory() {
        val rtApi: RTApi = RTApi.invoke()
        rtApi.getAllCategory().enqueue(object : Callback<DataRingtone> {
            override fun onResponse(call: Call<DataRingtone>, response: Response<DataRingtone>) {
                categoryRingtones!!.postValue(response.body())
            }

            override fun onFailure(call: Call<DataRingtone>, t: Throwable) {
                categoryRingtones!!.postValue(null)
            }
        })
    }

    fun getDataPopular() {
        val rtApi: RTApi = RTApi.invoke()
        rtApi.getPopular().enqueue(object : Callback<PopularRingtone> {
            override fun onResponse(
                call: Call<PopularRingtone>,
                response: Response<PopularRingtone>
            ) {
                if (populars?.value == null) {
                    populars?.postValue(response.body())
                }
            }

            override fun onFailure(call: Call<PopularRingtone>, t: Throwable) {
                populars?.postValue(null)
            }
        })
    }

    fun getSuggetKeyword() {
        var rtApi: RTApi = RTApi.invoke()
        rtApi.getKeywordSuggest("ringtone").enqueue(object : Callback<MenuWallpaper> {
            override fun onResponse(call: Call<MenuWallpaper>, response: Response<MenuWallpaper>) {
                suggestKeyword.postValue(response.body()!!.data)
            }

            override fun onFailure(call: Call<MenuWallpaper>, t: Throwable) {
                suggestKeyword.postValue(null)
            }
        })
    }

    fun searchRingtone(keyword: String) {
        var rtApi: RTApi = RTApi.invoke()
        rtApi.searchRingtone(keyword).enqueue(object : Callback<PopularRingtone> {
            override fun onResponse(
                call: Call<PopularRingtone>,
                response: Response<PopularRingtone>
            ) {
                ringtoneBySearch?.postValue(response.body())
            }

            override fun onFailure(call: Call<PopularRingtone>, t: Throwable) {
                ringtoneBySearch?.postValue(null)
            }
        })
    }

    fun loadMoreRingtone(url: String){
        val rtApi: RTApi = RTApi.invoke()
        rtApi.loadMoreRingtone(url).enqueue(object : Callback<PopularRingtone> {
            override fun onResponse(call: Call<PopularRingtone>, response: Response<PopularRingtone>) {
                response.body()?.let {
                    populars.postValue(it)

                }
            }
            override fun onFailure(call: Call<PopularRingtone>, t: Throwable) {
                Log.e("error", ""+ t)
                populars.postValue(null)
            }
        })
    }

}