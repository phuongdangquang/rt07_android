package com.ow.rt07.utils.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.devhoony.lottieproegressdialog.LottieProgressDialog
import com.ow.rt07.ui.main.subscription.SubActivity

//import cn.pedant.SweetAlert.SweetAlertDialog
//import com.trung.fitnessapp.FitnessApplication
//import com.trung.fitnessapp.R
//import com.trung.fitnessapp.manager.AppPreferences
//import com.trung.fitnessapp.manager.FTAnalytics
//import com.trung.fitnessapp.ui.iap.IAPActivity

abstract class BaseFragment : Fragment() {

//    private var loading : SweetAlertDialog? = null

     var loading: LottieProgressDialog? = null
     var success:LottieProgressDialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.i("BaseFragment","onViewCreated name class = ${this.javaClass.name}")

//        FTAnalytics.screenEvent(javaClass.name)

        if (loading == null && success == null){
            loading = context?.let {
                LottieProgressDialog(
                    context = it,
                    isCancel = false,
                    dialogWidth = 160,
                    dialogHeight = 160,
                    animationViewWidth = null,
                    animationViewHeight = null,
                    fileName = "loading.json",
                    title = "   Progressing ...",
                    titleVisible = View.VISIBLE
                )
            }

            success = context?.let {
                LottieProgressDialog(
                    context = it,
                    isCancel = false,
                    dialogWidth = 120,
                    dialogHeight = 120,
                    animationViewWidth = null,
                    animationViewHeight = null,
                    fileName = "success.json",
                    title = "",
                    titleVisible = View.INVISIBLE
                )
            }
        }

        setUpView()
        bind()
    }
    //
    open fun setUpView(){
        requireActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }

    open fun bind(){

    }

    fun showIap(){
        val intent = Intent(requireContext(), SubActivity()::class.java)
        startActivity(intent)
    }

    //
    fun showToast(message:String){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show()
    }
    fun showAlert(message: String){
        val builder = AlertDialog.Builder(requireActivity())
        with(builder)
        {
            setTitle("")
            setMessage(message)
            setPositiveButton("OK") { _, _ ->
                //nothing
            }
            show()
        }
    }

    fun openSoftKeyboard(context: Context, view: View) {
        view.requestFocus()
        // open the soft keyboard
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }
    fun hideKeyboard(){
        val inputMethodManager = ContextCompat.getSystemService(requireContext(), InputMethodManager::class.java)
        inputMethodManager?.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    fun showLoading(){
//        loading?.dismissWithAnimation()
//        context?.let {
//            loading = SweetAlertDialog(it,SweetAlertDialog.PROGRESS_TYPE)
//            loading?.setCancelable(false)
//            loading?.titleText = "Loading..."
//            loading?.progressHelper?.barColor = Color.parseColor("#0040DD")
//            loading?.show()
//        }

    }

    fun hideLoading(){
//        loading?.dismissWithAnimation()
    }
}