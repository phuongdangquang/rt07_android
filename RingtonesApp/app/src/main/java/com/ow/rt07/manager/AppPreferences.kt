package com.ow.rt07.manager

import android.content.Context
import android.content.SharedPreferences


object AppPreferences {
    private const val NAME = "RTPreferences"
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences

    private val IS_PURCHASED = Pair("isPurchased",false)


    fun init(context: Context) {
        preferences = context.getSharedPreferences(NAME, MODE)
    }

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }

    var isPurchased: Boolean

    get() = preferences.getBoolean(IS_PURCHASED.first, IS_PURCHASED.second)
    set(value) = preferences.edit {
        it.putBoolean(IS_PURCHASED.first, value)
    }
}