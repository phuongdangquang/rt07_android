package com.ow.rt07.model.wallpaper

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MenuWallpaper(
    val code: Int,
    val `data`: Data,
    val message: String
): Parcelable

@Parcelize
data class CateWallpaper(
    val code: Int = 0,
    val `data`: ArrayList<ItemX> = arrayListOf(),
    val message: String = "",
    val load_more_item: String = ""
): Parcelable

@Parcelize
data class SearchCateWallpaper(
    val code: Int,
    val `data`: DataX,
    val message: String,

): Parcelable

@Parcelize
data class DataX(
    val wallpapers: ArrayList<ItemX>,
    ): Parcelable


data class BaseData(
    val code: Int,
    val `data`: ArrayList<Data> ,
    val message: String
)
