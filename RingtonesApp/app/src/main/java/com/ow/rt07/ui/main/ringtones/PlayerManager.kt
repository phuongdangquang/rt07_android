package com.ow.rt07.ui.main.ringtones

import android.content.Context
import android.media.MediaPlayer
import android.media.MediaPlayer.OnPreparedListener
import android.net.Uri
import android.util.Log
import com.ow.rt07.utils.audioplayer.RTPlayerCallback
import com.ow.rt07.utils.audioplayer.exception.AudioException
import com.ow.rt07.utils.audioplayer.exception.PlayerDataSourceException
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.IOException
import java.net.URL
import java.util.*

interface PlayerManagerCallback {
    fun onPlay()
    fun onComplete()
    fun onPausePlay()
    fun onSeekPlay()
    fun onPrepared()
    fun onPreparing()
    fun onPlayProgress(mills: Long)
    fun onError(throwable: AudioException?)
}

const val VISUALIZATION_INTERVAL = 1000 / 25L

class PlayerManager  {
    var mediaPlayer: MediaPlayer? = null
    private var timerProgress: Timer? = null
    private var isPrepared = false
    private var context: Context? = null
    var isPause = false
    private var pauseTime: Long = 0
    var pausePos: Long = 0
    private var dataSource: String? = null

    var listener: PlayerManagerCallback? = null

    constructor(context: Context, lis: PlayerManagerCallback) {
        listener = lis
        this.context = context
    }

    fun setData(path: String) {

            dataSource = path
            setPreparePlayer()

    }

    private fun setPreparePlayer() {
        doAsync {
            if (dataSource != null) {
                mediaPlayer = MediaPlayer()
                mediaPlayer?.setDataSource(dataSource)
                listener?.onPreparing()
                mediaPlayer?.prepare()
            }
            uiThread {
                listener?.onPrepared()
            }
        }
    }



    fun playOrPause() {

        if (mediaPlayer != null) {
            if (mediaPlayer?.isPlaying == true) {
                pause()
            } else {
//                isPause = false
//                mediaPlayer?.setOnPreparedListener(this)
                mediaPlayer?.start()
                listener?.onPlay()
                mediaPlayer?.setOnCompletionListener {
                    stop()
                    listener?.onComplete()
                }
                setProgress()
            }
        } else {

            Log.e("TAG", "Player is null")
        }

    }


    fun seek(mills: Long) {
        pauseTime = mills
        if (mediaPlayer?.isPlaying == true) {
            pause()
        }
//        if (mediaPlayer != null) {
            Log.d("TAG", "$pausePos")
            isPause = false
            mediaPlayer?.seekTo(mills.toInt())
            mediaPlayer?.start()
            listener?.onPlay()
//            mediaPlayer?.setOnCompletionListener {
//                stop()
//                listener?.onComplete()
//            }
            setProgress()
//        }
    }

    fun setProgress() {
        timerProgress = Timer()
        timerProgress?.schedule(object : TimerTask() {
            override fun run() {
                    if (mediaPlayer != null && mediaPlayer?.isPlaying == true) {
                        val curPos = mediaPlayer?.currentPosition ?: 0
                        listener?.onPlayProgress(curPos.toLong())
                    }

            }
        }, 0, 500)
    }

    fun pause() {
        if (timerProgress != null) {
            timerProgress?.cancel()
            timerProgress?.purge()
        }
        if (mediaPlayer != null) {
            if (mediaPlayer?.isPlaying == true) {
                mediaPlayer?.pause()
                listener?.onPausePlay()
                pauseTime = (mediaPlayer?.currentPosition ?: 0).toLong()
                isPause = true
                pausePos = pauseTime
            }
        }
    }

    fun stop() {
        if (timerProgress != null) {

            timerProgress?.cancel()
            timerProgress?.purge()

        }
        if (mediaPlayer != null) {
            mediaPlayer?.seekTo(0)
//            mediaPlayer?.stop()
//            mediaPlayer?.setOnCompletionListener(null)
//            isPrepared = false
//            listener?.onStopPlay()
//            mediaPlayer?.currentPosition
//            pauseTime = 0
        }
//        isPause = false
//        pausePos = 0
//        curPos = 0
    }

    val isPlaying: Boolean
        get() {
            try {
                return mediaPlayer != null && mediaPlayer?.isPlaying == true
            } catch (e: IllegalStateException) {
                Log.e("TAG", "Player is not initialized!")
            }
            return false
        }

    fun release() {
        stop()
        if (mediaPlayer != null) {
            mediaPlayer?.release()
            mediaPlayer = null
        }
        isPrepared = false
        isPause = false
        dataSource = null
    }
}