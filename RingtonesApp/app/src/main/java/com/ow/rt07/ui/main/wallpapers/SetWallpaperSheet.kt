package com.ow.rt07.ui.main.wallpapers

import android.R
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.app.WallpaperManager
import android.content.ContentUris
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.AssetFileDescriptor
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.provider.ContactsContract
import android.provider.ContactsContract.RawContacts
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.andrefrsousa.superbottomsheet.SuperBottomSheetFragment
import com.devhoony.lottieproegressdialog.LottieProgressDialog
import com.ow.rt07.config.SetWallpaperHelper
import com.ow.rt07.databinding.TutorialSetWallpaperFragmentBinding
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.OutputStream


class SetWallpaperSheet(val bitmap: Bitmap, val url:String) : SuperBottomSheetFragment(),
    AdapterRecyclerViewInterface {

    private lateinit var binding: TutorialSetWallpaperFragmentBinding
    private val CONTACT_PERMISSION_CODE = 1
    private val CONTACT_PICK_CODE = 2
    var loading: LottieProgressDialog? = null
    var success: LottieProgressDialog? = null
    var wallpaperManager: WallpaperManager? = null
    var urlString: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        wallpaperManager = WallpaperManager.getInstance(context)
        wallpaperManager?.setWallpaperOffsetSteps(1f, 1f)
        this.urlString = url

        Log.e("URL", ""+ url)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        if (loading == null) {
            loading = context?.let {
                LottieProgressDialog(
                    context = it,
                    isCancel = false,
                    dialogWidth = 160,
                    dialogHeight = 160,
                    animationViewWidth = null,
                    animationViewHeight = null,
                    fileName = "loading.json",
                    title = "Processing",
                    titleVisible = View.VISIBLE
                )
            }

            success = context?.let {
                LottieProgressDialog(
                    context = it,
                    isCancel = false,
                    dialogWidth = 120,
                    dialogHeight = 120,
                    animationViewWidth = null,
                    animationViewHeight = null,
                    fileName = "success.json",
                    title = "success",
                    titleVisible = View.VISIBLE
                )
            }
        }

        binding = TutorialSetWallpaperFragmentBinding.inflate(inflater, container, false)
        binding.recyclerViewTutorialSetWallpaper.layoutManager =
            LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerViewTutorialSetWallpaper.adapter = AdapterTutorialSetWallpaper(this)
//            val bitmap = BitmapFactory.decodeResource(R.drawable.img_item_category_default)
        return binding.root
    }

    override fun onItemClicked(index: Int) {
//        val wallpaperManager =
//            WallpaperManager.getInstance(this.requireContext().applicationContext)

        loading?.show()
        try {
            if (index == 0) {
                setWallpaper(0)

            } else if (index == 1) {
                setWallpaper(1)

            } else if (index == 2) {
                setWallpaper(2)

            } else if (index == 3) {
                setAsContact()
//                this.dismiss()
            }
        } catch (e: IOException) {
        }
    }

    private fun setWallpaper(type: Int) {

        doAsync {
            val wallpaperHelper = SetWallpaperHelper(context)
            wallpaperHelper.setWallpaper(bitmap,type)
            uiThread {
                loading?.hide()
                success?.show()
                Handler().postDelayed({
                    success?.hide()
                }, 2000)
                dismiss()
            }
        }
    }

    // SET CONTACT
    fun setDisplayPhotoByRawContactId(rawContactId: Long, bmp: Bitmap): Boolean {
        val stream = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val byteArray: ByteArray = stream.toByteArray()
        val pictureUri = Uri.withAppendedPath(
            ContentUris.withAppendedId(
                RawContacts.CONTENT_URI,
                rawContactId
            ), RawContacts.DisplayPhoto.CONTENT_DIRECTORY
        )
        try {
            val afd: AssetFileDescriptor =
                requireContext().contentResolver.openAssetFileDescriptor(pictureUri, "rw")!!
            val os: OutputStream = afd.createOutputStream()
            os.write(byteArray)
            os.close()
            afd.close()
            return true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return false
    }

    fun setAsContact() {
        if (checkContactPermission()) {
            //allowed
            pick()
        } else {
            //not allowed, request
            requestContactPermission()
        }
    }

    private fun checkContactPermission(): Boolean {
        //check if permission was granted/allowed or not, returns true if granted/allowed, false if not
        return this.context?.let {
            ContextCompat.checkSelfPermission(
                it,
                android.Manifest.permission.READ_CONTACTS
            )
        } == PackageManager.PERMISSION_GRANTED
    }

    private fun requestContactPermission() {
        val permission = arrayOf(
            android.Manifest.permission.READ_CONTACTS,
            android.Manifest.permission.WRITE_CONTACTS
        )
        ActivityCompat.requestPermissions(
            this.requireActivity(),
            permission,
            CONTACT_PERMISSION_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        //handle permission request results || calls when user from Permission request dialog presses Allow or Deny
        if (requestCode == CONTACT_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission granted, can pick contact
                pick()
            } else {
                //permission denied, cann't pick contact, just show message
                Toast.makeText(this.context, "Permission denied...", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun pick() {
        var i = Intent(Intent.ACTION_PICK)
        i.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
        startActivityForResult(i, 111)
    }

    @SuppressLint("Range")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 111 && resultCode == RESULT_OK) {
            Log.e("result", "--------------------")

            val contactData: Uri? = data?.data
            var cursor: Cursor? =
                this.requireContext().contentResolver.query(contactData!!, null, null, null, null)
            if (cursor?.moveToFirst()!!) {
                var contactID =
                    cursor?.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID))
                Log.e("contactId", "$contactID")
//                cursor?.close()
//                val bit =
//                    BitmapFactory.decodeResource(resources, R.drawable.ic_menu_gallery)
                setDisplayPhotoByRawContactId(contactID.toLong(), bitmap)
                success?.show()
                Handler().postDelayed({
                    success?.hide()
                }, 2000)
                this.dismiss()


            }
//            val cursor1: Cursor? =
//                this.requireContext().contentResolver.query(
//                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                    null,
//                    null,
//                    null,
//                    null
//                )!!
////            cursor1.getString()
//            if (cursor1?.moveToFirst()!!) {
//                val contactId = cursor1.getString(cursor1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID))
//                    setDisplayPhotoByRawContactId(contactId.toLong(), bitmap)
//                Log.e("contactId", "$contactId")
//
//            }
        }
    }

    override fun getPeekHeight(): Int {
        return 910
    }

    override fun getCornerRadius(): Float {
        return 20f
    }

    override fun getExpandedHeight(): Int {
        return -2
    }
}