package com.ow.rt07.ui.main.maker.recorder

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.ow.rt07.R
import com.ow.rt07.databinding.SaveRecordDialogFragmentBinding

interface DialogSaveRecordInterface {
    fun onSavedWithName(name:String)
}

class DialogSaveRecordFragment(private val listener: DialogSaveRecordInterface): DialogFragment() {

    lateinit var binding: SaveRecordDialogFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.FullScreenDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SaveRecordDialogFragmentBinding.inflate(inflater, container, false)

        binding.btnCancel.setOnClickListener {
            dismiss()
        }
        binding.btnSave.setOnClickListener {
            hideKeyboard()
            dismiss()
            listener.onSavedWithName(binding.tftName.text.toString())
        }

        return binding.root
    }

    private fun hideKeyboard(){
        val inputMethodManager = ContextCompat.getSystemService(requireContext(), InputMethodManager::class.java)
        inputMethodManager?.hideSoftInputFromWindow(view?.windowToken, 0)
    }
}