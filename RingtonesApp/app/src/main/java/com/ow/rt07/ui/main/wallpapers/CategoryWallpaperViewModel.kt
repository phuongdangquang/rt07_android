package com.ow.rt07.ui.main.wallpapers

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.model.wallpaper.CateWallpaper
import com.ow.rt07.model.wallpaper.Data
import com.ow.rt07.model.wallpaper.ItemX
import com.ow.rt07.model.wallpaper.MenuWallpaper
import com.ow.rt07.network.RTApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CategoryWallpaperViewModel: ViewModel() {
     var categoryWallpaper: MutableLiveData<CateWallpaper?>? = null
    var moreWallpaper: MutableLiveData<CateWallpaper?>? = null

    init {
        moreWallpaper = MutableLiveData()
    }

//    fun loadMoreItem(){
//
//        loadMoreWallpaper(url)
//
//
//    }

    fun getWallpaperByCategory(url: String): LiveData<CateWallpaper?> {
        if (categoryWallpaper == null) {
            categoryWallpaper = MutableLiveData()
            getDataCategoryWallpaper(url)
        }
        return categoryWallpaper as MutableLiveData<CateWallpaper?>
    }



    fun getDataCategoryWallpaper(url: String) {
        var rtApi: RTApi = RTApi.invoke()
        rtApi.getCateWallpaper(url).enqueue(object : Callback<CateWallpaper> {
            override fun onResponse(call: Call<CateWallpaper>, response: Response<CateWallpaper>) {
                if (categoryWallpaper!!.value == null){
                    categoryWallpaper!!.postValue(response.body())
                }
            }

            override fun onFailure(call: Call<CateWallpaper>, t: Throwable) {
                categoryWallpaper?.postValue(null)
            }
        })
    }


    fun loadMoreWallpaper(url: String) {
        var rtApi: RTApi = RTApi.invoke()
        rtApi.loadMoreWallpaper(url).enqueue(object : Callback<CateWallpaper> {
            override fun onResponse(call: Call<CateWallpaper>, response: Response<CateWallpaper>) {
                if (categoryWallpaper!!.value == null){
                    categoryWallpaper!!.postValue(response.body())
                }
            }

            override fun onFailure(call: Call<CateWallpaper>, t: Throwable) {
                categoryWallpaper?.postValue(null)
            }
        })
    }



}