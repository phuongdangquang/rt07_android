package com.ow.rt07.utils.audioplayer.exception

class PermissionDeniedException : AudioException() {
    override val type: Int
        get() = READ_PERMISSION_DENIED
}