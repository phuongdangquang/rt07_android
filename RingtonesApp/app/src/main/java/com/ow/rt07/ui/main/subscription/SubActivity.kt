package com.ow.rt07.ui.main.subscription

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.ow.rt07.databinding.ActivitySplashBinding
import com.ow.rt07.databinding.ActivitySubBinding
import com.ow.rt07.utils.UIApplicationUtils

class SubActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySubBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySubBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onResume() {
        super.onResume()
        UIApplicationUtils.transparentStatusBar(this, true)
        Log.d("Main", "On Resume !")
    }
}