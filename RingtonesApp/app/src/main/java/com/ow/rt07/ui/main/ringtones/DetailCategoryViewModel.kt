package com.ow.rt07.ui.main.ringtones

import androidx.lifecycle.MutableLiveData
import com.ow.rt07.model.ringtone.DataRingtone
import com.ow.rt07.model.ringtone.PopularRingtone
import com.ow.rt07.network.RTApi
import com.ow.rt07.utils.base.BaseViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailCategoryViewModel: BaseViewModel() {

     var ringtones: MutableLiveData<PopularRingtone?>? = null

    fun getRingtone(category_id: Int) : MutableLiveData<PopularRingtone?> {
        if (ringtones == null) {
            ringtones = MutableLiveData()
        }
        getDataRingtone(category_id)
        return ringtones as MutableLiveData<PopularRingtone?>
    }

    fun getDataRingtone(category_id: Int){
        val rtApi: RTApi = RTApi.invoke()
        rtApi.getRingtoneInCategory(category_id, 1).enqueue(object : Callback<PopularRingtone> {
            override fun onResponse(call: Call<PopularRingtone>, response: Response<PopularRingtone>) {
                ringtones!!.postValue(response.body())
            }

            override fun onFailure(call: Call<PopularRingtone>, t: Throwable) {
                ringtones!!.postValue(null)
            }
        })
    }
}