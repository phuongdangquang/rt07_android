package com.ow.rt07.ui.main.wallpapers

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ow.rt07.MainActivity
import com.ow.rt07.R
import com.ow.rt07.config.Util
import com.ow.rt07.databinding.WallpaperFragmentBinding
import com.ow.rt07.model.wallpaper.ItemX
import com.ow.rt07.utils.base.BaseFragment


class WallpaperFragment : BaseFragment(), AdapterSuggestKeyWord.SuggestKeywordClickInterface {

    private lateinit var binding: WallpaperFragmentBinding
    lateinit var viewModel: WallpaperViewModel
    lateinit var adapterWallpaper: AdapterWallpaper
    lateinit var adapterSuggest: AdapterSuggestKeyWord
    lateinit var adapterCategoryWallpaper: AdapterCategoryWallpaper
//    var wallpapers: Wallpapers = Wallpapers()
    var wallpaperItem: ArrayList<ItemX>  = arrayListOf()
    var keyword: String = ""
    var ratio:String = "16:9"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context?.let {
            ratio = Util.getRatio(it)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(WallpaperViewModel::class.java)
        binding = WallpaperFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun setUpView() {
        super.setUpView()
        action()
        getWallpaper()
        getKeywordSuggest()
        (activity as MainActivity).showBottomNavigation()
        binding.recyclerViewCategoryWallpaper.visibility = View.INVISIBLE
        binding.recyclerViewSuggestKeyword.visibility = View.INVISIBLE
        binding.recyclerViewWallpaper.visibility = View.VISIBLE
        binding.btnCancel.visibility = View.GONE
        //adapter wallpaper
        adapterWallpaper = AdapterWallpaper()
        binding.recyclerViewWallpaper.layoutManager =
            LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerViewWallpaper.adapter = adapterWallpaper
        //adapter suggest
        adapterSuggest = AdapterSuggestKeyWord(emptyList(), "", "wallpaper", this)
        binding.recyclerViewSuggestKeyword.layoutManager =
            LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerViewSuggestKeyword.adapter = adapterSuggest
        //adapter category wallpaper
        adapterCategoryWallpaper =  AdapterCategoryWallpaper(wallpaperItem,
            object : AdapterRecyclerViewInterface {
                override fun onItemClicked(index: Int) {
                    val intent =
                        Intent(context, DetailWallpaperActivity::class.java)
                    val bundle = Bundle()
                    bundle.putBoolean("is_show", true)
                    bundle.putInt("current_position", index)
                    bundle.putParcelableArrayList(
                        "wallpapers",
                        wallpaperItem
                    )
                    bundle.putString(
                        "load_more_url",
                        ""
                    )
                    intent.putExtras(bundle)
                    context!!.startActivity(intent)
                }
            })
        binding.recyclerViewCategoryWallpaper.layoutManager =
            GridLayoutManager(context, 2)
        binding.recyclerViewCategoryWallpaper.adapter = adapterCategoryWallpaper

    }

    private fun getWallpaper() {
        viewModel.getWallpaper(ratio).observe(viewLifecycleOwner, Observer {
            it?.let {
                for (i in it) {
//                    wallpapers = i.wallpapers
                    adapterWallpaper.setContent(i.wallpapers)
                    binding.loadingView.visibility = View.GONE
                    Log.e("getRatio", ratio)
                }
            }
        })
    }

    fun getKeywordSuggest() {
        viewModel.getSuggetKeyword().observe(viewLifecycleOwner, Observer {
            it?.let {
                adapterSuggest.setContent(it.keyword)
            }
        })
    }

    fun searchWallpaper(keyword: String) {
        viewModel.getWallpaperBySearch(keyword, ratio)
            .observe(viewLifecycleOwner, Observer {
                it?.let {
                    wallpaperItem.clear()
                    wallpaperItem.addAll(it.wallpapers)
                    adapterCategoryWallpaper.notifyDataSetChanged()
//                    adapterCategoryWallpaper.setContent(it.wallpapers)
                    binding.loadingView.visibility = View.INVISIBLE
                }
            })
        hideKeyboard()
    }

    fun action() {
        actionCancel()
        searchViewListener()
    }

    fun searchViewListener() {
//        binding.searchBar.setOnClickListener {
//            Log.d("search", "search click")
//            val animation = AnimationUtils.loadAnimation(context, R.anim.enter_from_right)
//            binding.btnCancel.startAnimation(animation)
//            binding.btnCancel.visibility = View.VISIBLE
//            binding.recyclerViewWallpaper.visibility = View.GONE
//            binding.recyclerViewSuggestKeyword.visibility = View.VISIBLE
//        }

        binding.searchBar.setOnFocusChangeListener(View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                if (!binding.btnCancel.isVisible) {
                    val animation = AnimationUtils.loadAnimation(context, R.anim.enter_from_right)
                    binding.btnCancel.startAnimation(animation)
                }
                binding.btnCancel.visibility = View.VISIBLE
                binding.recyclerViewWallpaper.visibility = View.INVISIBLE
                binding.recyclerViewSuggestKeyword.visibility = View.VISIBLE
                binding.navigationView.visibility = View.GONE
            }
        })

        binding.searchBar.apply {
            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_SEARCH -> {
                        //action submit
                        Log.e("searchbar", "text submit")
                        binding.loadingView.visibility = View.VISIBLE
                        Log.e("text submit", ""+binding.searchBar.text )
                        searchWallpaper(binding.searchBar.text.toString())
//                        binding.recyclerViewWallpaper.visibility = View.INVISIBLE
//                        binding.recyclerViewSuggestKeyword.visibility = View.INVISIBLE
//                        binding.recyclerViewCategoryWallpaper.visibility = View.VISIBLE

                        if (binding.btnCancel.isVisible) {
                            val animation =
                                AnimationUtils.loadAnimation(context, R.anim.exit_to_right)
                            binding.btnCancel.startAnimation(animation)
                        }
                    }
                }
                false
            }
            doOnTextChanged { text, _, _, _ ->
                text?.let {
                    Log.e("Search", "on Text Change")
                }
            }
        }
    }

    override fun tagClick(keyword: String) {
        binding.loadingView.visibility = View.VISIBLE
        searchWallpaper(keyword)
        binding.searchBar.setText(keyword)
        binding.recyclerViewWallpaper.visibility = View.INVISIBLE
        binding.recyclerViewSuggestKeyword.visibility = View.INVISIBLE
        binding.recyclerViewCategoryWallpaper.visibility = View.VISIBLE
    }

    fun actionCancel() {
        binding.btnCancel.setOnClickListener {
            wallpaperItem.clear()
            adapterCategoryWallpaper.notifyDataSetChanged()
            binding.searchBar.text = null
            binding.searchBar.clearFocus()
            hideKeyboard()
            binding.recyclerViewWallpaper.visibility = View.VISIBLE
            binding.recyclerViewCategoryWallpaper.visibility = View.INVISIBLE
            binding.recyclerViewSuggestKeyword.visibility = View.INVISIBLE
            binding.btnCancel.visibility = View.GONE
            binding.navigationView.visibility = View.VISIBLE
//            val animation = AnimationUtils.loadAnimation(context, R.anim.exit_to_right)
//            binding.btnCancel.startAnimation(animation)
        }
    }
}
