package com.ow.rt07.ui.main.wallpapers

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.lujun.androidtagview.TagView
import com.ow.rt07.R
import com.ow.rt07.databinding.RowCategorySearchBinding
import com.ow.rt07.databinding.RowSearchBinding
import com.ow.rt07.model.ringtone.CategoryRingtone
import com.ow.rt07.model.wallpaper.Wallpapers
import com.ow.rt07.ui.main.ringtones.AdapterItemCategoryRingtone
import com.ow.rt07.ui.main.ringtones.AllCategoryRingtoneFragmentDirections
import com.ow.rt07.ui.main.ringtones.RingtonesFragmentDirections


class AdapterSuggestKeyWord(val category: List<CategoryRingtone>, var suggestKeyword: String, val typeMode: String, val listener: SuggestKeywordClickInterface): RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    class SuggestKeyHolder(val binding: RowSearchBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)

    class CategoryHolder(val binding: RowCategorySearchBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)



    fun setContent(value: String) {
        suggestKeyword = value
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return 1
        } else if (position == 1) {
            return 2
        } else {
            return 2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == 1) {
            val binding =
                RowSearchBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return SuggestKeyHolder(binding, parent)
        } else if (viewType == 2) {
            val binding = RowCategorySearchBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            return CategoryHolder(binding, parent)
        } else {
            val binding = RowCategorySearchBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            return CategoryHolder(binding, parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is SuggestKeyHolder -> {
                val suggestList = suggestKeyword.split(",")
                holder.binding.txtKeySuggestWallpaper.text = "suggestion"
                holder.binding.tagView.tagTextColor = Color.parseColor("#99EBEBF5")
                holder.binding.tagView.tagBackgroundColor = Color.parseColor("#2E2540")
                holder.binding.tagView.tagBorderColor = Color.parseColor("#2E2540")
                holder.binding.tagView.backgroundColor = Color.TRANSPARENT
                holder.binding.tagView.borderColor = Color.TRANSPARENT
                holder.binding.tagView.borderWidth = 0F
                val typeface = ResourcesCompat.getFont(holder.parent.context, R.font.helvetica)
                holder.binding.tagView.setTagTypeface(typeface)
                holder.binding.tagView.removeAllTags()
                holder.binding.tagView.setTags(suggestList)

                holder.binding.tagView.setOnTagClickListener(object : TagView.OnTagClickListener {
                    override fun onTagClick(position: Int, text: String) {
                        listener.tagClick(text)
                    }
                    override fun onTagLongClick(position: Int, text: String?) {}
                    override fun onSelectedTagDrag(position: Int, text: String?) {}
                    override fun onTagCrossClick(position: Int) {}
                })
            }

            is CategoryHolder -> {
                if (typeMode == "ringtone"){
                    holder.binding.txtCategory.visibility = View.VISIBLE
                    holder.binding.recyclerViewCategoryRingtones.layoutManager = GridLayoutManager(holder.parent.context, 2)
                    holder.binding.recyclerViewCategoryRingtones.adapter = AdapterItemCategoryRingtone(
                        category.toTypedArray(), object : AdapterRecyclerViewInterface{
                            override fun onItemClicked(index: Int) {
                                val action =
                                    RingtonesFragmentDirections.actionNavigationRingtoneToNavigationDetailCategoryRingtone(
                                        category[index].id,
                                        category[index].name
                                    )
                                Navigation.findNavController(holder.itemView).navigate(action)
                            }
                        })
                }else{
                    holder.binding.txtCategory.visibility = View.GONE
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return 2
    }

    interface SuggestKeywordClickInterface {
        fun tagClick(keyword: String)
    }

}