package com.ow.rt07.ui.main.wallpapers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ow.rt07.R
import com.ow.rt07.databinding.RowTutorialSetWallpaperBinding
import com.ow.rt07.databinding.RowWallpaperBinding

class AdapterTutorialSetWallpaper(val listener: AdapterRecyclerViewInterface): RecyclerView.Adapter<AdapterTutorialSetWallpaper.TutorialSetWallpaperHolder>() {

    class TutorialSetWallpaperHolder(val binding: RowTutorialSetWallpaperBinding, val parent: ViewGroup) :
        RecyclerView.ViewHolder(binding.root)

    val list_title_tutorial: ArrayList<String> = arrayListOf("Set as Wallpaper & Lock screen", "Set as only Wallpaper", "Set as only Lock screen", "Set as contacts photo")
    val list_icon_tutorial: ArrayList<Int> = arrayListOf(R.drawable.ic_set_as_wallpaper_and_lockscreen,R.drawable.ic_set_as_wallpaper, R.drawable.ic_set_as_lockscreen, R.drawable.ic_set_as_contact)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TutorialSetWallpaperHolder {
        val binding =
            RowTutorialSetWallpaperBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TutorialSetWallpaperHolder(binding, parent)
    }

    override fun onBindViewHolder(holder: TutorialSetWallpaperHolder, position: Int) {
        holder.binding.root.setOnClickListener{
            listener.onItemClicked(position)
        }
        if (position == list_icon_tutorial.size - 1){
            holder.binding.seperatorBottomsheet.visibility = View.INVISIBLE
        }
        holder.binding.txtSetWallpaper.text = list_title_tutorial[position]
        holder.binding.imgSetWallapaper.setImageResource(list_icon_tutorial[position])
    }

    override fun getItemCount(): Int {
        return list_icon_tutorial.size
    }
}