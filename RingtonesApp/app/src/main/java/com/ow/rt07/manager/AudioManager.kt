package com.ow.rt07.manager

import android.media.MediaPlayer
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.ow.rt07.RingtonesApplication

object AudioManager {
    private var audio : MediaPlayer? = null

    var isPlaying : MutableLiveData<Boolean> = MutableLiveData(false)
    
}